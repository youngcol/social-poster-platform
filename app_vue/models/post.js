import { Model } from 'vue-models'

export default class PostModel extends Model { static defaults() { return {

_model_name: 'user',

computed: {
    full_name() {
        return `${this.first_name} ${this.last_name}`
    }
},

methods: {

    /**
     *
     * @returns {Array}
     */
    getMediaFiles() {
        var urls = [];

        if (this.media_files)
        {
            urls  = _.map(this.media_files, function (item) {
                return {
                    id: item.id,
                    url: item.s3_url === '' ? '/uploads/' + item.filename : item.s3_url
                };
            });
        }

        return urls;
    },

    getScheduledTime()
    {
        var ret = null;

        if (this.scheduled_time.process_status !== 'draft')
        {
            ret = this.scheduled_time.scheduled_time;
        }

        return ret;
    }
}

}}

static schema() { return {
    first_name: {
        type: String
    },
    last_name: {
        type: String
    }
}}

}


import { Model } from 'vue-models'

export default class SocialNetwork extends Model { static defaults() { return {

    // full model name
    _model_name: 'App\\Models\\SocialEngine\\SocialNetwork',

    computed: {

    },

    methods: {

        /**
         *
         * @returns {Array}
         */
    }

}}

    static schema() { return {
        name: {
            name: String
        },
        parent: {
            title: String
        }
    }}
}

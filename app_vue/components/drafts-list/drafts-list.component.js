import h from '../../helpers';
import PostModel from "../../models/post";

export default {
    name: "DraftsList",
    props: {
        bundle: {
            type: Object,
            required: true
        },

    },
    data() {
        return {
            // name: 'Matt Stauffer',
            // title: this.post.title,
        }
    },
    watch: {

    },

    async mounted() {

        this.$root.$on('post.clicked.edit', this.onPostClickedEdit);


    },

    methods: {
        onPostEditWindowClosed(result)
        {
            if (result.action == 'instantly')
            {
                this.$refs.postsList.removePostById(result.postId);
            }
            else if(result.action == 'scheduled')
            {
                this.$refs.postsList.removePostById(result.postId);
            }
            else if(result.action == 'draft')
            {

            }
            else if(result.action == 'queued')
            {
                this.$refs.postsList.removePostById(result.postId);
            }
        },

        onPostClickedEdit(post)
        {
            window.newPostWindow.openPost(new PostModel(post), {
                onCloseAfterUserAction: this.onPostEditWindowClosed
            });

        }
    },

    created () {

    },

    beforeDestroy() {

    }
}

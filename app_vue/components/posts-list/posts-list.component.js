import h from '../../helpers';
import PostModel from '../../models/post'


export default {
    name: "PostsList",
    props: {
        bundle: {
            type: Object,
            required: true
        }
    },

    data() {
        return {
            // social2: false,
            isLoadingBundle: false,
            storage: null,
        }
    },
    watch: {

    },

    created()
    {
        // debugger;
        this.storage = this.bundle;

    },

    async mounted() {

        this.$root.$on('doc_scrolled_to_bottom', this.onScrolledToBottom);


    },

    methods: {

        removePostById(postId)
        {
            _.map(this.storage.list, function (bundle) {

                var foundIndex = _.findIndex(bundle.items, {id: postId});

                if (foundIndex != -1)
                {
                    bundle.items.splice(foundIndex, 1);
                    return false;
                }
            });

        },

        socialAccount(id)
        {
            return _.find(user_social_accounts, {id: id});
        },

        onScrolledToBottom()
        {
            if (this.storage.list_items_count < this.storage.limit) return false;
            if (this.isLoadingBundle) return false;
            //=-=--=-=-==

            this.isLoadingBundle = true;


            h.api.post(h.route('GetLastPostsTask'), {
                type: this.storage.type,
                page: 0, //this.storage.page+1,
                limit: this.storage.limit
            }).then((response) => {
                this.onLoadedNewBundle(response.data.data);
                this.isLoadingBundle = false;
            });

        },


        onLoadedNewBundle(data)
        {
            var resp = data.resGetLastPostsTask;

            this.storage.list_items_count += resp.list_items_count;
            this.storage.page = resp.page;

            _.map(resp.list, (group) => {

                var groupIndex = _.findIndex(this.storage.list, {date: group.date});

                if (groupIndex == -1)
                {
                    this.storage.list.push(group);
                }
                else
                {
                    this.storage.list[groupIndex].items =
                        _.union(this.storage.list[groupIndex].items, group.items);
                }
            });
        },

        editPost(post)
        {
            this.$root.$emit('post.clicked.edit', post);
        }

    }
}

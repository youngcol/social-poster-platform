export default {
    methods: {
        /**
         * Return value from locales array
         *
         * @param route
         * @returns {*|string}
         */
        $L: function (route) {
            var parts = route.split('.');


            var ret = this.$root.i18n[parts.shift()];

            parts.map(function (i) {
                ret = ret[i];
            });

            // debugger;
            // return this.$root.i18n[route];

            return ret;
        }
    }
}

import h from '../../helpers';
import PostModel from "../../models/post";

export default {
    name: "SentList",
    props: {
        bundle: {
            type: Object,
            required: true
        },

    },
    data() {
        return {
            // name: 'Matt Stauffer',
            // title: this.post.title,
        }
    },
    watch: {

    },

    async mounted() {

        // this.$root.$on('post.clicked.edit', this.onPostClickedEdit);


    },

    methods: {

        onPostClickedEdit(post)
        {
            // window.newPostWindow.openPost(new PostModel(post), {
            //     onCloseAfterUserAction: this.onPostEditWindowClosed
            // });

        }
    },

    created () {

    },

    beforeDestroy() {

    }
}

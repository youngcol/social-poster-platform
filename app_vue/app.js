import Vue from 'vue';
import _ from 'lodash'

import Buefy from 'buefy'
import './buefy.scss'

Vue.use(Buefy)

require('./bootstrap');


// ----------- COMPONENTS ---------
import Profile from './components/Profile.vue';
import Helper from './stories/Helpers.vue';
import PostsList from './components/posts-list/PostsList.vue';
import DraftsList from './components/drafts-list/DraftsList.vue';
import QueueList from './components/queue-list/QueueList.vue';
import SentList from './components/sent-list/SentList.vue';

Vue.component('profile', Profile);
Vue.component('profile', Profile);
Vue.component('posts-list', PostsList);
Vue.component('drafts-list', DraftsList);
Vue.component('queue-list', QueueList);
Vue.component('sent-list', SentList);
// ----------- END COMPONENTS ---------



// ----------- MODELS ---------
import h from './helpers';
import PostModel from './models/post'
import SocialNetwork from './models/SocialEngine/SocialNetwork'
// ----------- END MODELS ---------


//
// const post = new PostModel({
//     first_name: 'Jane',
//     last_name: 'Doe'
// });
//
// debugger;
// h.repo.getModels((par)=>{return new SocialNetwork(par)}, 'App\\Models\\Pet')
//     .then((resp)=>{
//
//         resp.data.data.resGetModelsCollectionTask.collection
//
//         debugger;
//     })

let globalVue = new Vue({
    el: '.wrap',


    data: {
        i18n: i18n,

    },

    mounted() {


        this.locales

        // $('nav .dropdown').hover(function(){
        //     var $this = $(this);
        //     $this.addClass('show');
        //     $this.find('> a').attr('aria-expanded', true);
        //     $this.find('.dropdown-menu').addClass('show');
        // }, function(){
        //     var $this = $(this);
        //     $this.removeClass('show');
        //     $this.find('> a').attr('aria-expanded', false);
        //     $this.find('.dropdown-menu').removeClass('show');
        // });

// debugger;
//
//         const response = h.api.modelsCollection(
//             'App\\Models\\SocialEngine\\UserSocialAccount',
//             'SocialNetwork'
//         );

        //global events

        // this.$root.$on('post.clicked.edit', (post) => {
        //     debugger;
        //
        // });

    },

    methods: {

        onScroll()
        {

            debugger;
        },

        handleScroll: _.debounce(() => {

            log('app::handleScroll');

            const delta = 300;
            var bottomHeight = $(document).height() - (window.scrollY + window.outerHeight);

            if (bottomHeight < delta)
            {
                globalVue.$root.$emit(sp(globalVue.events, 'doc_scrolled_to_bottom'))
            }

        }, 200),

    }

});


// require('./test_components');

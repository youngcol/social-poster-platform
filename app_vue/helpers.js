import axios from 'axios'

var def = {
    api: {
        async get(url, params={})
        {
            var props = {};
            props.params = params;

            return axios.get(url, props)
                .catch(error => {
                    debugger;
                    error.response
                });
        },

        async post(url, data={})
        {
            data = _.extend(data, csrf);
            return axios
                .post(url, data)
                .catch(error => {
                    debugger;
                    error.response
                });
        },

        async runTask(taskName, params)
        {
            var data = {
                task_name: taskName,
                task_params: params
            };

            data = _.extend(data, csrf);
            return axios.post(sp(routes, 'api.run.task'), data);
        }
    },
    route(name)
    {
        return sp(routes, name);
    }
};

/**
 * Models repository object
 */
def.repo = {
    /**
     * Get cu models collection from server
     *
     * @param modelName
     * @param relations
     * @param offset
     * @param limit
     * @returns {Promise<Promise<AxiosResponse<T>>>}
     */
    async getModels(modelBuilder, modelName, relations=[], page=0, limit=10, order='asc')
    {
        var data = {
            model_name: modelName,
            page: page,
            limit: limit,
            order: order
        };

        if (relations.length)
        {
            data.relations = relations;
        }

        data = _.extend(data, csrf);
        var resp = await axios.post(sp(routes, 'api.cu.models'), data);

        // page: resp.data.data.resGetModelsCollectionTask.page,
        var res = resp.data.data.resGetModelsCollectionTask;
        var ret = _.cloneDeep(_.omit(res, ['collection']));

        return _.extend(ret, {
                list: _.map(resp.data.data.resGetModelsCollectionTask.collection, (item) => {
                    return modelBuilder(item);
                })
            }
        )
    },


    /**
     * Update models on server
     * @param modelName
     * @param modelsData
     * [{id: 1, param: 4343}, {id:2, title: 'change1'}]
     * @returns {Promise<void>}
     */
    async updateModels(modelName, modelsData)
    {
        def.api.runTask('UpdateModelsCollectionTask', {
                model_name: modelName,
                update_data: modelsData
            }
        );
    },

    /**
     * Delete model on server
     *
     * @param modelName
     * @param ids [1,2,3]
     * @returns {Promise<void>}
     */
    async deleteModels(modelName, ids=[])
    {
        def.api.runTask('DeleteModelsCollectionTask', {
                model_name: modelName,
                models_ids: ids
            }
        );
    },
};


export default def;

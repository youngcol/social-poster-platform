import Vue from "vue";
import VueModels from 'vue-models'
Vue.use(VueModels);


Object.defineProperty(Vue.prototype, '$_', { value: _ });

// app custom events
Object.defineProperty(Vue.prototype, 'events', { value:
        {
            doc_scrolled_to_bottom: 'doc_scrolled_to_bottom'
        }
});


/**
 * Scroll directive
 */
Vue.directive('scroll', {
    inserted: function (el, binding) {
        let f = function (evt) {
            if (binding.value(evt, el)) {
                window.removeEventListener('scroll', f)
            }
        }
        window.addEventListener('scroll', f)
    }
});

/**
 * Pass params into components
 */
Vue.component("slotscope", {
    render() {
        return this.$scopedSlots.default(this.$attrs);
    }
});


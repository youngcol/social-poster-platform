
function fork(call, time){
    var ret;

    eU(time) && (time = 30);
    ret = setTimeout(call, time);

    return ret;
}


function eU(v){return typeof v == 'undefined'}

/**
 * Get safe param
 *
 * @param ret
 * @param key
 * @param def
 * @return {*}
 */
function sp(ret, key, def){
    if (!ret)
    {
        if(eU(def))
        {
            return null;
        }
        else
        {
            return def;
        }
    }


    if(eU(ret[key]))
    {
        if(eU(def))
        {
            return null;
        }
        else
        {
            return def;
        }
    }
    else
    {
        return ret[key];
    }
}

/**
 * If key doesnt exists in array
 *
 * @param p
 * @param k
 * @returns {boolean}
 */
function noparam(p, k)
{
    return !sp(p, k);
}

function log_tag()
{
    var all_tags_info = arguments[0];
    var used_tags = arguments[1];

    if(!all_tags_info.is_use_console_loggin) return;
    //-=-=-=-=-=-


    if(typeof used_tags == 'string')
    {
        used_tags = [used_tags];
    }

    // выбрать активные тэги для логов
    var active_tags = [];

    used_tags.map(function(tag_name)
    {
        if(all_tags_info.loggin[tag_name])
        {
            active_tags.push(tag_name);
        }
    });

    if(active_tags.length)
    {
        var length = arguments.length;
        var tag_string = ' <= '+ ' ['+ active_tags.join(',')+ '] ' +
            (all_tags_info.__login_object_name || all_tags_info.obj_name);

        if( length == 2 )  console.log({empty: 'empty'}, tag_string);
        else if( length == 3 )  console.log(arguments[2], tag_string);
        else if( length == 4 )  console.log(arguments[2], arguments[3], tag_string);
        else if( length == 5 )  console.log(arguments[2], arguments[3], arguments[4], tag_string);
        else if( length == 6 )  console.log(arguments[2], arguments[3], arguments[4], arguments[5], tag_string);
        else if( length == 7 )  console.log(arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], tag_string);
        else if( length == 8 )  console.log(arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7], tag_string);
        else if( length == 9 )  console.log(arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7], arguments[8], tag_string);
        else if( length == 10 )  console.log(arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7], arguments[8], arguments[9], tag_string);
    }
}

function app__def_comp_options(compname, $$)
{
    $$.obj_name                 = name__obj_name(compname);
    $$.is_use_console_loggin    = 1;
    $$.__login_object_name      = $$.obj_name;

    return $$;
}


function name__obj_name(name)
{
    return [newUnicID(),  name].join(' ');
}

var _unicId = 0;
function newUnicID()
{
    return 'uniq_' + (_unicId++);
}

var _zindex = 100;
function zindex()
{
    return ++_zindex;
}

uniqueId=0;
function uniqId()
{
    return uniqueId++;
}


function crit_noparam(message){
    throw new Error(message);
}

function toInt(v)
{
    return Math.floor(v);
}

function p(str)
{
    console.log(str)
}


function eU(v){return typeof v == 'undefined'}
function eN(v){return v === null}
function eF(v){return v === false}
function eT(v){return v === true}
function eNU(v){ return eN(v) || eU(v)}


function $box__out_click(clickName, $box, call)
{
    var id = $box.attr('id');

    if(!id)
    {
        crit('Box has no id')
    }

    $(document).mousedown(function(event)
    {
        var $tar = $(event.target);

        var is_out_click =
            ($tar.attr('id') != $box.attr('id'));

        if (is_out_click)
        {
            is_out_click = !$tar.parents('#'+id).length;
        }

        if (is_out_click)
        {
            call($tar);
        }
    });
}


/**
 * Клонировать объект
 *
 * @param obj
 * @return {*}
 */
function clone(obj){
    if(obj == null || typeof(obj) != 'object') return obj;

    if (obj.constructor)
        var temp = new obj.constructor();
    else
        var temp = {};

    for(var key in obj)
        temp[key] = clone(obj[key]);

    return temp;
}


function button_loading_right($btn)
{
    $btn.addClass('disabled right');
}

function button_loading($btn)
{
    var t = $btn.text();
    if (t != '...') {
        $btn.attr('data-old-text', t);
    }

    $btn
//        .button('loading')
        .addClass('disabled')
        .text('...');
}

function button_reset($btn)
{
    var text = $btn.attr('data-old-text');

    $btn
//        .button('reset')
        .removeClass('disabled')
        .text(text);
}

function button_disable($btn)
{
    $btn
        .addClass('disabled')
        .attr('disabled', 'disabled');
}

function button_status($btn, statusCls)
{
    var cls = $btn.attr('class');

    ['btn-primary','btn-success','btn-info','btn-warning', 'btn-danger']
        .map(function(c) {
            $btn.removeClass(c)
    });

    $btn.addClass(statusCls);

    _.delay(function() {

        $btn.attr('class', cls);

    }, 500);
}

function button_enable($btn)
{
    $btn
        .removeClass('disabled')
        .removeAttr('disabled');
}

function infoNotify(text) {
    $.simplyToast(text, 'info', {
        ele: 'body', // which element to append to
//                    offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
        align: 'right', // ('left', 'right', or 'center')
        width: 250, // (integer, or 'auto')
        delay: 2000,
        allow_dismiss: true,
        stackup_spacing: 10 // spacing between consecutively stacked growls.
    });
}

function successNotify(text) {
    $.simplyToast(text, 'success', {
        ele: 'body', // which element to append to
//                    offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
        align: 'right', // ('left', 'right', or 'center')
        width: 250, // (integer, or 'auto')
        delay: 4000,
        allow_dismiss: true,
        stackup_spacing: 10 // spacing between consecutively stacked growls.
    });
}

function errorNotify(text) {
    $.simplyToast(text, 'danger', {
        ele: 'body', // which element to append to
//                    offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
        align: 'right', // ('left', 'right', or 'center')
        width: 250, // (integer, or 'auto')
        delay: 4000,
        allow_dismiss: true,
        stackup_spacing: 10 // spacing between consecutively stacked growls.
    });
}

function getFileExtension(name)
{
    var found = name.lastIndexOf('.') + 1;
    return (found > 0 ? name.substr(found) : "");
}

/**
 *
 * @param $box
 * @param callb
 * @param par
 */
function toggleModal($box, callb, par)
{
    if (sp(par, 'force_open'))
    {
        $box.removeClass('hidden');
    }
    else
    {
        $box.toggleClass('hidden');
    }

    if (!$box.hasClass('hidden'))
    {
        var top = $(window).scrollTop();
        $box
            .css('z-index', zindex())
            .css('top', (top + 45) + 'px');

        $(".close-icon", $box).unbind().click(function () {

            $box.toggleClass('hidden');
            callb && callb.onClose && callb.onClose();

        });
    }
}

function closeModal($box)
{
    $box.toggleClass('hidden');
}

function log()
{
    console.log.apply(null, arguments);
}

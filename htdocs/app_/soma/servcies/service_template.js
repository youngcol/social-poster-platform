;(function(app, undefined) {

    'use strict';

    // package
    app.services = app.services || {};

    app.services.service_template = function(__, config, dispatcher, api)
    {
        var pub = {}, f = {}, on={}, ui={}, _i = {}, self = this, $$ = {

            $box: null,

            state: {

            },

            $btn:   null,
            status: null,

            loggin:
            {

            }
        };

        f.construct = function()
        {
            $$ = app__def_comp_options('new_post_window', $$);
            _.extend($$, __);


            f.__s('big_bang');
        };

        f.__s = function(state, par)
        {
            log_tag($$, 'state', state, par);

            switch (state)
            {
                case 'big_bang':
                {
                    $$.$btn     = $('#main-page-btn');
                    $$.btnCls   = $$.$btn.attr('class');

                    break;
                }
            }
        };



        pub.test = function()
        {

        };


        f.construct();

        $$._i = {

            test:    pub.test,


        };

        return $$._i;
    };

})(window.app = window.app || {});

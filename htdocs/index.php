<?php

define("PATH_ROOT", __DIR__ . '/../');
require PATH_ROOT . 'app/server.php';

// Run the app
$app->run();

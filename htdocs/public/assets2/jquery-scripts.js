/* additonal jquery scripts */
// ie https://api.sharedcount.com/v1.0/?apikey=b9b9070064e6b4333bbb1db7af47c15772bf799c&url=https://www.brightlocal.com/2019/01/21/photos-in-google-local-services-ads-what-you-need-to-know/&callback=json
//
// jQuery.sharedCount = function(url, fn) {
//     siteDom = 'https://www.brightlocal.com';
//     path = window.location.pathname;
//     fullUrl = siteDom + path;
//     url = encodeURIComponent(url || fullUrl);
//     var domain = "//api.sharedcount.com/v1.0/"; /* SET DOMAIN */
//     var apikey = "b9b9070064e6b4333bbb1db7af47c15772bf799c" /*API KEY HERE*/
//     var arg = {
//       data: {
//         url : url,
//         apikey : apikey
//       },
//         url: domain,
//         cache: true,
//         dataType: "json"
//     };
//     if ('withCredentials' in new XMLHttpRequest) {
//         arg.success = fn;
//     }
//     else {
//         var cb = "sc_" + url.replace(/\W/g, '');
//         window[cb] = fn;
//         arg.jsonpCallback = cb;
//         arg.dataType += "p";
//     }
//     return jQuery.ajax(arg);
// };

// document ready event
jQuery(document).ready(function($) {
    siteDom = 'https://www.brightlocal.com';
    path = window.location.pathname;
    var fullUrl = siteDom + path;
    var hardShares =  $('#countme').data("count");

    // $.sharedCount(fullUrl, function(data){
    //     var sharedCountTotal = data.Facebook.total_count + data.LinkedIn + data.Pinterest + data.StumbleUpon + data.GooglePlusOne;
    //     grandTotal = sharedCountTotal + hardShares;
    //     $("#countme").text(grandTotal);
    //     // console.log(sharedCountTotal);
    //     // console.log(hardShares);
    //     // console.log(grandTotal);
    // });
    //
    // $(".loop-load-more .post").each(function(){
    //     var postShares =  $(this).find('.share-count').data("count");
    //     var postUrl =  $(this).find('.share-count').data("url");
    //     var theElement =  $(this).find('.share-count');
    //     var postsTotal = 0;
    //     $.sharedCount(postUrl, function(data){
    //         var postShareTotal = data.Facebook.total_count + data.LinkedIn + data.Pinterest + data.StumbleUpon + data.GooglePlusOne;
    //         var postsTotal = postShareTotal + postShares;
    //         // console.log(postShares);
    //         // console.log(postUrl);
    //         // console.log(postsTotal);
    //         // console.log(theElement);
    //         theElement.text(postsTotal);
    //     });
    //
    //
    // });

    // $('.gfield input').keyup(function() {
    //     if( $(this).val().length === 0 ) {
    //       $(this).closest('div').removeClass('active');
    //     } else {
    //       $(this).closest('div').addClass('active');
    //     }
    // });

    // $('.gfield > .ginput_container > input').keyup(function() {
    //     if( $(this).val().length === 0 ) {
    //       $(this).closest('li').removeClass('active');
    //     } else {
    //       $(this).closest('li').addClass('active');
    //     }
    // });
    
    // $('.payup.inline .ginput_full input[type="text"]').keyup(function() {
    //     if( $(this).val().length === 0 ) {
    //       $(this).closest('.ginput_full').removeClass('active');
    //     } else {
    //       $(this).closest('.ginput_full').addClass('active');
    //     }
    // });
});

// window load event
jQuery(window).on('load', function() {
    jQuery('.singled .share-facebook').on('click', function(){
        var u = jQuery('link[rel=shortlink]').attr('href');
        var t = document.title;
        sharePopup('https://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t));
        return false;
    });

    jQuery('.grouped .share-facebook').on('click', function(){
        var u = jQuery(this).siblings('.shortlink').attr('href');
        var t = jQuery(this).closest('.type-post').find( "a" ).attr('title');
        sharePopup('https://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t));
        return false;
    });

    jQuery('.singled .share-twitter').on('click', function(){
        var u = jQuery('link[rel=shortlink]').attr('href');
        var t = document.title+' - ';
        sharePopup('https://twitter.com/share?url='+encodeURIComponent(u)+'&text='+encodeURIComponent(t)+'&via=bright_local');
        return false;
    });

    jQuery('.grouped .share-twitter').on('click', function(){
        var u = jQuery(this).siblings('.shortlink').attr('href');
        var t = jQuery(this).closest('.type-post').find( "a" ).attr('title') +' - ';
        sharePopup('https://twitter.com/share?url='+encodeURIComponent(u)+'&text='+encodeURIComponent(t)+'&via=bright_local');
        return false;
    });

    jQuery('.singled .share-linkedin').on('click', function(){
        var u = jQuery('link[rel=shortlink]').attr('href');
        var t = document.title;
        var d = jQuery("meta[name='description']").attr('content');
        sharePopup('https://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(u)+'&title='+encodeURIComponent(t)+'&summary='+encodeURIComponent(d)+'&source='+encodeURIComponent('http://www.brightlocal.com')+'');
        return false;
    });

    jQuery('.grouped .share-linkedin').on('click', function(){
        var u = jQuery(this).siblings('.shortlink').attr('href');
        var t = jQuery(this).closest('.type-post').find( "a" ).attr('title');
        var d = jQuery(this).attr('data-desc');
        sharePopup('https://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(u)+'&title='+encodeURIComponent(t)+'&summary='+encodeURIComponent(d)+'&source='+encodeURIComponent('http://www.brightlocal.com')+'');
        return false;
    });

    // twitter quotes
    jQuery('.actio .twitter').on('click', function(){
        var t = jQuery(this).parents().siblings('.tweettext').text().trim();
        var u = jQuery('link[rel=shortlink]').attr('href');

        sharePopup('https://twitter.com/share?url='+encodeURIComponent(u)+'&text='+encodeURIComponent(t)+'');
        return false;
    });

    // twitter takaways
    jQuery('li.tweetme').on('click', function(){
        var t = jQuery(this).text().trim();
        var u = jQuery('link[rel=shortlink]').attr('href');

        sharePopup('https://twitter.com/share?url='+encodeURIComponent(u)+'&text='+encodeURIComponent(t)+'');
        return false;
    });
});

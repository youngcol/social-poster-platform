<?php

namespace App\VO;

/**
 * Class Vo - value object
 * @package App\VO
 */

class Vo
{
    protected $args = null;

    public function __construct($args)
    {
        $this->val(...[$args]);

        $this->args = $args;
    }


    public function get(): string
    {
        return $this->args;
    }

    public function _()
    {
        return $this->args;
    }

    public function __toString(): string
    {
        return (string)$this->args;
    }

    public function toInt(): int
    {
        return (int)$this->args;
    }

    public function __toArray()
    {
        return $this->args;
    }

    public function toArray()
    {
        return $this->args;
    }

    public function toIds()
    {
        return map(explode(',', $this->args), function ($item) {
            return (int)$item;
        });
    }
}

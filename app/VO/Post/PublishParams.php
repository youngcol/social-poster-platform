<?php

declare(strict_types=1);
namespace App\VO\Post;

use App\Common\VoException;
use App\VO\Vo;

class PublishParams extends Vo
{
    public function __construct(string $publishType, array $params=[])
    {
        $this->val($publishType, $params);
        $this->publishType = $publishType;
        $this->params = $params;
    }

    protected function val($publishType, $params)
    {
        val($publishType, 'post.publish_params.publish_type');

        $keys = array_keys($params);

        foreach ($keys as $key)
        {
            val($key, 'post.publish_params.keys');

            if ($key == 'scheduled_time')
            {
                val($params[$key], 'datetime');
            }

            if ($key == 'accounts')
            {
                $accounts = $params[$key];

                foreach ($accounts as $account)
                {

                }
            }
        }
    }
}


//                    val_rule(sp($account, 'account_id'), 'required|min:1');
//                    val_rule(sp($account, 'boards'), 'required|min:1');
//                https://github.com/ptrofimov/matchmaker

//        'pinterest' => ['account_id' => '', 'board_ids' => '']
//        val($val, 'pinterest.oauth.scopes');
//foreach ($scopes as $scope) {
//    val($scope, 'pinterest.oauth.scopes');
//}

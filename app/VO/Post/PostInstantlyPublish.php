<?php

declare(strict_types=1);
namespace App\VO\Post;

use App\Common\VoException;
use App\Contracts\PublishReasonContract;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\Post\PostPublishParams;
use App\VO\Vo;

class PostInstantlyPublish extends Vo implements PublishReasonContract
{
    public function __construct(Post $post, PostPublishParams $postPublishParams)
    {
        $this->id = time();
        $this->post = $post;
        $this->postPublishParams = $postPublishParams;
    }

    protected function val($val)
    {

    }


    public function getPostPublishParams(): PostPublishParams
    {
        return $this->postPublishParams;
    }

    public function getPost(): Post
    {
        return $this->post;
    }

}

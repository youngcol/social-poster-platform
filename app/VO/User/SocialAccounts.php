<?php

declare(strict_types=1);
namespace App\VO\User;

use App\Common\ValidationException;
use App\Common\VoException;
use App\Models\SocialEngine\UserSocialAccount;
use App\Models\User;
use App\VO\Vo;

class SocialAccounts extends Vo
{
    public function __construct(User $user, array $socialAccountsId)
    {
        $this->val($user, $socialAccountsId);
    }

    protected function val(User $user, array $socialAccountsId)
    {

        $userSocialAccounts = UserSocialAccount::where('user_id', $user->id)->whereIn('id', $socialAccountsId)->get();

        if (count($userSocialAccounts) !== count($socialAccountsId))
        {
            throw new ValidationException('User social accounts are not fully right');

        }

        $this->args = $userSocialAccounts;
    }
}

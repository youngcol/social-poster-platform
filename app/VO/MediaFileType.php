<?php

namespace App\VO;

use App\Common\VoException;
use App\VO\Vo;

class MediaFileType extends Vo
{
    protected function val(string $val)
    {
        val($val, 'mediafile.type');
    }
}

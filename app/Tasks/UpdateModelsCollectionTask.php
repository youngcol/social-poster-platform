<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 * Update models by id
 *
 */

use App\Common\Res;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class UpdateModelsCollectionTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        VoVal $modelName,
        VoVal $modelsData
    )
    {
        if (!class_exists($modelName->_()))
        {
            return resFail("Model doesn't exists", ['model_name' => $modelName]);
        }
        //-=-=-=-=-=-=-=-=-=-=-=-=

        $upd = [];
        $r = repo($modelName->_());

        foreach ($modelsData->toArray() as $modelData)
        {
            $id = $modelData['id'];

            $r->where([
                'user_id' => $user->id,
                'id' => $id
            ]);

            unset($modelData['id']);

            $updated = $r->update($modelData);
            $upd[] = [
                'id' => $id,
                'updated' => $updated
            ];
        }

        return new Res([
            'upd' => $upd
        ]);
    }
}

<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine;
use App\Common\Res;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\Post\PostPublishParams;
use App\Models\SocialEngine\Post\PostQueuedPlace;
use App\Models\SocialEngine\Post\PostScheduledTime;
use App\Models\SocialEngine\QueueTime;
use App\Models\User;
use App\Tasks\Task;
use App\VO\Post\CreationParams;
use App\VO\Post\PublishParams;
use App\VO\User\MediaFiles;
use App\VO\User\SocialAccounts;
use App\VO\VoVal;
use Carbon\Carbon;

/**
 *
 * Creates new user post that can be sent to social media
 * @package App\Tasks\SocialEngine
 */
class CreatePostTask extends Task
{
    use SocialEngineTrait;
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param User $user
     * @param VoVal $text Post text
     * @param MediaFiles $mediaFiles Post media files
     * @param SocialAccounts|null $voSocialAccounts Social accounts to be sent to
     * @param PublishParams $publishParams Post publish params
     * @return Res
     */
    public function run
    (
        User $user,
        VoVal $text,
        MediaFiles $mediaFiles,
        ?SocialAccounts $voSocialAccounts,
        PublishParams $publishParams
    )
    {

        $post = Post::create([
            'user_id' => $user->id,
            'text' => $text->_(),
            'is_draft' => $publishParams->publishType === 'draft'
        ]);

        $post->mediaFiles()->sync($mediaFiles->toArray());

        $postPublishParams = new PostPublishParams();
        $postPublishParams->fill([
            'post_id' => $post->id,
            'social_accounts' => map($voSocialAccounts->toArray(), function ($item) {return $item->id;}),
            'custom_params' => $publishParams->params,
        ]);
        $postPublishParams->save();


        $postScheduledTime = new PostScheduledTime();
        $postScheduledTime->fill([
            'user_id' => $user->id,
            'post_id' => $post->id,
            'publish_params_id' => $postPublishParams->id,
        ]);

        if ($publishParams->publishType == 'queued')
        {
            $sheduledTime = $this->get_next_queue_time(now_date());

            $postScheduledTime->scheduled_time = date_mysql_format($sheduledTime);
            $postScheduledTime->save();
        }
        elseif($publishParams->publishType == 'scheduled')
        {
            $postScheduledTime->scheduled_time = $publishParams->params['scheduled_time'];
            $postScheduledTime->save();
        }
        elseif($publishParams->publishType == 'draft')
        {
            $postScheduledTime->process_status = 'draft';
            $postScheduledTime->save();
        }
        elseif($publishParams->publishType == 'instantly')
        {
            $postScheduledTime->scheduled_time = date_mysql_format(now_date());
            $postScheduledTime->process_status = 'draft';
            $postScheduledTime->save();
        }

        return new Res([
            'post' => $post,
            'postPublishParams' => $postPublishParams,
        ]);
    }

}

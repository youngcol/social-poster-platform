<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\Post\PostPublishParams;
use App\Models\SocialEngine\Post\PostScheduledTime;
use App\Models\User;
use App\Tasks\Task;
use App\VO\Post\PublishParams;
use App\VO\User\MediaFiles;
use App\VO\User\SocialAccounts;
use App\VO\VoVal;

class UpdatePostTask extends Task
{
    use SocialEngineTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        int $postId,
        User $user,
        VoVal $text,
        array $mediaFilesModels,
        SocialAccounts $socialAccountsId,
        PublishParams $publishParams,
        ?VoVal $removeMediaIds
    )
    {
        $post = Post::where('user_id', $user->id)->find($postId);
        if (eN($post)) return resFail('Cant find post');
        //-=-=-=-=-=-=-=-=-=-=-=-=

        if (!eN($removeMediaIds))
        {
            $post->mediaFiles()->detach($removeMediaIds->toIds());
        }

        $attachMediaIds = map($mediaFilesModels, function ($item) {
            return $item->id;
        });
        $post->mediaFiles()->attach($attachMediaIds);

        $post->is_draft = $publishParams->publishType === 'draft';
        $post->text = $text->_();
        $post->save();


        // update post publish stuff
        $postPublishParams = $post->scheduledTime->publishParams;

        $postPublishParams->fill([
            'post_id' => $post->id,
            'social_accounts' => map($socialAccountsId->toArray(), function ($item) {return $item->id;}),
            'custom_params' => $publishParams->params,
        ]);
        $postPublishParams->save();


        $postScheduledTime = $post->scheduledTime;
        $postScheduledTime->fill([
            'user_id' => $user->id,
            'post_id' => $post->id,
            'publish_params_id' => $postPublishParams->id,
        ]);

        if ($publishParams->publishType == 'queued')
        {
            $sheduledTime = $this->get_next_queue_time(now_date());
            $postScheduledTime->scheduled_time = date_mysql_format($sheduledTime);
            $postScheduledTime->process_status = 'scheduled';
            $postScheduledTime->save();
        }
        elseif($publishParams->publishType == 'scheduled')
        {
            $postScheduledTime->scheduled_time = $publishParams->params['scheduled_time'];
            $postScheduledTime->process_status = 'scheduled';
            $postScheduledTime->save();
        }
        elseif($publishParams->publishType == 'draft')
        {
            $postScheduledTime->scheduled_time = date_mysql_format(now_date());
            $postScheduledTime->process_status = 'draft';
            $postScheduledTime->save();
        }
        elseif($publishParams->publishType == 'instantly')
        {
            $postScheduledTime->scheduled_time = date_mysql_format(now_date());
            $postScheduledTime->process_status = 'draft';
            $postScheduledTime->save();
        }

        return new Res([
            'post' => $post,
            'postPublishParams' => $postPublishParams,
        ]);
    }
}

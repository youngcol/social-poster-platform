<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\CU;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class GetPinterestAccountsBoardsTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        array $accountIds
    )
    {
        $boards = [];
        $socialAccounts = $user->socialAccount()->whereIn('id', $accountIds)->get();

        foreach ($socialAccounts as $socialAccount)
        {
            $this->c->pinner->auth->setOAuthToken($socialAccount->access_token);
            $list = $this->c->pinner->getMeBoards()->boards;

//            $list = [
//                'data' => [
//                    ['id' => 1111111, 'name' => 'board 1'],
//                    ['id' => 2222222, 'name' => 'board 2'],
//                ]
//            ];

            $boards[] = [
                'account_id' => $socialAccount->id,
                'boards' => $list
            ];
        }

        return new Res([
            'boards' => $boards
        ]);
    }
}

<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine\Publish;

use App\Common\FlowException;
use App\Common\Res;
use App\Models\SocialEngine\MediaFile;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\Post\PostPublishLog;
use App\Models\SocialEngine\Post\PostPublishParams;
use App\Models\SocialEngine\SocialNetwork;
use App\Models\SocialEngine\UserSocialAccount;
use App\Contracts\PublishReasonContract;
use App\Tasks\MediaFile\DownloadMediaFromS3Task;
use App\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Support\Arr;


class PublishToSocialAccountsTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        PublishReasonContract $publishReason
    )
    {
        $published = [];

        /** @var Post $post */
        $post = $publishReason->getPost();

        /** @var Post\PostPublishParams $publishParams */
        $publishParams = $publishReason->getPostPublishParams();


        $socialAccounts = UserSocialAccount::query()
            ->whereIn('id', $publishParams->social_accounts)
            ->get();

        if ($post->scheduledTime)
        {
            $post->scheduledTime->process_status = 'processing';
            $post->scheduledTime->save();
        }


        foreach ($socialAccounts as $userSocialAccount)
        {
            $is_success = false;
            $info = [];
            $social_network_id = 0;

            if ($userSocialAccount->isBelongsTo(SocialNetwork::PINTEREST))
            {
                $resPublished = $this->publishToPinterest($post, $publishParams, $userSocialAccount);
                $pinId = data_get($resPublished, 'pin.id');

                if ($pinId)
                {
                    $is_success = true;
                    $info =
                        [
                            'id' => $pinId
                        ];
                }

                $social_network_id = SocialNetwork::PINTEREST;

            }
            elseif($userSocialAccount->isBelongsTo(SocialNetwork::TWITTER))
            {
                $resPublished = $this->publishToTwitter($post, $userSocialAccount);

                if (data_get($resPublished, 'tweet.result.id'))
                {
                    $is_success = true;
                    $info =
                        [
                            'id' => $resPublished->tweet->result->id,
                            'created_at' => $resPublished->tweet->result->created_at
                        ];
                }

                $social_network_id = SocialNetwork::TWITTER;
            }

            // update posts stuff
            $gmtNow = now_date();
            PostPublishLog::create([
                'social_network_id' => $social_network_id,
                'post_id' => $post->id,
                'is_success' => $is_success,
                'publish_time' => date_mysql_format($gmtNow),
                'info' => json_encode($info),

                'publich_reason_class' => get_class($publishReason),
                'publich_reason_id' => $publishReason->id
            ]);

            $published[] = $resPublished;
        }

        if ($post->scheduledTime)
        {
            $post->scheduledTime->process_status = 'done';
            $post->scheduledTime->save();
        }

        return new Res([
            'published' => $published
        ]);
    }

    protected function publishToTwitter(Post $post, UserSocialAccount $userSocialAccount)
    {
        $this->c->twitter->makeConnection(
            vo($userSocialAccount->access_token, 'filled.string'),
            vo($userSocialAccount->access_token_secret, 'filled.string')
        );

        $mediaFilesPaths = map($post->mediaFiles, function ($item)
        {
            if (strlen($item->s3_url) > 0)
            {
                $res = task(new DownloadMediaFromS3Task,
                    [
                        $item
                    ]
                );

                check($res->fail(), 'Can\'n download media file to local server', ['res' => $res, 'mediaFile' => $item]);

                return uploads_path($res->destFileName);
            }
            elseif(strlen($item->filename) > 0)
            {
                return uploads_path($item->filename);
            }
            else
            {
                flow_exception('MediaFile model doesnt contain filename', [$item]);
            }
        });

        $resTweet = $this->c->twitter->createTweet($post->text, $mediaFilesPaths);

        return new Res(
            [

                'tweet' => $resTweet,
            ]
        );
    }

    /**
     * @param Post $post
     * @param PostPublishParams $publishParams
     * @param UserSocialAccount $userSocialAccount
     */
    protected function publishToPinterest(Post $post, PostPublishParams $publishParams, UserSocialAccount $userSocialAccount)
    {
        /** @var MediaFile $mediaFile */
        $mediaFile = sp($post->mediaFiles, 0);
        $mediaFile->val();

        $accountInfo = Arr::first
            (
                sp($publishParams->custom_params, 'accounts', []),
                function ($value, $key) use ($userSocialAccount)
                {
                    return $value['account_id'] == $userSocialAccount->id;
                }
            );


        check($accountInfo === null, 'Cant find account info', [$publishParams]);
        //-=-=-=-=-=-=-=-=-=-=-=-=

        $this->c->pinner->auth->setOAuthToken($userSocialAccount->access_token);


        foreach ($accountInfo['board_ids'] as $boardId)
        {
            if ($mediaFile->isUploadedToS3())
            {
                $resPin = $this->c->pinner->createPinFromImageUrl
                    (
                        $post->text,
                        uploads_path($mediaFile->filename),
                        $boardId
                    );
            }
            elseif ($mediaFile->isUploadedLocally())
            {
                $resPin = $this->c->pinner->createPinFromImagePathname
                    (
                        $post->text,
                        uploads_path($mediaFile->filename),
                        $boardId
                    );
            }
        }

        return new Res(
            [
                'pin' => $resPin,
            ]
        );
    }
}

<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 * Delete models collection
 */

use App\Common\Res;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class DeleteModelsCollectionTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        VoVal $modelName,
        VoVal $ids
    )
    {

        if (!class_exists($modelName->_()))
        {
            return resFail("Model doesn't exists", ['model_name' => $modelName]);
        }
        //-=-=-=-=-=-=-=-=-=-=-=-=

        $r = repo($modelName->_());

        $delete = $r->where(['user_id' => $user->id])
                    ->delete($ids->toArray());

        return new Res([
            'delete' => $delete,
            'ids' => $ids->toArray()
        ]);
    }
}

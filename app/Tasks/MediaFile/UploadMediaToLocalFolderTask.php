<?php
declare(strict_types=1);
namespace App\Tasks\MediaFile;

use App\Common\Res;
use App\Models\SocialEngine\MediaFile;
use App\Models\User;
use App\Tasks\Task;

use Slim\Http\UploadedFile;

class UploadMediaToLocalFolderTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run(User $user, UploadedFile $uploadedFile)
    {

            if ($uploadedFile->getError() !== UPLOAD_ERR_OK)
            {
                return resFail('request_file_upload_error', []);
            }

            $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
            $clientMediaType = $uploadedFile->getClientMediaType();

            $type = vo(explode('/', $clientMediaType)[0], 'mediafile.type');

            $mediaFile = MediaFile::create([
                'type' => $type,
                'user_id' => $user->id
            ]);


            $filename = uuid1FromId($mediaFile->id) . '.' . $extension;

            $uploadedFile->moveTo(UPLOADS_PATH . DIRECTORY_SEPARATOR . $filename);

            $mediaFile->filename = $filename;
            $mediaFile->save();

//
//            dump($extension);echo 'UploadMediaToLocalFolderTask.php:27'; exit;
//            $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
//            $filename = sprintf('%s.%0.8s', $basename, $extension);
//
//            $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
//
//            $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
//            $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
//            $filename = sprintf('%s.%0.8s', $basename, $extension);
//
//            $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
//



//            dump($filesArray);echo 'UploadMediaToLocalFolderTask.php:17'; exit;
//
//            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
//                $filename = $this->moveUploadedFile($directory, $uploadedFile);
//                $response->write('uploaded ' . $filename . '<br/>');
//            }
//
//
//
//            foreach ($filesArray as $fileItem) {
//
//                'image/'
//
//            }
//
//            $fileinfo = @getimagesize($fileItem["tmp_name"]);
//            $width = $fileinfo[0];
//            $height = $fileinfo[1];
//
//            $allowed_image_extension = array(
//                "png",
//                "jpg",
//                "jpeg"
//            );
//
//            // Get image file extension
//            $file_extension = pathinfo($_FILES["file-input"]["name"], PATHINFO_EXTENSION);
//
//            // Validate file input to check if is not empty
//            if (! file_exists($_FILES["file-input"]["tmp_name"])) {
//                $response = array(
//                    "type" => "error",
//                    "message" => "Choose image file to upload."
//                );
//            }    // Validate file input to check if is with valid extension
//            else if (! in_array($file_extension, $allowed_image_extension)) {
//                $response = array(
//                    "type" => "error",
//                    "message" => "Upload valiid images. Only PNG and JPEG are allowed."
//                );
//                echo $result;
//            }    // Validate image file size
//            else if (($_FILES["file-input"]["size"] > 2000000)) {
//                $response = array(
//                    "type" => "error",
//                    "message" => "Image size exceeds 2MB"
//                );
//            }    // Validate image file dimension
//            else if ($width > "300" || $height > "200") {
//                $response = array(
//                    "type" => "error",
//                    "message" => "Image dimension should be within 300X200"
//                );
//            } else {
//                $target = "image/" . basename($_FILES["file-input"]["name"]);
//                if (move_uploaded_file($_FILES["file-input"]["tmp_name"], $target)) {
//                    $response = array(
//                        "type" => "success",
//                        "message" => "Image uploaded successfully."
//                    );
//                } else {
//                    $response = array(
//                        "type" => "error",
//                        "message" => "Problem in uploading image files."
//                    );
//                }
//            }
//        }

            return new Res([
                'mediaFile' => $mediaFile
            ]);

    }
}

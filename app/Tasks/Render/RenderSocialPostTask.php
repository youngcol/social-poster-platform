<?php
declare(strict_types=1);
namespace App\Tasks\Render;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class RenderSocialPostTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        array $posts,
        VoVal $renderType
    )
    {

        if ($renderType->_() === 'middle_cart')
        {
            $html = render('app/components/posts/middle_cart_list', [

            ]);
        }

        return new Res([
            'html' => $html
        ]);
    }

}

<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 * Get models collection task (acts as repository)
 */

use App\Common\Res;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class GetModelsCollectionTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $cu,
        VoVal $modelName,
        VoVal $relations,
        VoVal $page,
        VoVal $limit,
        VoVal $order
    )
    {
        $name = $modelName->_();
        if (!class_exists($name))
        {
            return resFail("Model doesn't exists", ['model_name' => $name]);
        }
        //-=-=-=-=-=-=-=-=-=-=-=-=
        global $dictionary_models;
        $isDictionaryModel = in_array($name, $dictionary_models);

        $model = new $name;
        $q = $model::query();

        if (!$isDictionaryModel)
        {
            $q->where('user_id', '=', $cu->id);
        }

        $q->offset($page->toInt() * $limit->toInt())->limit($limit->toInt());

        $withModels = $relations->toArray();

        if ($withModels and count($withModels))
        {
            $q->with($withModels);
        }

        $q->orderBy('id', $order->_());

        $collection = $q->get();

        return new Res([
            'collection' => $collection,
            'limit' => $limit->_(),
            'page' => $page->_(),
            'relations' => $relations->_(),
            'order' => $order->_(),
            'model_name' => $modelName->_()
        ]);
    }
}

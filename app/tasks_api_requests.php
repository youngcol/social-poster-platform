<?php

use App\Facades\CU;
use App\Models\User;
use App\Tasks\DeleteModelsCollectionTask;
use App\Tasks\GetModelsCollectionTask;
use App\Tasks\UpdateModelsCollectionTask;

$tasks_api_requests = [];

$tasks_api_requests['templateddsdds'] =
[
    // only users with this permissions allowed
    'allowed_permissions' => [],

    'params' => [
        'model_name' => 'required|filled.string',
        'page' => 'required|numeric',
        'relations' => 'array'
    ],

    'runner' => function (array $params) {

        $res = task(new template_task,
            [
                CU::user(),
                $params['model_name'],
                $params['relations'],
                $params['page'],
                $params['limit'],
                $params['order']
            ]
        );
        return $res;
    }
];

$tasks_api_requests['GetModelsCollectionTask'] =
[
    // only users with this permissions allowed
    'allowed_permissions' => [],

    'params' => [
        'user_id' => 'required|numeric',
        'model_name' => 'required|filled.string',
        'page' => 'required|numeric',
        'order' => 'required|mysql.order',
        'limit' => 'required|numeric',
        'relations' => 'array'
    ],

    'runner' => function (array $params) {

        $User = repo(User::class)->find($params['user_id']->_());

        $res = task(new GetModelsCollectionTask,
            [
                $User,
                $params['model_name'],
                $params['relations'],
                $params['page'],
                $params['limit'],
                $params['order']
            ]
        );
        return $res;
    }
];


$tasks_api_requests['DeleteModelsCollectionTask'] =
[
    // only users with this permissions allowed
    'allowed_permissions' => [],

    'params' => [
        'model_name' => 'required|filled.string',
        'models_ids' => 'array'
    ],

    'runner' => function (array $params) {

        $res = task(new DeleteModelsCollectionTask,
            [
                CU::user(),
                $params['model_name'],
                $params['models_ids'],
            ]
        );
        return $res;
    }
];

$tasks_api_requests['UpdateModelsCollectionTask'] =
[
    // only users with this permissions allowed
    'allowed_permissions' => [],

    'params' => [
        'model_name' => 'required|filled.string',
        'update_data' => 'array'
    ],

    'runner' => function (array $params) {

        $res = task(new UpdateModelsCollectionTask,
            [
                CU::user(),
                $params['model_name'],
                $params['update_data'],
            ]
        );
        return $res;
    }
];

$container['tasks_api_requests'] = $tasks_api_requests;

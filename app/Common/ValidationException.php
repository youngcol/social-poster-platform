<?php

namespace App\Common;


use function GuzzleHttp\Psr7\parse_request;

class ValidationException extends \Exception
{
    public function __construct($message, array $data=[])
    {
        $this->data = $data;
        parent::__construct($message .' >> '. $this->getDataAsString());
    }

    public function getDataAsString()
    {
        $ret = [];

        foreach ($this->data as $key => $datum)
        {
            $ret[] = strtoupper($key). ': `' .(is_array($datum) ? json_encode($datum) : $datum). '`';
        }

        return implode(' | ', $ret);
    }

    public function setDataField($key, $val)
    {
        $this->data[$key] = $val;
    }
}

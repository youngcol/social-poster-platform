<?php

namespace App\Common;


class SimpleView
{
    private $data = array();
    private $render = FALSE;

    public function __construct($template)
    {
        $file = TEMPLATES_PATH . '/' . strtolower($template) . '.php';

        if (file_exists($file)) {
            $this->render = $file;
        } else {
            throw new \Exception('Template ' . $file . ' not found!');
        }
    }

    public function assign($variable, $value)
    {
        $this->data[$variable] = $value;
        return $this;
    }

    public function render()
    {
        ob_start();
        extract($this->data);
        include($this->render);
        $ret = ob_get_contents();
        ob_end_clean();
        return $ret;
    }
}

<?php

namespace App\Common;

/**
 * Class Res - returned resource
 */

class Res
{
    private $_keys;

    public function __construct($data = [])
    {
        foreach ($data as $k => $v) {
            $this->{$k} = $v;
            $this->_keys[] = $k;
        }
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function toArray()
    {
        $ret = [];

        foreach ($this->_keys as $key) {
            $ret[$key] = $this->{$key};
        }

        return $ret;
    }

    public function ok()
    {
        return true;
    }

    public function fail()
    {
        return false;
    }
}


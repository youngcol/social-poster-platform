<?php


namespace App\Common;

/**
 * Class ResFail - returned resource on fail
 */
class ResFail
{
    private $_keys;

    public function __construct($data = [])
    {
        foreach ($data as $k => $v) {
            $this->{$k} = $v;
            $this->_keys[] = $k;
        }
    }

    public function toArray()
    {
        $ret = [];

        foreach ($this->_keys as $key) {
            $ret[$key] = $this->{$k};
        }

        return $ret;
    }

    public function ok()
    {
        return false;
    }

    public function fail()
    {
        return true;
    }
}

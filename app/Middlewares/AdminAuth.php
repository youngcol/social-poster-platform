<?php

namespace App\Middlewares;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Models\Session;
use App\Models\User;

class AdminAuth extends Middleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {

        if( !isset($_SESSION['uniqid']) ){
            $this->container->flash->addMessage('warning', 'Not logged in.');
            return redirect( 'signin');
        }

        // search the current session uniqid in the session table
        $session = Session::where('uniqid', '=', $_SESSION['uniqid'])->first();

        if( !$session ){
            $this->container->flash->addMessage('warning', 'Session not found.');
            return redirect( 'signin');
        }

        // search the current users exist for the current session uniqid
        $user = User::where('id', '=', $session->user_id)->where('active', 1)->first();

        if( !$user ){
            $this->container->flash->addMessage('danger', 'User not logged in.');
            return redirect( 'signin');
        }

        if (!$user->is_admin) {
            return redirect( 'signin');
        }

        return $next($request, $response);
    }
}

<?php

namespace App\Middlewares;

use App\Facades\session as sess;
use App\Models\Session;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use App\Models\User;

class Auth extends Middleware
{
    protected $mode = false;

    public function __construct($container, $mode='web', $context='app')
    {
        $this->context = vo($context, 'middleware.context');
        $this->mode = vo($mode, 'middleware.mode');

        parent::__construct($container);
    }

	public function __invoke(Request $request, Response $response, callable $next)
	{
	    $kitid = sp($_COOKIE, '_kitid');

	    if (eN($kitid))
	    {
            $this->container->flash->addMessage('warning', 'Not logged in.');
            sess::set('is_authorized', false);
            if ($this->mode == 'web')
            {
                return redirect('signin');
            }
            else
            {
                return json_401('Unauthorized. Please use authorisation form.');
            }
	    }

		// search the current session uniqid in the session table
		$session = Session::where('uniqid', '=', $kitid)->first();

		if( !$session ){
			$this->container->flash->addMessage('warning', 'Session not found.');
            sess::set('is_authorized', false);

            if ($this->mode == 'web')
            {
                return redirect('signin');
            }
            else
            {
                return json_401('Unauthorized. Session not found.');
            }
		}

        sess::set('is_authorized', true);

		// when php session expired and custom auth session is still working
		if (sess::get('uniqid') !== $kitid)
		{
            sess::fill([
                'current_user' => User::find($session->user_id)->schema(),
                'uniqid' => $kitid
            ]);
		}


		if ($this->context === 'admin')
		{
            $user = sess::get('current_user');

            if (!$user->is_admin) {
                return redirect( 'signin');
            }
		}

		return $next($request, $response);
	}

}

<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contact
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 *
 * @package App\Model
 */
final class Contact extends Model
{
//    use SoftDeletes;

    protected $table = 'contacts';

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'first_name',
        'last_name',
        'email',
        'phone',
    ];

    protected $casts = [
//        'social_network_id' => 'integer'
    ];

    public function contactLists()
    {
        return $this->belongsToMany(Contact::class, 'contact_list__contact', 'contact_id', 'contact_list_id');
    }
}

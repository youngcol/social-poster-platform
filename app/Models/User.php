<?php
declare(strict_types=1);
namespace App\Models;

use App\Models\SocialEngine\UserSocialAccount;

class User extends Model {

    protected $fillable = [
        'id',
        'name',
        'password',
        'permissions',
        'is_admin',
        'active',
        'email'
    ];


    public function schema()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'permissions' => $this->permissions,
            'is_admin' => $this->is_admin,
            'active' => $this->active,
            'email' => $this->email
        ];
    }


	public function encryptPassword(string $password): string
	{
		return password_hash($password, PASSWORD_DEFAULT);
	}

	public function checkPassword(string $password): bool
	{
		return password_verify($password, $this->password);
	}

    /**
     * @param string $password
     * @return $this
     */
	public function setPassword(string $password): Model
	{
		$this->password = $this->encryptPassword($password);
		return $this;
	}

    /**
     * Check user have permission
     * @param string $permission
     * @return bool
     */
    public function can(string $permission): bool
    {
        return strpos($this->permissions, $permission) !== false;
	}

    /**
     * Add permission to user
     * @param string $name
     */
    public function addPermission(string $name)
    {
        $permissions = explode(' ', $this->permissions);
        $permissions[] = strtolower($name);
        $this->permissions = implode(' ', array_unique($permissions));

        $this->save();
	}

    /**
     * Remove permission from user
     * @param string $name
     */
	public function removePermission(string $name)
    {
        $permissions = array_filter(explode(' ', $this->permissions), function ($item) use ($name) {
            return $item !== $name;
        });

        $this->permissions = implode(' ', array_unique($permissions));
        $this->save();
	}

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function socialAccount()
    {
        return $this->hasMany(UserSocialAccount::class);
	}

    /**
     * @param $networkId
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|null|object
     */
    public function getFirstSocialAccount(int $networkId)
    {
        return $this->socialAccount()->where('social_network_id', $networkId)->first();
	}
}

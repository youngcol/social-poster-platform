<?php

/**
 * Post - article to post into social engines
 */
namespace App\Models\SocialEngine;

use App\Models\Model;
use App\Models\SocialEngine\Post\PostScheduledTime;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'title',
        'text',
        'user_id',
        'is_draft'
    ];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

    public function mediaFiles()
    {
        return $this->belongsToMany(MediaFile::class, 'post_media_files', 'post_id', 'media_file_id');
    }

    public function scheduledTime()
    {
        return $this->hasOne(PostScheduledTime::class, 'post_id', 'id');
    }

}

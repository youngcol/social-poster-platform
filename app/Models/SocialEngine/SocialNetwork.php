<?php

namespace App\Models\SocialEngine;
use App\Models\Model;

class SocialNetwork extends Model
{
    protected $table = 'social_networks';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'title'
    ];

    public const PINTEREST = 1;
    public const TWITTER = 2;
    public const LINKEDIN = 3;
    public const INSTAGRAM = 4;
    public const FACEBOOK = 5;
    public const YOUTUBE = 6;

}

<?php

declare(strict_types=1);
namespace App\Models\SocialEngine\Post;
use App\Models\Model;
use App\Models\SocialEngine\Post;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PostPublishLog
 * @package App\Models\SocialEngine\Post
 */
final class PostPublishLog extends Model
{
//    use SoftDeletes;

    protected $table = 'post_publish_logs';

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'social_network_id',
        'post_id',
        'is_published',
        'posted_time',
        'publich_reason_class',
        'publich_reason_id',
    ];

    public function post()
    {
        return $this->hasOne(Post::class, 'id', 'post_id');
    }
}

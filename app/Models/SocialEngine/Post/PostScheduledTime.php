<?php

namespace App\Models\SocialEngine\Post;
use App\Contracts\PublishReasonContract;
use App\Models\Model;
use App\Models\SocialEngine\Post;
use App\Models\User;

/**
 * Class PostSheduledTime
 * @package App\Models\SocialEngine
 */
class PostScheduledTime extends Model implements PublishReasonContract
{
    protected $table = 'post_sheduled_times';

    protected $fillable = [
        'user_id',
        'post_id',
        'scheduled_time',
        'process_started_time',
        'publish_params_id',
        'process_status'
    ];

    protected $casts = [
        'scheduled_time' => 'datetime'
    ];


    public function getPostPublishParams(): PostPublishParams
    {
        return $this->publishParams;
    }

    public function getPost(): Post
    {
        return $this->post;
    }

    public function publishParams()
    {
        return $this->belongsTo(PostPublishParams::class, 'publish_params_id', 'id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}

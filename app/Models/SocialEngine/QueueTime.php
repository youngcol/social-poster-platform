<?php

declare(strict_types=1);
namespace App\Models\SocialEngine;
use App\Models\Model;
use App\Models\SocialEngine\Post\PostPublishLog;
use App\Models\SocialEngine\Post\PostQueuedPlace;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QueueTime
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $week_day
 * @property time $intraday_time
 * @property integer $user_id
 *
 * @package App\Model
 */
final class QueueTime extends Model
{
//    use SoftDeletes;

    protected $table = 'queue_times';

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'week_day',
        'intraday_time',
        'order',
        'user_id',
    ];

    public function publishedQueuedPlace()
    {
        return $this->belongsToMany(PostQueuedPlace::class, 'queue_time_published_log', 'queue_time_id', 'post_queued_place_id');
    }

    public function getTimePartsAttribute()
    {
        return explode(':', $this->intraday_time);
    }
}

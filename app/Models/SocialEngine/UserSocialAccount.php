<?php

namespace App\Models\SocialEngine;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * User attached social accounts
 *
 * Class UserSocialAccount
 * @package App\Models\SocialEngine
 */

class UserSocialAccount extends Model
{
    use SoftDeletes;

    protected $table = 'user_social_accounts';

    protected $fillable = [
        'user_id',
        'social_network_id',
        'access_token',
        'access_token_secret',
        'username',
        'social_account_id',
        'profile_image',
    ];

    protected $casts = [
        'social_network_id' => 'integer'
    ];


    public function schema()
    {
        return [
            'id' => $this->id,
            'social_network_id' => $this->social_network_id,
            'username' => $this->username,
            'profile_image' => $this->profile_image
        ];
    }


    /**
     * @param int $socialNetworkId
     * @return bool
     */
    public function isBelongsTo(int $socialNetworkId): bool
    {
        return $this->social_network_id === $socialNetworkId;
    }

    public function socialNetwork()
    {
        return $this->hasOne(SocialNetwork::class, 'id', 'social_network_id');
    }
}

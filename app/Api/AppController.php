<?php

namespace App\Api;

use App\Common\ValidationException;
use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\GetModelsCollectionTask;
use App\Tasks\SocialEngine\GetLastPostsTask;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;
use Exception;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class AppController extends Controller
{
    public function resGetPinterestAccountsBoardsTask(Request $request, Response $response, $args)
    {
        list
            (
            $err,
            $voAccountIds,
            ) = params(
            $request,
            'res.GetPinterestAccountsBoardsTask',
            [
                'accountIds' => 'ids.string'
            ]
        );

        if ($err)
        {
            $messages = map($err, function ($item) {
                return $item->getMessage();
            });
            return bad_request_error($messages);
        }

        $res = task(new GetPinterestAccountsBoardsTask,
            [
                CU::user(),
                $voAccountIds->toIds()
            ]
        );

        return json_200([
            'resGetPinterestAccountsBoardsTask' => $res
        ]);
    }

    public function resGetLastPostsTask(Request $request, Response $response, $args)
    {
        list
            (
            $err,
            $type,
            $page,
            $limit,
            ) = params(
            $request,
            'res.GetLastPostsTask',
            [
                'type' => 'post.status',
                'page' => 'numeric',
                'limit' => 'numeric',
            ]
        );

        if ($err)
        {
            $messages = map($err, function ($item) {
                return $item->getMessage();
            });
            return bad_request_error($messages);
        }

        $res = task(new GetLastPostsTask,
            [
                $type,
                $page->toInt(),
                $limit->toInt()
            ]
        );

        return json_200([
            'resGetLastPostsTask' => $res
        ]);
    }

    public function publishAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Publish',
            'page' => 'publish'
        ];

        return $this->appRender('app/index', $params, $request, $response);
    }



    /**
     * @api {get} /cu/models Get models collection
     * @apiName getCurrentUserModels
     * @apiGroup CurrentUser
     * @apiDescription Method for getting models collection
     * @apiPermission user
     *
     * @apiSuccessExample {json} Успешно (200)
     *     HTTP/1.1 200 OK
     *     {
     *       "data": [
     *         {
     *
     *         }
     *       ]
     *     }
     *
     * @apiUse StandardErrors
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     */
    public function getCurrentUserModels(Request $request, Response $response, $args)
    {
        list
            (
            $err,
            $modelName,
            $relations,
            $limit,
            $page,
            $order
            ) = params(
            $request,
            'cu.models.get',
            [
                'model_name' => 'filled.string',
                'relations' => 'filled.string',
                'limit' => 'numeric',
                'page' => 'numeric',
                'order' => 'mysql.order'
            ]
        );
        if ($err)
        {
            $messages = map($err, function ($item) {
                return $item->getMessage();
            });
            return bad_request_error($messages);
        }

        $res = task(new GetModelsCollectionTask,
            [
                CU::user(),
                $modelName,
                $relations,
                $page,
                $limit,
                $order
            ]
        );

        return json_200([
            'resGetModelsCollectionTask' => $res
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return \Slim\Http\Response
     * @throws Exception
     */
    public function runTask(Request $request, Response $response, $args)
    {
        list
            (
            $err,
            $taskName,
            $taskParams,
            ) = params(
            $request,
            'run.task',
            [
                'task_name' => 'filled.string',
                'task_params' => 'array',
            ]
        );

        if ($err)
        {
            $messages = map($err, function ($item) {
                return $item->getMessage();
            });
            return bad_request_error($messages);
        }

        // check task api request
        $taskRequest = sp($this->container['tasks_api_requests'], $taskName->_());
        if (!$taskRequest)
        {
            return bad_request_error(['No api request for this task']);
        }

        // check user permissions
        $missedPermissions = [];
        foreach ($taskRequest['allowed_permissions'] as $permission)
        {
            if (!CU::user()->can($permission))
            {
                $missedPermissions[] = $permission;
            }
        }

        if (count($missedPermissions))
        {
            return json_403([
                'permissions' => $missedPermissions
            ]);
        }

        // validate task params
        $params = $taskParams->toArray();
        $err = [];
        $voTaskParams = [];
        
        map($taskRequest['params'], function ($voRule, $field) use ($params, &$err, &$voTaskParams) {
            
            $voSplittedRules = explode('|', $voRule);
            $vo = null;

            foreach ($voSplittedRules as $voSplittedRule) {
                try
                {
                    $vo = vo(sp($params, $field), $voSplittedRule);
                }
                catch (Exception $e)
                {
                    $e->setDataField('FIELD NAME', $field);
                    $err[] = $e;
                }
            }

            $voTaskParams[$field] = $vo;
        });

        if (count($err))
        {
            return bad_request_error($err);
        }
        //-=-=-=-=-=-=-=-=-=-=-=-=

        $res = $taskRequest['runner']($voTaskParams);

        return json_200([
            'res'. $taskName => $res
        ]);
    }
}

<?php

namespace App\Api;

use App\Controllers\Controller;
use App\Facades\CU;
use App\Tasks\MediaFile\UploadMediaToLocalFolderTask;
use App\Tasks\SocialEngine\CreatePostTask;
use App\Tasks\SocialEngine\Publish\PublishToSocialAccountsTask;
use App\Tasks\SocialEngine\UpdatePostTask;
use App\VO\Post\CreationParams;
use App\VO\Post\PostInstantlyPublish;
use App\VO\Post\PublishParams;
use App\VO\User\MediaFiles;
use App\VO\User\SocialAccounts;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class SocialEngineController extends Controller
{
    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     * @throws \Exception
     */
    public function postUpdateAction(Request $request, Response $response)
    {
        list
            (
            $err,
            $text,
            $creationType,
            $scheduledTime,
            $socialAccounts,
            $postId,
            $removeMediaIds
            ) = params(
            $request,
            'post.update',
            [
                'text' => 'filled.string',
                'creation_type' => 'post.publish_params.publish_type',
                'scheduled_time' => 'datetime',
                'social_accounts' => 'ids.string',
                'post_id'   => 'numeric',
                'remove_media_ids' => 'ids.string',
            ]
        );

        $publishParams = $request->getParam('publish_params');
        $publishParams = json_decode($publishParams, 1);
        if ($scheduledTime)
        {
            $publishParams['scheduled_time'] = $scheduledTime->_();
        }
        $voPublishParams = new PublishParams($creationType, $publishParams);

//        dump($publishParams);echo 'PrototypeController.php:66'; exit;
        $mediaFiles = $this->uploadMediaIntoTempFolder($request, [
            'media1', 'media2', 'media3'
        ]);

        $voSocialAccounts = new SocialAccounts(CU::user(), $socialAccounts->toIds());

        $resUpdatePostTask = task(new UpdatePostTask,
            [
                $postId->toInt(),
                CU::user(),
                $text,
                $mediaFiles,
                $voSocialAccounts,
                $voPublishParams,
                $removeMediaIds
            ]
        );


        // public post
        $resPublishToSocialAccountsTask = null;
        if ($creationType == 'instantly')
        {
            $resPublishToSocialAccountsTask = task(new PublishToSocialAccountsTask,
                [
                    new PostInstantlyPublish(
                        $resUpdatePostTask->post,
                        $resUpdatePostTask->postPublishParams
                    )
                ]
            );
        }

        return json_200([
            'resPublishToSocialAccountsTask' => $resPublishToSocialAccountsTask,
            'resCreatePostTask' => $resUpdatePostTask
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     * @throws \Exception
     */
    public function postStoreAction(Request $request, Response $response)
    {
        list
            (
            $err,
            $text,
            $creationType,
            $scheduledTime,
            $socialAccounts,
            ) = params(
            $request,
            'post.store',
            [
                'text' => 'filled.string',
                'creation_type' => 'post.publish_params.publish_type',
                'scheduled_time' => 'datetime',
                'social_accounts' => 'filled.string'
            ]
        );

        $publishParams = $request->getParam('publish_params');
        $publishParams = json_decode($publishParams, 1);
        if ($scheduledTime)
        {
            $publishParams['scheduled_time'] = $scheduledTime->_();
        }
        $publishParams = new PublishParams($creationType, $publishParams);
//        dump($publishParams);echo 'PrototypeController.php:66'; exit;

        $mediaFiles = $this->uploadMediaIntoTempFolder($request, [
            'media1', 'media2', 'media3'
        ]);

        $mediaIds = map($mediaFiles, function ($item) {
            return $item->id;
        });

        $socialAccounts = new SocialAccounts(CU::user(), explode(',', $socialAccounts));

        $res = task(new CreatePostTask,
            [
                CU::user(),
                $text,
                new MediaFiles(CU::user(), $mediaIds),
                $socialAccounts,
                $publishParams
            ]
        );

        // public post
        $resPublishToSocialAccountsTask = null;
        if ($creationType == 'instantly')
        {
            $resPublishToSocialAccountsTask = task(new PublishToSocialAccountsTask(),
                [
                    new PostInstantlyPublish(
                        $res->post,
                        $res->postPublishParams
                    )
                ]
            );
        }

        return json_200([
            'resPublishToSocialAccountsTask' => $resPublishToSocialAccountsTask,
            'resCreatePostTask' => $res
        ]);
    }


    public function uploadMediaFile(Request $request, Response $response)
    {
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile1 = $uploadedFiles['media1'];

        $Res = task(new UploadMediaToLocalFolderTask(), [$uploadedFile1]);

        if ($Res->ok)
        {
            return json_200([

            ]);
        } else {
            return json_400([

            ]);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $accountId
     * @return \Slim\Http\Response
     */
    public function deleteUserSocialAccountAction(Request $request, Response $response, $accountId)
    {
        $id = $request->getParam('id');
        $socialAccount = CU::user()->socialAccount()->where('id', $id)->first();
        $del = $socialAccount->delete();

        return json_200([
            'del' => $del
        ]);
    }
}

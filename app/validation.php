<?php
/*
 * Validation rules for val() function
 *
 * https://github.com/rakit/validation#available-rules
 */

use App\Models\SocialEngine\SocialNetwork;

$val = [
    'filled.string' => 'min:1',
    'datetime' => 'date:Y-m-d H:i:s',
    'time' => 'date:H:i',
    'array' => 'array',
    'render.post.type' => 'in:middle_cart',
    'middleware.context' => 'in:app,admin',
    'middleware.mode' => 'in:web,api',

    'post.status' => 'in:sent,draft,failed,queued',
    'post.publish_params.publish_type' => 'in:scheduled,queued,instantly,draft',
    'post.publish_params.keys' => 'in:accounts,scheduled_time',

    'user.username' => 'min:6',
    'user.email' => 'email',
    'user.password' => 'min:6',

    'pinterest.oauth.scopes' => 'in:read_public,write_public,read_relationships,write_relationships',
    'mediafile.type' => 'in:image,video',

//    'mediafile.type2' => function($type) {
//
//    },

    'queue.week_day' => 'in:monday,tuesday,wednesday,thursday,friday,saturday,sunday',

    'mysql.order' => 'in:asc,desc',
];

$val['ids.string'] = function ($type) {
    $ids = explode(',', $type);

    foreach ($ids as $id)
    {
        if ($id != (int)$id)
        {
            return false;
        }
    }
};


/**
 * Validations requests for controllers input
 */
$val_request = [
    'user.signin' => [
        'username' => 'required',
        'password' => 'required',
        'remember_me' => '',
    ],
    'user.signup' => [
        'email' => 'required',
        'username' => '',
        'password' => 'required'
    ],

    'res.GetPinterestAccountsBoardsTask' => [
        'accountIds' => 'required',
    ],
    'res.GetLastPostsTask' => [
        'type' => 'required',
        'page' => 'required',
        'limit' => 'required',
    ],

    'post.update' => [
        'creation_type' => 'required',
        'social_accounts' => 'required',
        'post_id'=>'required',
        'text' => '',
        'media1' => '',
        'media2' => '',
        'media3' => '',
        'scheduled_time' => '',
        'remove_media_ids' => '',
    ],

    'post.store' => [

        'text' => '',
        'media1' => '',
        'media2' => '',
        'media3' => '',
        'creation_type' => 'required',
        'social_accounts' => 'required',
        'scheduled_time' => '',
    ],

    'user.password.change' => [
        'pass1' => 'required',
        'pass2' => 'required',
        'code' => 'required',
    ],

    'cu.models.get' => [
        'model_name' => 'required',
        'relations' => '',
        'limit' => 'required',
        'page' => 'required',
        'order' => 'required'
    ],

    'run.task' => [
        'task_name' => 'required',
        'task_params' => 'required',
    ],
];


/**
 * List of dictionary models that are accessable from api
 */
$dictionary_models = [
    //    'App\Models\SocialEngine\SocialNetwork'
    'SocialNetwork' => SocialNetwork::class,

];

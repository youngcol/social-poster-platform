<?php

declare(strict_types=1);

use App\Common\Res;
use App\Common\ValidationException;
use App\Common\ValidationRuleException;
use App\Repositories\Repository;
use Carbon\Carbon;
use \Psr\Http\Message\ResponseInterface as Response;
use Rakit\Validation\ErrorBag;
use Ramsey\Uuid\Uuid;
use Slim\Http\Request;

/**
 * @param $text
 * @return bool|null|string|string[]
 */
function slugify($text)
{
	// replace non letter or digits by -
	$text = preg_replace('~[^\pL\d]+~u', '-', $text);

	// transliterate
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	// remove unwanted characters
	$text = preg_replace('~[^-\w]+~', '', $text);

	// trim
	$text = trim($text, '-');

	// remove duplicate -
	$text = preg_replace('~-+~', '-', $text);

	// lowercase
	$text = strtolower($text);

	if (empty($text)) {
		return 'n-a';
	}

	return $text;
}

/**
 * @return false|string
 */
function now()
{
	return date('Y-m-d H:i:s');
}

/**
 * @param $route
 * @param array $params
 * @return mixed
 */
function route($route, $params=[])
{
    global $app;
    return $app->getContainer()->get('router')->pathFor($route, $params);
}

function build_get_route()
{

}

function url($route, $params=[], $https=false)
{
    return ($https ? 'https://' : 'http://') . settings('host') . route($route, $params);
}

function uploads_url($filename, $https=false)
{
    return ($https ? 'https://' : 'http://') . settings('host') . '/uploads/' . $filename;
}

/**
 * Run task
 *
 * @param $task
 * @param $args
 * @return \App\Common\ResFail
 * @throws Exception
 */
function task($task, $args)
{
    try
    {
        return $task->run(...$args);
    }
    catch (\Exception $e) {

        handle_task_exception($e);

        return resFail('last_tier_task_handle_exceptions', [
            'e' => [
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ],
        ]);
    }
}

/**
 * Save param from array
 *
 * @param $array
 * @param $key
 * @param null $default
 * @return null
 */
function sp($array, $key, $default=null)
{
    return isset($array[$key]) ? $array[$key] : $default;
}

/**
 *
 * @param $value
 * @param $ruleName
 * @param null $arg1
 * @param null $arg2
 * @return Res
 * @throws ValidationException
 */
function val($value, $ruleName, $arg1=null, $arg2=null)
{
    global $val;

//    check(!isset($val[$ruleName]), "Rule name `$ruleName` doesn't exists in validation.php");
    
    if (isset($val[$ruleName]) and is_callable($val[$ruleName])) {
        if ($val[$ruleName]($value, $arg1, $arg2) === false) {
            throw new ValidationException();
        }
    } else {

        $validator = new Rakit\Validation\Validator();

//        $validator->setMessages([
//            'required' => ':attribute harus diisi',
//            'email' => ':email tidak valid',
//            // etc
//        ]);
//        dump($ruleName, [sp($val, $ruleName, $ruleName)]);echo 'helpers.php:133'; exit;
        $validation = $validator->make(
            [
                'val_value' => $value
            ],
            [
                'val_value' => sp($val, $ruleName, $ruleName)
            ]
        );

        $validation->validate();

        if ($validation->fails())
        {
            $errors = $validation->errors();

            throw new ValidationException(
                'Value object error',
                [
                    'CURRENT VALUE' => $value,
                    'RULE' => $ruleName,
                    'ERRORS' => $errors->all(),
                ]
            );
        }
    }
    return new Res(['valRule' => sp($val, $ruleName, $ruleName)]);
}

/**
 * Build value object
 *
 * @param $val
 * @param string $valRule
 * @return \App\VO\VoVal
 * @throws ValidationException
 */
function vo($val, $valRule='filled.string')
{
    $valRes = val($val, $valRule);
    return new \App\VO\VoVal($val, $valRule, $valRes->valRule);
}

/**
 * @param Request $r
 * @param $reqName
 * @throws ValidationException
 */
function val_request(Request $r, $reqName)
{
    global $val_request;

    $rules = sp($val_request, $reqName);
    if (eN($rules)) {
        throw new ValidationException("No such key in val_request: `$reqName`");
    }

    $validator = new Rakit\Validation\Validator();

//        $validator->setMessages([
//            'required' => ':attribute harus diisi',
//            'email' => ':email tidak valid',
//            // etc
//        ]);

    $requestParams = $r->getParams(array_keys($rules));
    $validation = $validator->make($requestParams, $rules);
    $validation->validate();

//    new ErrorBag();

    if ($validation->fails()) {
        $errors = $validation->errors();

//        $err = map($errors->toArray(), function ($item, $key) {
//            return '`'.$key. '` -> '. implode(" | ",$item);
//        });

        $log = [
            'request' => $requestParams,
            'ruleName' => $reqName,
            'rules' => $rules,
            'errors' => $errors->toArray(),
        ];

        throw new ValidationException('Request validation ', $log);
    }
}

function map($arr, $func) {
    $ret = [];

    foreach ($arr as $key => $item) {
        $ret[] = $func($item, $key);
    }

    return $ret;
}

/**
 * Equal null
 * @param $val
 * @return bool
 */
function eN($val)
{
    return $val === null;
}

/**
 * Equal false
 * @param $val
 * @return bool
 */
function eF($val)
{
    return $val === false;
}

/**
 * @param $csrf
 */
function csrf(array $csrf)
{
    echo '<input type="hidden" name="'.$csrf['nameKey'].'" value="'.$csrf['name'] .'">';
    echo '<input type="hidden" name="'.$csrf['valueKey'].'" value="'.$csrf['value'] .'">';
}

/**
 * @param Response $response
 * @param $route
 * @return Response
 */
function redirect(string $route, array $params = [])
{
    $response = new \Slim\Http\Response();
    return $response->withStatus(302)->withHeader('Location', route($route, $params));
}

function redirect_url($url)
{
    $response = new \Slim\Http\Response();
    return $response->withStatus(302)->withHeader('Location', $url);
}

/**
 * Log warn event
 *
 * @param string $message
 * @param array $data
 */
function log_warn(string $message, array $data=[])
{
    $user = sp($_SESSION, 'current_user');
    $userId = sp($user, 'id');
    $userName = sp($user, 'name');
    $time = date('r');
    $url = sp($_SERVER, 'REQUEST_URI');

    $log = [
        " ",
        " ",
        "======================",
        "USER: $userName ID: $userId TIME: $time",
        "URL: ". $url,
        "GET:". json_encode($_GET, JSON_PRETTY_PRINT),
        "POST:". json_encode($_POST, JSON_PRETTY_PRINT),
        '~~~~~~~~~~~~~~~~~~~~~~',
        '',
        $message,
        json_encode($data, JSON_PRETTY_PRINT),
        "~~~~~~~~~~~~~~~~~~~~~~",
    ];


    file_put_contents(
        PATH_ROOT. '/logs/warn.log',
        implode("\n", $log) . "\n\n",
        FILE_APPEND
    );
}

/**
 * Логирование по тэгам
 *
 * @param $fileName
 * @param $title
 * @param null $data
 */
function log_tag($fileName, $title, $data = null)
{
    $time = date('r');

    $content = [
        $time,
        $title
    ];

    if ($data)
    {
        $content[] = serialize($data);
    }

    file_put_contents(
        PATH_ROOT. "/logs/tag__$fileName.log",
        implode("\n", $content) . "\n\n",
        FILE_APPEND
    );
}

/**
 * Log error event
 *
 * @param string $message
 * @param array $data
 */
function log_err(string $message, array $data=[])
{
    $user = sp($_SESSION, 'current_user');
    $userId = sp($user, 'id');
    $userName = sp($user, 'name');
    $time = date('r');
    $url = sp($_SERVER, 'REQUEST_URI');

    $log = [
        "",
        "======================",
        "USER: $userName ID: $userId TIME: $time",
        "URL: ". $url,
        "GET:". json_encode($_GET, JSON_PRETTY_PRINT),
        "POST:". json_encode($_POST, JSON_PRETTY_PRINT),
        '~~~~~~~~~~~~~~~~~~~~~~',
        '',
        $message,
        json_encode($data, JSON_PRETTY_PRINT),
        "~~~~~~~~~~~~~~~~~~~~~~",
    ];


    file_put_contents(
        PATH_ROOT. '/logs/err.log',
        implode("\n", $log) . "\n\n",
        FILE_APPEND
    );
}

/**
 * @param Exception $e
 */
function log_exception($e)
{
    $log = [
        date('r'),
        jTraceEx($e)
    ];

    $sep = "\n\n=======================================\n\n";

    file_put_contents(
        PATH_ROOT. 'logs/exceptions.log',
        implode("\n", $log) . $sep,
        FILE_APPEND
    );
}

function json_200($data)
{
    $response = new \Slim\Http\Response();
    return $response->withJson([
        'data' => $data
    ], 200);
}

function json_201($data)
{
    $response = new \Slim\Http\Response();
    return $response->withJson([
        'data' => $data
    ], 201);
}

/**
 * https://code-maze.com/top-rest-api-best-practices/
 *
 * Bad request
 *
 * @param $message
 * @param $data
 * @return \Slim\Http\Response
 */
function json_400($data)
{
    $response = new \Slim\Http\Response();
    return $response->withJson([
        'error' => 'Invalid payload',
        'details' => $data
    ], 400, JSON_PRETTY_PRINT);
}

/**
 * Forbidden
 * Was the user correctly authenticated, but they don’t have the required permissions to access the resource? 👉 403 Forbidden.
 * @param $message
 * @param $data
 * @return \Slim\Http\Response
 */
function json_403($data=[])
{
    $response = new \Slim\Http\Response();
    return $response->withJson([
        'message' => 'Current user don\'t have the required permissions',
        'data' => $data
    ], 403, JSON_PRETTY_PRINT);
}

/**
 * @param $message
 * @param $data
 * @return \Slim\Http\Response
 */
function json_404($message, $data)
{
    $response = new \Slim\Http\Response();
    return $response->withJson([
        'message' => $message,
        'data' => $data
    ], 400, JSON_PRETTY_PRINT);
}


/**
 * 401 Unauthorized	The requested page needs a username and a password.
 *
 * @param $message
 * @param $data
 * @return \Slim\Http\Response
 */
function json_401($message, $data=[])
{
    $response = new \Slim\Http\Response();
    return $response->withJson([
        'message' => $message,
        'data' => $data
    ], 401, JSON_PRETTY_PRINT);
}

/**
 * Api bad request error
 *
 * @param array $errors
 * @return \Slim\Http\Response
 */
function bad_request_error($errors=[])
{
    $response = new \Slim\Http\Response();
    return $response->withJson([
        'message' => 'Bad request',
        'errors' => $errors
    ], 400, JSON_PRETTY_PRINT);
}

/**
 * Get settings key from settings.php file
 *
 * @param $k
 * @return mixed
 * @throws Exception
 */
function settings($k)
{
    global $settings;
    $keys = explode('.', $k);
    $ret = $settings['settings'];
    $passedKeys = [];

    foreach ($keys as $key) {

        $passedKeys[] = $key;

        if (!isset($ret[$key])) {
            $passed = implode('.', $passedKeys);
            throw new Exception("No such key in settings.php: '$passed'");
        }

        $ret = $ret[$key];
    }

    return $ret;
}

/**
 * @param Request $r
 * @param $keys
 * @return array
 */
function params(\Psr\Http\Message\ServerRequestInterface $r, $reqVal, $rules)
{
    $err = [];

    try
    {
        val_request($r, $reqVal);
    }
    catch (Exception $e)
    {
        if (settings('env') === 'dev')
        {
            throw $e;
        }

        $err[] = $e;
    }

    $ret = map($rules, function ($voRule, $field) use ($r, &$err) {
        $requestValue = $r->getParam($field);

        try
        {
            $vo = vo($requestValue, $voRule);
            return $vo;
        }
        catch (Exception $e)
        {
            if (settings('env') === 'dev')
            {
                throw $e;
            }

            $err[] = $e;
        }
    });

    if (count($err))
    {
        $ret = array_fill(0, count($rules) + 1, null);
        $ret[0] = $err;
    }
    else
    {
        array_unshift($ret, null);
    }

    return $ret;
}

/**
 * Redirect back to previous page
 *
 * @param $err
 * @return \Slim\Http\Response
 */
function back_with_errors($exceptions)
{
    $list = [];
    foreach ($exceptions as $exception)
    {
        foreach ($exception->data['errors'] as $field => $error)
        {
            $list[$field] = array_values($error);
        }
    }

    $_SESSION['last_bad_request_errors'] = $list;

    return redirect_url($_SESSION['last_requests'][0]);
}

/**
 * @param $folderPath
 */
function create_folder_tree_by_pathname($folderPath)
{
    $folderPath = str_replace('\\','/', $folderPath);
    $folders = explode('/', $folderPath);
    if($folderPath[0] == '/')
    {
        $path = '/';
    }
    else
    {
        $path = '';
    }

    foreach ($folders as $folder)
    {
        if(strlen($folder))
        {
            $path .= $folder. '/';

            if(!file_exists($path))
            {
                mkdir($path);
            }
        }
    }
}

/**
 * Generate uuid-like string from id
 *
 * @param $id
 * @return string
 * @throws Exception
 */
function uuid1FromId($id): string
{
    $uuid1 = Uuid::uuid1();
    $uuid1->toString();

    $appKey = settings('key');
    $idMd5 = md5((string)$id);
    $keyMd5 = md5($appKey . $idMd5);

    return implode('-', [
        $uuid1->toString(),
        substr($idMd5, 3, 4),
        substr($keyMd5, 6, 4),
        substr($idMd5, 17, 4),
        substr($keyMd5, 17, 4)
    ]);
}

function files($folder)
{
    $files = scandir($folder);
    array_shift($files);
    array_shift($files);
    return $files;
}

/**
 * @param $message
 * @param array $data
 * @throws \App\Common\FlowException
 */
function flow_exception($message, $data=[])
{
    throw new \App\Common\FlowException($message, json_encode($data));
}

/**
 *
 * @param $assertion
 * @param $message
 * @param array $data
 * @throws \App\Common\FlowException
 */
function check($assertion, $message, $data=[])
{
    if ($assertion === true) {
        throw new \App\Common\FlowException($message, json_encode($data));
    }
}

/**
 * @param $assertion
 * @param $message
 * @param array $data
 * @throws \App\Common\FlowException
 */
function check_false($assertion, $message, $data=[])
{
    if ($assertion !== false) {
        throw new \App\Common\FlowException($message, json_encode($data));
    }
}

/**
 * @param $assertion
 * @param $message
 * @param array $data
 * @throws \App\Common\FlowException
 */
function check_null($assertion, $message, $data=[])
{
    if ($assertion !== null) {
        throw new \App\Common\FlowException($message, json_encode($data));
    }
}

/**
 * @param $assertion
 * @param $message
 * @param array $data
 * @throws \App\Common\FlowException
 */
function check_not_null($assertion, $message, $data=[])
{
    if ($assertion === null) {
        throw new \App\Common\FlowException($message, json_encode($data));
    }
}

/**
 * @param Exception $e
 * @throws Exception
 */
function handle_task_exception(Exception $e)
{
    if (settings('env') === 'dev')
    {
        throw $e;
    }
    else
    {
        log_exception($e);
    }
}

/**
 * @param $code
 * @param array $data
 * @return \App\Common\ResFail
 */
function resFail($code, $data=[])
{
    $data['_status'] = 'failed';
    $data['_code'] = $code;
    return new \App\Common\ResFail($data);
}

/**
 * @param Carbon $date
 * @return mixed
 */
function day_of_week(Carbon $date)
{
    $days = [
        0 => 'sunday',
        1 => 'monday',
        2 => 'tuesday',
        3 => 'wednesday',
        4 => 'thursday',
        5 => 'friday',
        6 => 'saturday',
    ];

    return $days[$date->dayOfWeek];
}


/**
 * @return Carbon
 */
function now_date($tz='Etc/GMT+0')
{
    return Carbon::now($tz);
}

/**
 * @param Carbon $date
 * @return string
 */
function date_mysql_format(Carbon $date)
{
    return $date->format('Y-m-d H:i:s');
}


/**
 * Printf с параметрами в виде массива
 *
 * sprintf2('[v] and [v2]', array('v'=> '11', 'v2'=>'22'))
 *
 * @param $s
 * @param array $data
 * @return mixed
 */
function sprintf2($s, $data=array())
{
    $loop = 3;
    $ret = $s;

    $keys = array_map(function($v){
        return '['.$v.']';
    }, array_keys($data));

    do
    {
        $ret = str_replace($keys, $data, $ret);
        $is_need_again = !eF(strpos($ret, '['));

    }while($loop-- && $is_need_again);

    return $ret;
}


/**
 * Run
 *
 * @param $cmd
 * @return array
 */
function run_exec_async($cmd)
{
    $tmpl_str = "$cmd > /dev/null &";
//    $tmpl_str = "$cmd";

    $output = array();
    $return_var = array();

    $ret = exec($tmpl_str, $output, $return_var);

    return array(
        'ret'           => $ret,
        'output'        => $output,
        'return_var'    => $return_var,
        'exec_str'      => $tmpl_str
    );
}

/**
 * @param $commandName
 * @param array $params
 * @return array
 */
function run_partisan_async($commandName, $params=[])
{
    $paramsString = '';
    foreach ($params as $key => $val)
    {
        $paramsString .= sprintf2('--[k]="[v]" ',
            array('k' => $key, 'v' => $val));
    }

    $c = "php ../partisan $commandName $paramsString";

    return run_exec_async($c);
}

/**
 * Run partisan command
 *
 * @param $commandName
 * @param array $params
 * @return array
 */
function run_partisan($commandName, $params=[])
{
    $paramsString = '';
    foreach ($params as $key => $val)
    {
        $paramsString .= sprintf2('--[k]="[v]" ',
            array('k' => $key, 'v' => $val));
    }

    $c = "php ../partisan $commandName $paramsString";

    $output = array();
    $return_var = array();

    $ret = exec($c, $output, $return_var);

    return array(
        'ret'           => $ret,
        'output'        => $output,
        'return_var'    => $return_var,
        'exec_str'      => $c
    );
}

/**
 * Get uploads path
 */
function uploads_path($filename)
{
    return UPLOADS_PATH . DIRECTORY_SEPARATOR . $filename;
}

/**
 * @param $tmpl
 * @param array $data
 * @return mixed
 */
function render($tmpl, $data=[])
{
    global $container;
    return $container->view2->fetch($tmpl, $data);
}

/**
 * @param $title
 * @param $url
 * @param $pageName
 * @param array $submenu
 * @param $currentPage
 * @param $currentSubmenu
 * @return mixed
 */
function menu_item($title, $url, $pageName, $submenu=[], $currentPage, $currentSubmenu)
{
    global $container;
    $container['menu.current_page'] = $currentPage;
    $container['menu.current_submenu'] = $currentSubmenu;
    $container["menu.page.$pageName.submenu"] = $submenu;

    return render('app/menu_item', [
        'title'     => $title,
        'url'       => $url,
        'page_name' => $pageName,
        'submenu'   => $submenu,
        'current_page' => $currentPage,
        'current_submenu' => $currentSubmenu
    ]);
}

/**
 * @param $currentSubmenu
 * @return mixed|string
 */
function render_submenu($currentPage, $currentSubmenu)
{
    global $container;

//    if (eN($currentSubmenu))
//    {
//        return '';
//    }

    $submenu = null;
    if (isset($container["menu.page.$currentPage.submenu"]))
    {
        $submenu = $container["menu.page.$currentPage.submenu"];
    }

    if (!$submenu)
    {
        return '';
    }

    return render('app/submenu', [
        'current_submenu'   => $currentSubmenu,
        'submenu'           => $submenu,
    ]);
}

/**
 * @param $arr
 * @param $key
 * @param array $def
 */
function array_create_key(&$arr, $key, $def = [])
{
    if (eN(sp($arr, $key)))
    {
        $arr[$key] = $def;
    }
}

/**
 * Get default repo for model
 *
 * @param $class
 * @return Repository
 */
function repo($class)
{
    return new Repository($class);
}

/**
 * Get locale string
 *
 * @param $route
 * @return mixed
 */
function L($route)
{
    global $container;
    return $container['cu_language']->o($route);
}

/**
 * Get list of all keys in array recursivly
 * @param $array
 * @param int $level
 * @return array
 */
function array_key_routes_recursive($array, $level = 1){
    $ret = [];

    foreach($array as $key => $value){
        //If $value is an array.
        if(is_array($value)){
            //We need to loop through it.
            $rec = array_key_routes_recursive($value, $level + 1);
            foreach ($rec as $item) {
                $ret[] = $key. '.'.$item;
            }

        } else{
            //It is not an array, so print it out.
            $ret[] = $key;
        }
    }

    return $ret;
}

/**
 * Get static file version
 *
 * @param $fileUrl
 * @return string
 */
function mix($fileUrl)
{
    $version = 'v';
    if (file_exists(HTDOC_PATH. $fileUrl))
    {
        $version .= filemtime(HTDOC_PATH. $fileUrl);
    }

    return $fileUrl . "?$version=1";
}

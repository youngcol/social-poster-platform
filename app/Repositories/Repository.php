<?php
declare(strict_types=1);
namespace App\Repositories;

/**
 * Different repository methods with Eloquent model
 *
 * Class Repository
 * @package App\Repositories
 */
class Repository
{
    protected $model;
    public $querySql;
    protected $currentQuery = null;


    public function __construct(string $modelClass)
    {
        $this->model = new $modelClass;
    }

    /**
     * Resets current query
     */
    public function resetQuery()
    {
        $this->currentQuery = null;
    }

    private function getCurrentQuery()
    {
        if (eN($this->currentQuery)) {
            $this->currentQuery = ($this->model)::query();
        }

        return $this->currentQuery;
    }

    /**
     * Model find method
     * @param $ids
     * @return mixed
     */
    public function find($ids)
    {
        return $this->model->find($ids);
    }

    /**
     * Model all method
     * @return mixed
     */
    public function all()
    {
        return $this->model->all();
    }


    /**
     * Get first record
     * @param null $select
     * @return mixed
     */
    public function first($select=null)
    {
        $q = $this->getCurrentQuery();

        if ($select)
        {
            $s = explode(',', $select);
            $q->select($s);
        }

        $this->buildQueryLoq();

        $ret = $q->first();
        $this->resetQuery();
        return $ret;

    }

    /**
     * Get rows collection
     *
     * @param array $where
     * @param array $with
     * @param array $orderBy
     * @return mixed
     */
    public function get($select=null)
    {
        $q = $this->getCurrentQuery();

        if ($select)
        {
            $s = explode(',', $select);
            $q->select($s);
        }

        $this->buildQueryLoq();
        $ret = $q->get();
        $this->resetQuery();

        return $ret;
    }

    public function toSql()
    {
        $this->buildQueryLoq();
        return $this->querySql;
    }


    /**
     * Pagination for select queries
     * @param int $page
     * @param int $limit
     * @return $this
     */
    public function page($page = 0, $limit = 100)
    {
        $q = $this->getCurrentQuery();

        $q->limit($limit);
        $q->offset($page * $limit);

        return $this;
    }

    /**
     *
     * @param $where
     * @param null $opt
     *
     * examples
     *  where('key','val') `key = val`
     *  where('key<=','val') `key <= val`
     *  where(['key>=' => 'val']) `key>=val`
     * @return $this
     */
    public function where($where, $opt=null)
    {
        $q = $this->getCurrentQuery();

        if (eN($opt))
        {
            foreach ($where as $key => $value)
            {
                $w = $this->getWhereOperators($key, $value);
                $q->where($w[0], $w[1], $w[2]);
            }
        }
        else
        {
            $w = $this->getWhereOperators($where, $opt);
            $q->where($w[0], $w[1], $w[2]);
        }

        return $this;
    }

    private function getWhereOperators($key, $value)
    {
        $operator = '=';
        $l = strlen($key)-1;

        if ($key[$l] == '<')
        {
            $operator = '<';
        }
        elseif($key[$l] == '>')
        {
            $operator = '>';
        }
        elseif($key[$l-1] == '<' and $key[$l] == '=')
        {
            $operator = '<=';
        }
        elseif($key[$l-1] == '>' and $key[$l] == '=')
        {
            $operator = '>=';
        }

        return [trim($key, '<>='), $operator, $value];
    }

    public function orWhere($where=[], $isSubquery=false)
    {
        $q = $this->getCurrentQuery();

        if ($isSubquery)
        {
            $q->where(function ($query) use ($where) {
                foreach ($where as $key => $value)
                {
                    $w = $this->getWhereOperators($key, $value);
                    $query->where($w[0], $w[1], $w[2]);
                }
            });
        }
        else
        {
            foreach ($where as $key => $value) {
                $q->orWhere($key, '=', $value);
            }
        }


        return $this;
    }


    public function groupBy()
    {
        $groupBy = func_get_args();
        $q = $this->getCurrentQuery();

        foreach ($groupBy as $val) {
            $q->groupBy($val);
        }

        return $this;
    }
    /**
     * @param array $orderBy
     * @return $this
     */

    public function orderBy($orderBy=[])
    {
        $q = $this->getCurrentQuery();

        foreach ($orderBy as $field => $type) {
            $q->orderBy($field, $type);
        }

        return $this;
    }

    /**
     * With method of the model
     * with('other','some')
     * @return $this
     */
    public function with()
    {
        $with = func_get_args();
        $q = $this->getCurrentQuery();

        foreach ($with as $item) {
            $q->with($item);
        }
        return $this;
    }


    protected function buildQueryLoq()
    {
        $sql = $this->currentQuery->toSql();
        $bindings = $this->currentQuery->getBindings();

        $addSlashes = str_replace('?', "'?'", $sql);
        $fullSql = vsprintf(str_replace('?', '%s', $addSlashes), $bindings);

        $this->querySql = [$sql, $bindings, $fullSql];
    }

    public function distinct()
    {
        $q = $this->getCurrentQuery();
        $q->distinct();
        return $q;
    }

    public function delete($ids)
    {
        $q = $this->getCurrentQuery();
        $q->whereIn('id', $ids);
        $this->buildQueryLoq();

        $ret = $q->delete($ids);
        $this->resetQuery();
        return $ret;
    }

    public function update($data)
    {
        $q = $this->getCurrentQuery();
        $this->buildQueryLoq();

        $ret = $q->update($data);
        $this->resetQuery();
        return $ret;
    }

}

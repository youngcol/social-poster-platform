<?php

define('COMMANDS_PATH',      PATH_ROOT . 'app/Console/Commands');
define('CODE_TEMPLATE_PATH', PATH_ROOT . 'app/Console/CodeTemplates');
define('MIGRATIONS_PATH',    PATH_ROOT . 'db/migrations');
define('SEEDS_PATH',         PATH_ROOT . 'db/seeds');
define('LOGS_PATH',          PATH_ROOT . 'logs');
define('HTDOC_PATH',         PATH_ROOT . 'htdocs');
define('TEMPLATES_PATH',     PATH_ROOT . 'templates');
define('MODELS_PATH',        PATH_ROOT . 'app/Models');
define('TASKS_PATH',         PATH_ROOT . 'app/Tasks');
define('VO_PATH',            PATH_ROOT . 'app/VO');
define('UPLOADS_PATH',       PATH_ROOT . 'htdocs/uploads');
define('TEMP_UPLOADS_PATH',  PATH_ROOT . 'temp/uploads');
define('APP_PATH',           PATH_ROOT . 'app');
define('DB_PATH',            PATH_ROOT . 'db');




ini_set('session.entropy_length', 128);
ini_set('session.entropy_file', '/dev/urandom');
ini_set('session.hash_function', 'sha256');
ini_set('session.hash_bits_per_character', 5);


//define('CACHE_PATH',          __DIR__ . '/cache' );
//define('PUBLIC_PATH',          __DIR__ . '/public' )
//define('SCHEMAS_PATH',       __DIR__ . '/app/src/Schema');
//define('CONFIG_PATH',        __DIR__ . '/config');
//define('APP_PATH',           __DIR__ . '/app');
//define('APP_SRC_PATH',       __DIR__ . '/app/src');
//define('MODULES_PATH',       __DIR__ . '/app/modules');
//define('VIEWS_PATH',         __DIR__ . '/app/resources/views');


require PATH_ROOT . 'vendor/autoload.php';

require PATH_ROOT . 'app/bootstrap_dependencies.php';
require PATH_ROOT . 'app/dependencies.php';
require PATH_ROOT . 'app/validation.php';
$app = new \Slim\App($container);

require PATH_ROOT . 'app/middlewares.php';
require PATH_ROOT . 'app/routes.php';
require PATH_ROOT . 'app/routes_api.php';
require PATH_ROOT . 'app/tasks_api_requests.php';

if (settings('env') === 'dev')
{
    require PATH_ROOT . 'app/routes_prototype.php';
}


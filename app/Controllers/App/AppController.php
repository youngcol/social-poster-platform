<?php

namespace App\Controllers\App;

use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\UserSocialAccount;
use App\Models\User;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rakit\Validation\Rules\Uppercase;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Finder\Finder;

class AppController extends Controller
{

    public function templatesAction(Request $request, Response $response, $args)
    {
        $accounts = UserSocialAccount::where('user_id', CU::user()->id)->get();

        $params = [
            'accounts' => $accounts,
            'title' => 'Templates',
            'page' => 'templates'
        ];

        return $this->appRender('app/templates', $params, $request, $response);
    }


    /**
     * Show vue stories
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function vueStoriesAction(Request $request, Response $response, $args)
    {
        $finder = new Finder();
        $finder->directories()->in(TEMPLATES_PATH.'/vue_stories');

        $folders = map($finder, function ($item) {

            $folder = basename($item->getPathname());

            $finder2 = new Finder();
            $files = $finder2->files()->in(TEMPLATES_PATH.'/vue_stories/'. $folder);

            $stories = map($files, function ($item) {
                return ucfirst($item->getBasename('.blade.php'));
            });

            return [
                'folder' => $folder,
                'stories' => $stories
            ];
        });

        $folder = $request->getAttribute('folder');
        $story = $request->getAttribute('story');

        $tmpl = 'vue_stories/index';
        $storyHtml = null;
        if ($folder)
        {
            $storyHtml = render('vue_stories/'. $folder . '/'. strtolower($story));
        }

        $params = [
            'title' => 'Vue stories',
            'page' => 'vue_stories',
            'folders' => $folders,
            'current_folder' => $folder,
            'current_story' => $story,
            'storyHtml' => $storyHtml
        ];

        return $this->appRender($tmpl, $params, $request, $response);
    }


    public function publishAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Publish',
            'page' => 'publish'
        ];

        return $this->appRender('app/index', $params, $request, $response);
    }

    public function postsAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Publish',
            'page' => 'posts'
        ];

        return $this->appRender('app/index', $params, $request, $response);
    }

    public function indexAction(Request $request, Response $response, $args)
    {
        $accounts = UserSocialAccount::where('user_id', CU::user()->id)->get();

        $params = [
            'accounts' => $accounts,
            'title' => 'Dashboard',
            'page' => 'dashboard',
            'submenu' => 'queue',
        ];

        return $this->appRender('app/index', $params, $request, $response);
    }

    public function accountsAction(Request $request, Response $response, $args)
    {
        $accounts = UserSocialAccount::where('user_id', CU::user()->id)->orderBy('social_network_id')->get();

        $params = [
            'accounts' => $accounts,
            'title' => 'Accounts',
            'page' => 'accounts'
        ];

        return $this->appRender('app/accounts', $params, $request, $response);
    }

}

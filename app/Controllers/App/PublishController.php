<?php

namespace App\Controllers\App;

use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\SocialEngine\GetLastPostsTask;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class PublishController extends Controller
{
    public function indexAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Publish',
            'page' => 'posts'
        ];

        return $this->appRender('app/index', $params, $request, $response);
    }

    public function queueAction(Request $request, Response $response, $args)
    {
        $count = 20;
        $res = task(new GetLastPostsTask,
            [
                vo('queued', 'post.publish_params.publish_type'),
                0,
                $count
            ]
        );

        $params = [
            'title' => 'Publish',
            'page' => 'publish',
            'submenu' => 'queue',
            'resGetLastPostsTask' => $res
        ];

        return $this->appRender('app/publish/queue', $params, $request, $response);
    }
}

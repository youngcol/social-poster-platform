<?php

namespace App\Controllers\App;

use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\SocialEngine\GetLastPostsTask;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Collection;

class PostsController extends Controller
{
    public function sentAction(Request $request, Response $response, $args)
    {
        $count = 20;
        $res = task(new GetLastPostsTask,
            [
                vo('sent', 'post.status'),
                0,
                $count
            ]
        );

        $params = [
            'title' => 'Publish',
            'page' => 'publish',
            'submenu' => 'sent',
            'resGetLastPostsTask' => $res
        ];

        return $this->appRender('app/posts/sent', $params, $request, $response);
    }

    public function failedAction(Request $request, Response $response, $args)
    {
        $count = 20;

        $res = task(new GetLastPostsTask,
            [
                vo('failed', 'post.status'),
                0,
                $count
            ]
        );

        $params = [
            'title' => 'Failed',
            'page' => 'publish',
            'submenu' => 'failed',
            'resGetLastPostsTask' => $res
        ];

        return $this->appRender('app/posts/failed', $params, $request, $response);
    }

    public function draftsAction(Request $request, Response $response, $args)
    {
        $count = 20;
        $res = task(new GetLastPostsTask,
            [
                vo('draft', 'post.status'),
                0,
                $count
            ]
        );

        $params = [
            'title' => 'Publish',
            'page' => 'publish',
            'submenu' => 'drafts',
            'resGetLastPostsTask' => $res
        ];

        return $this->appRender('app/posts/drafts', $params, $request, $response);
    }

    public function indexAction(Request $request, Response $response, $args)
    {
        $params = [
            'title' => 'Publish',
            'page' => 'posts'
        ];

        return $this->appRender('app/index', $params, $request, $response);
    }
}

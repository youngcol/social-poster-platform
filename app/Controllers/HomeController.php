<?php

namespace App\Controllers;

use App\Facades\CU;

use App\Facades\session as sess;
use App\Models\Session;
use App\Models\User;
use App\Tasks\SignUpUserTask;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Ramsey\Uuid\Uuid;

class HomeController extends Controller
{
    public function newLayoutAction (Request $request, Response $response, $args)
    {
        $params = [
            'page' => 'home',
        ];

//        return $this->render('public2/login', $params, $request, $response);
        return $this->render('public2/index', $params, $request, $response);
    }

	public function index (Request $request, Response $response, $args)
	{
        //        $data['messages'] = $this->container->flash->getMessages();

        $params = [
            'page' => 'home',
        ];

        // rator | 𝗧𝗛𝗘 𝗕𝗘𝗦𝗧 𝗢𝗡𝗟𝗜𝗡𝗘 𝗖𝗦𝗦 𝗚𝗘𝗡𝗘𝗥𝗔𝗧𝗢𝗥</title>

        return $this->render('public/index', $params, $request, $response);
	}

	public function signinAction(Request $request, Response $response, $args)
    {
        if (CU::isAuthorized())
        {
            return redirect('app.index');
        }

        $params = [

        ];

        return $this->render('public/login', $params, $request, $response);
    }

    public function signupStore(Request $request, Response $response, $args)
    {
        list
            (
                $err,
                $username,
                $email,
                $password

            ) = params(
                $request,
                'user.signup',
                [
                    'username' => 'user.username',
                    'email' => 'user.email',
                    'password' => 'user.password'
                ]
            );

        $Res = task(new SignUpUserTask, [$username, $email, $password]);

        if ($Res->ok()) {
            return redirect('app.index');
        } else {
            return json_400($Res->toArray());
        }
    }

    public function signup(Request $request, Response $response, $args)
    {
        $params = [];

        $this->render('public/signup', $params, $request, $response);
    }


    public function features(Request $request, Response $response, $args)
    {
        $params = [];

        $this->render('public/features', $params, $request, $response);
    }

    public function prices(Request $request, Response $response, $args)
    {
        $params = [];

        $this->render('public2/pricing', $params, $request, $response);
    }

    public function termsAction(Request $request, Response $response, $args)
    {
        $params = [];

        $this->render('public/terms', $params, $request, $response);
    }

    public function privacyPolicyAction(Request $request, Response $response, $args)
    {
        $params = [];

        $this->render('public/privacy_policy', $params, $request, $response);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     * @throws \Exception
     */
    public function passwordForgot(Request $request, Response $response, $args)
    {

        $isPost = $request->isPost();
        $isSentEmail = false;


        if ($isPost)
        {
            $email = $request->getParam('email');
            $user = User::where('email', $email)->first();

            if ($user)
            {
                $isSentEmail = true;
                $time = substr(time(), -3 ,3);
                $user->password_reset_code = uuid1FromId(rand(1,9999) . $time);
                $user->save();

                // send email
//                $this->container->mailer->
            }
            else
            {

            }

        }

        $params = [
            'isSentEmail' => $isSentEmail,
            'isPost' => $isPost,

        ];
        return $this->render('public/default/password_forgot', $params, $request, $response);

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function passwordReset(Request $request, Response $response, $args)
    {
        $code = sp($args, 'code');
        $isVerified = false;

        if ($code)
        {
            $user = User::where('password_reset_code', $code)->first();

            if ($user)
            {
                $isVerified = true;
            }
        }

        $params =
            [
                'isVerified' => $isVerified,
                'code'  => $code
            ];
        return $this->render('public/default/password_reset', $params, $request, $response);

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function passwordChange(Request $request, Response $response, $args)
    {
        list(
                $err,
                $pass1,
                $pass2,
                $code
            ) = params
            (
                $request,
                'user.password.change',
                [
                    'pass1' => 'user.password',
                    'pass2' => 'user.password',
                    'code' => 'filled.string',
                ]
            );

        if (!eN($err)) return $err;
        //-=-=-=-=-=-=-=-=-=-=-=-=


        $isVerified = false;

        $user = User::where('password_reset_code', $code)->first();

        if ($user)
        {
            $isVerified = true;
            $user->password_reset_code='';
            $user->setPassword($pass1->_());
            $user->save();
        }

        $params = [

            'isVerified' => $isVerified,
            'code' => $code
        ];
        return $this->render('public/default/password_change', $params, $request, $response);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function emailVerify(Request $request, Response $response, $args)
    {
        $code = sp($args, 'code');
        $user = User::where('email_verify_code', $code)->first();

        $isVerified = false;

        if ($user)
        {
            $user->is_email_verified = 1;
            $user->save();
            $isVerified = true;
        }

        $params = [
            'isVerified' => $isVerified
        ];

        return $this->render('public/default/email_verified', $params, $request, $response);
    }


    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function loginAction(Request $request, Response $response, $args)
    {
        // get the parameters
        $username = $request->getParam('username');
        $password = $request->getParam('password');
        $rememberMe = $request->getParam('remember_me');

        // search the user
        $user = User::where('email', '=', $username)->where('active', 1)->first();

        // if user not exist and password isn't correct redirect to index...
        if( !$user || !$user->checkPassword($password) ){
            $this->container->flash->addMessage('warning', 'Incorrect username or password');
            return redirect('signin');
        }

        $sessionId = Uuid::uuid1() . Uuid::uuid1();

        // set cookie
        $expire = time()+60*60*24*3; // 3 days
        if ($rememberMe == 'on')
        {
            $expire = time()+60*60*24*365*3; // 3 years
        }
        setcookie('_kitid', $sessionId, $expire, '/');

        // set session array
        sess::fill([
            'current_user' => $user->schema(),
            'uniqid' => $sessionId
        ]);

        // set row into db
        $session = new Session;
        $session->user_id = $user->id;
        $session->uniqid = $sessionId;
        $session->save();

        // then redirect to admin dashboard
        return redirect('app.index');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function logoutAction(Request $request, Response $response, $args)
    {
        $session = $session = Session::where('uniqid', '=', $_SESSION['uniqid'])->first();

        $session->delete();
        unset($_SESSION['uniqid']);
        unset($_SESSION['current_user']);

        $this->container->flash->addMessage('success', 'You are logged out');
        return redirect('index');
    }
}



/**
 * @api {get} /right Список прав
 * @apiName GetRights
 * @apiGroup Right
 * @apiDescription Метод для получения списка прав.
 * @apiPermission user
 *
 * @apiSuccessExample {json} Успешно (200)
 *     HTTP/1.1 200 OK
 *     {
 *       "data": [
 *         {
 *           "type": "right",
 *           "id": "1",
 *           "attributes": {
 *             "name": "manageUsers",
 *             "description": "Управление пользователями",
 *             "created_at": "2016-10-17T07:38:21+0000",
 *             "updated_at": "2016-10-17T07:38:21+0000",
 *             "created_by": 0,
 *             "updated_by": null
 *           }
 *         }
 *       ]
 *     }
 *
 * @apiUse StandardErrors
 * @apiUse UnauthorizedError
 * @apiUse NotFoundError
 */


/**
 * @api {post} /right Создание права
 * @apiName CreateRight
 * @apiGroup Right
 *
 * @apiDescription Метод для создания нового права.
 *
 * @apiPermission admin
 *
 * @apiParam {String} name Имя права (уникальный)
 * @apiParam {String} description Человекопонятное описание
 *
 * @apiParamExample {json} Пример запроса:
 *    {
 *      "data": {
 *        "attributes": {
 *          "name": "manageUsers",
 *          "description": "Управление пользователями"
 *        }
 *      }
 *    }
 *
 * @apiHeader {String} Authorization Bearer TOKEN
 *
 * @apiSuccessExample {json} Успешно (200)
 *     HTTP/1.1 200 OK
 *     {
 *       "data": {
 *         "type": "right",
 *         "id": "1",
 *         "attributes": {
 *           "name": "manageUsers",
 *           "description": "Управление пользователями",
 *           "created_at": "2016-10-17T07:38:21+0000",
 *           "updated_at": "2016-10-17T07:38:21+0000",
 *           "created_by": 1,
 *           "updated_by": null
 *         },
 *         "links": {
 *           "self": "http://bootstrapi.dev/api/right/1"
 *         }
 *       }
 *     }
 *
 * @apiUse StandardErrors
 * @apiUse UnauthorizedError
 */

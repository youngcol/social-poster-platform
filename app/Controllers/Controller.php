<?php

namespace App\Controllers;

use App\Facades\CU;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\MediaFile\UploadMediaToLocalFolderTask;
use Illuminate\Support\Arr;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class Controller
{
	public function __construct($container)
	{
		$this->container = $container;

		$this->requests_queue_service();
	}

    private function requests_queue_service()
    {
        $_SESSION['last_requests'][] = $_SERVER['REQUEST_URI'];
        $c = count($_SESSION['last_requests']);

        if ($c > 2)
        {
            $_SESSION['last_requests'] = [
                $_SESSION['last_requests'][$c-2],
                $_SESSION['last_requests'][$c-1]
            ];
        }
	}

    public function fetch($tmpl, $data, Request $request)
    {
        $this->templateData($request, $data);
        return $this->container->view2->fetch($tmpl, $data);
    }

    public function render($tmpl, $data, Request $request, Response $response)
    {
        $this->templateData($request, $data);

        return $this->container->view2->render($response, $tmpl, $data);
    }

    private function templateData($request, &$data)
    {
        $data['csrf']['nameKey'] 	= $this->container->csrf->getTokenNameKey();
        $data['csrf']['valueKey'] 	= $this->container->csrf->getTokenValueKey();
        $data['csrf']['name'] 		= $request->getAttribute($data['csrf']['nameKey']);
        $data['csrf']['value'] 		= $request->getAttribute($data['csrf']['valueKey']);

        $data['name'] = settings('name');
        $data['host'] = settings('host');
        $data['env'] = settings('env');
        $data['seo'] = settings('seo');

        if (isset($_SESSION['last_bad_request_errors']))
        {
            $data['errors'] = $_SESSION['last_bad_request_errors'];
        }
    }


    protected function appRender(string $tmpl, array $data, Request $request, Response $response)
    {
        $this->templateData($request, $data);
        $data['current_user'] = $_SESSION['current_user'];
        $components_html = [];

        $data['global_js_models'] = [];

        $data['global_js_models']['routes'] = [];
        $routes = $this->container->router->getRoutes();
        foreach ($routes as $route)
        {
            $data['global_js_models']['routes'][$route->getName()] = $route->getPattern();
        }

        $data['global_js_models']['i18n'] = unserialize(file_get_contents(PATH_ROOT . 'i18n.php'));

        $data['components_html'] = $components_html;

        return $this->render($tmpl, $data, $request, $response);
    }

    /**
     * @param Request $request
     * @param $mediaFiles
     * @return array
     */
    protected function uploadMediaIntoTempFolder(Request $request, $mediaFiles)
    {
        $uploadedFiles = $request->getUploadedFiles();
        $ret = [];

        foreach ($mediaFiles as $mediaFile)
        {
            $uploadedFile = sp($uploadedFiles, $mediaFile);
            if ($uploadedFile)
            {
                $res = task(new UploadMediaToLocalFolderTask(), [CU::user(), $uploadedFile]);

                if ($res->ok())
                {
                    $ret[] = $res->mediaFile;
                }
            }
        }

        return $ret;
    }
}

<?php

namespace App\Controllers\Admin;

use App\Controllers\Controller;
use App\Facades\db;
use App\Services\Language;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\User;
use App\Models\Session;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Finder\Finder;
use Illuminate\Database\Capsule\Manager as Capsule;
use Underscore\Types\Arrays;

class AdminController extends Controller
{

	public function dashboard (Request $request, Response $response, $args)
	{
	    $params['users_count'] = 6;
        return $this->render('admin/dashboard', $params, $request, $response);
	}

	public function developmentFormAction(Request $request, Response $response, $args)
    {
        $command = $request->getParam('command');
        $param1 = $request->getParam('param1');

        switch($command)
        {
            case "drop:whole:database":
            {
                $ret = run_partisan('database:drop');

                return json_200(
                    [
//                        'exec' => implode("<br/>", $out1)
//                            . "<br>". implode("<br/>", $out2)
//                            . "<br> Whole database dropped!"

                        'exec' => "Whole database dropped!"
                    ]
                );
                break;
            }
            case "run:seed":
            {
//                db::table('seeds')->where('version', $param1)->delete();
                $param1 = '';
                break;
            }

            case "migrate:up":
            {
                $param1 = '';
                break;
            }
        }


        $cmd = implode(';', [
            'cd '. PATH_ROOT,
            "php partisan $command $param1"
        ]);

        $output = [];
        
        $execResult = exec($cmd, $output);

        return json_200(['exec' => $execResult]);
    }

	public function routes(Request $request, Response $response, $args)
    {
        $routes = $this->container->router->getRoutes();
        $params = [
            'routes' => $routes,
        ];

        return $this->render('admin/routes', $params, $request, $response);

    }

	public function development(Request $request, Response $response, $args)
    {

        $params = [
            'seeds' => files(SEEDS_PATH),
            'migrations' => db::table('migrations')->get()
        ];

        return $this->render('admin/development', $params, $request, $response);

    }

	public function logs(Request $request, Response $response, $args)
	{
        $logFile = $request->getParam('log');
        $fileContent = null;
        $parsedContent = null;
        $isTag = (strpos($logFile, 'tag__') === 0);
        $isExceptions = $logFile === 'exceptions.log';
        
        if ($logFile) {
            $logPathName = LOGS_PATH . '/'. $logFile;
            $filesize = filesize($logPathName);

            $blockLength = $filesize > 10000 ? 10000 : $filesize;
            $fh = fopen($logPathName, 'r');

            // go to the end of the file
            fseek($fh, 0, SEEK_END);
            fseek($fh, -$blockLength, SEEK_CUR);
            $fileContent = fread($fh, $blockLength);
        }

        if ($isTag)
        {
            $separated = explode("\n\n", $fileContent);

            foreach ($separated as $block)
            {
                if (strlen(trim($block)) == 0) continue;
                //-=-=-=-=-=-=-=-=-=-=-=-=

                $item = explode("\n", $block);

                if (!isset($item[1]))
                {
                    dump($item);echo 'AdminController.php:187'; exit;
                }

                $push = [
                    'time' => $item[0],
                    'title' => $item[1]
                ];

                if (count($item) == 3)
                {
                    $push['data'] = unserialize($item[2]);
                }

                $parsedContent[] = $push;
            }
        }
        
        if ($isExceptions)
        {
            $blocks = explode("\n\n=======================================\n\n", $fileContent);

            $parsedContent = [];

            foreach ($blocks as $block)
            {
                $lines = explode("\n", $block);

                $header = [];
                $isHeaderOk = false;

                foreach ($lines as $k => $line) {
                    $header[] = $line;

                    if ($line === '')
                    {
                        $isHeaderOk = true;
                        break;
                    }
                }

                if ($isHeaderOk)
                {
                    $parsedContent[] = [
                        'header' => $header,
                        'lines' => array_splice($lines, count($header))
                    ];
                }
                else
                {
                    $parsedContent[] = [
                        'header' => '',
                        'lines' => $lines
                    ];
                }
            }
        }

	    $params = [
	        'title' => 'Logs',
	        'files' => scandir(LOGS_PATH),
            'fileContent' => $fileContent,
            'parsedContent' => $parsedContent,
            'isTag' => $isTag,
            'isExceptions' => $isExceptions,
        ];

		return $this->render('admin/logs/index', $params, $request, $response);
	}

    public function mysqlBuilder(Request $request, Response $response, $args)
    {

        $sql = DB::table('users')->where('perss', '=', '555')->toSql();
//        dump($sql);echo 'AdminController.php:262'; exit;

        $params = [

        ];

        return $this->render('admin/mysql_builder', $params, $request, $response);
	}

    public function databaseStateAction(Request $request, Response $response, $args)
    {

        $finder = new Finder();
        $finder->files()->name('*.php')->in(DB_PATH.'/states');

        $list = map($finder, function ($file) {
            return $file->getBasename('.php');
        });

        $params = [
            'list' => $list,
            'isRun' => false
        ];

        return $this->render('admin/database_state', $params, $request, $response);
	}


    public function runDatabaseStateAction(Request $request, Response $response, $args)
    {
        $file = $args['file'];

        require_once (DB_PATH.'/states/DbState.php');
        require_once (DB_PATH.'/states/'.$file.'.php');

        $report = [];

        $report['partisan'] = run_partisan('database:truncate');

        $objStateBuilder = new $file;
        $report['state_builder'] = $objStateBuilder->run();

        $params = [
            'list' => [],
            'file' => $file,
            'isRun' => true,
            'report' => $report
        ];

        return $this->render('admin/database_state', $params, $request, $response);
	}


    public function languagesAction(Request $request, Response $response, $args)
    {
        $finder = new Finder();
        $finder->directories()->in(PATH_ROOT.'lang');

        $dics = [];
        $locales = [];

        foreach ($finder as $folder)
        {
            $locale = $folder->getBasename();

            $lang = new Language($locale);
            $dics[$locale] = array_key_routes_recursive($lang->getDictionary());
            $locales[] = $locale;
        }

        $diffs = [];

        foreach ($dics as $locale => $keys)
        {
            foreach ($dics as $locale2 => $keys2) {
                $diff = array_diff($keys, $keys2);

                if (count($diff))
                {
                    $diffs[] = [
                        'locale1' => $locale,
                        'locale2' => $locale2,
                        'message' => "`$locale2` miss keys that `$locale` has",
                        'keys' => $diff
                    ];
                }

            }
        }


        $params = [
            'diffs' => $diffs,

        ];


        return $this->render('admin/languages', $params, $request, $response);
	}
}

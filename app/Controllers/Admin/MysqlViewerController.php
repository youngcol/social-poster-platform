<?php


namespace App\Controllers\Admin;

use App\Controllers\Controller;
use App\Facades\db;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\User;
use App\Models\Session;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Finder\Finder;
use Illuminate\Database\Capsule\Manager as Capsule;
use Underscore\Types\Arrays;


class MysqlViewerController extends Controller
{

    public function indexAction(Request $request, Response $response, $args)
    {
        $tables = $this->buildTablesMap();
//        dump($tables);echo 'MysqlViewerController.php:24'; exit;
        $params = [
            'tables' => $tables
        ];

        return $this->render('admin/mysql-viewer/index', $params, $request, $response);
    }

    public function showTableAction(Request $request, Response $response, $args)
    {
        $table = $args['table'];
        $tables = $this->buildTablesMap();
//        $table = 'post_media_files';
//        $table = 'users';
        $tableTiers = $this->buildTableTiers($table, $tables);

//        dump($tableTiers);echo 'MysqlViewerController.php:43'; exit;
        $params = [
            'tableTiers' => $tableTiers
        ];

        return $this->render('admin/mysql-viewer/table', $params, $request, $response);
    }

    protected function buildTableTiers($needle, $tables)
    {
        $ret = [
            'table' => $tables[$needle],
            'down' => [],
            'up' => []
        ];

        $table = $tables[$needle];
        $processedTables = [$needle];

        // build up tiers
        $tablesList = $table['foreign_tables'];
        do {
            $nextTableList = [];
            $currentTier = [];

            $processedTables = array_merge($processedTables, $tablesList);
            foreach ($tablesList as $tableName)
            {
                $cursorTable = $tables[$tableName];

                $childTables = $this->getChildsForTable($tableName, $tables, $processedTables);

                $nextTableList = array_merge($nextTableList, $cursorTable['foreign_tables']);
                $nextTableList = array_merge($nextTableList, $childTables);
                $currentTier[] = $cursorTable;
            }

            if (count($currentTier))
            {
                $ret['up'][] = $currentTier;
            }

            $nextTableList = array_unique(array_diff($nextTableList, $processedTables));

            $tablesList = $nextTableList;

        } while(count($tablesList));


        // build down tiers
        $tablesList = [$table['name']];
        $processedTables = [$needle];

        do {
            $nextTableList = [];
            $currentTier = [];
            $processedTables = array_merge($processedTables, $tablesList);

            foreach ($tablesList as $tableName)
            {
                $childTables = $this->getChildsForTable($tableName, $tables, $processedTables);
                $nextTableList = array_merge($nextTableList, $childTables);

                $currentTier[] = $tables[$tableName];

                foreach ($childTables as $childTable) {
                    $cursorTable = $tables[$childTable];
                    $currentTier[] = $cursorTable;

                    $nextTableList = array_merge($nextTableList, $cursorTable['foreign_tables']);
                }
            }

            if (count($currentTier))
            {
                $ret['down'][] = $currentTier;
            }
            //dump($processedTables);echo 'MysqlViewerController.php:115'; exit;
            $nextTableList = array_unique(array_diff($nextTableList, $processedTables));

            $tablesList = $nextTableList;

        } while(count($tablesList));


        $ret['up'] = array_reverse($ret['up']);

        return $ret;
    }

    protected function getChildsForTable($needle, $tables, $exclude=[])
    {
        $ret = [];

        foreach ($tables as $table)
        {
            if (in_array($needle, $table['foreign_tables']))
            {
                if (in_array($table['name'], $exclude))
                {
                    continue;
                }

                $ret[] = $table['name'];
            }
        }


        return $ret;
    }

    protected function buildTablesMap()
    {
        $tables = map(db::select('show tables'), function ($item) {
            $vars = get_object_vars($item);
            return array_pop($vars);
        });

        $ret = [];

        foreach ($tables as $table) {

            $tableFields = db::select('DESCRIBE '. $table);

            $tableForeignKeys = db::select("SELECT
                    `column_name`, 
                    `referenced_table_schema` AS foreign_db, 
                    `referenced_table_name` AS foreign_table, 
                    `referenced_column_name`  AS foreign_column 
                FROM
                    `information_schema`.`KEY_COLUMN_USAGE`
                WHERE
                    `constraint_schema` = SCHEMA()
                AND
                    `table_name` = '$table'
                AND
                    `referenced_column_name` IS NOT NULL
                ORDER BY
                    `column_name`;");


            $foreignTables = map($tableForeignKeys, function ($item) {
                return $item->foreign_table;
            });

            $ret[$table] = [
                'name' => $table,
                'fields' =>  $tableFields,
                'foreign' => $tableForeignKeys,
                'foreign_tables' => $foreignTables
            ];
        }

        return $ret;
    }
}

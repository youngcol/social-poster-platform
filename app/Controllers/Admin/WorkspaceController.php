<?php


namespace App\Controllers\Admin;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Controllers\Controller;
use Symfony\Component\Finder\Finder;

class WorkspaceController extends Controller
{
    public function leftColumn(Request $request, Response $response, $args)
    {
        $finder = new Finder();
        $finder->files()->name('*.php')->in(PATH_ROOT . 'app_workspace');
        $leftColumn = $this->renderLeftColumn($request, $finder);

        return json_200(['column' => $leftColumn]);
    }

    public function index(Request $request, Response $response, $args)
    {

        $finder = new Finder();
        $finder->files()->name('*.php')->in(PATH_ROOT . 'app_workspace');
        $leftColumn = $this->renderLeftColumn($request, $finder);

        $openedFile = sp($_GET, 'file');

        $this->render('admin/workspace_layout', [
            'leftColumn' => $leftColumn,
            'openedFile' => $openedFile,
        ], $request, $response);
    }

    public function search(Request $request, Response $response, $args)
    {
        $needle = $request->getParam('needle');

        $finder = new Finder();
        $finder->in(PATH_ROOT . 'app_workspace');
        $finder->files()->contains($needle);
        $leftColumn = $this->renderLeftColumn($request, $finder);

        return json_200(['column' => $leftColumn]);
    }

    public function renderLeftColumn($request, $finder)
    {
        $grouped = [];
        $folders = [];
        foreach ($finder as $file) {
            /* @var \Symfony\Component\Finder\SplFileInfo $file */
            $relativePath = $file->getRelativePath();

            if (sp($grouped, $relativePath) === null ) {
                $grouped[$relativePath] = [];
                $folders[] = $relativePath;
            }

            $grouped[$relativePath][] = [
                'path' => $file->getPathname(),
                'name' => $file->getBasename()
            ];
        }

        $response = new \Slim\Http\Response();
        sort($folders);

        return $this->fetch('admin/workspace_files', [
            'grouped' => $grouped,
            'folders' => $folders,
        ], $request);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function fileCreate(Request $request, Response $response, $args)
    {
        $folder = vo($request->getParam('folder'), 'filled.string');
        $filename = vo($request->getParam('file'), 'filled.string');

        $newPathName = PATH_ROOT . 'app_workspace/' . $folder . '/' . $filename;
        $newPathName = str_replace('.php', '', $newPathName);
        $newPathName .= '.php';

        file_put_contents($newPathName, "<?php\n\nglobal \$container;\n\n");


        $finder = new Finder();
        $finder->files()->name('*.php')->in(PATH_ROOT . 'app_workspace/' . $folder);
        $leftColumn = $this->renderLeftColumn($request, $finder);

        return json_200(['column' => $leftColumn]);
    }

    public function folderCreate(Request $request, Response $response, $args)
    {
        $folder = $request->getParam('folder');
        $newPathName = PATH_ROOT . 'app_workspace/' . $folder;
        $newFileName = $newPathName . '/aaa_main.php';

        if (!file_exists($newPathName))
        {
            mkdir($newPathName);
        }

        if (!file_exists($newFileName))
        {
            file_put_contents($newFileName, "<?php\n\nglobal \$container;\n\n");
        }

        $finder = new Finder();
        $finder->files()->name('*.php')->in($newPathName);
        $leftColumn = $this->renderLeftColumn($request, $finder);

        return json_200(['column' => $leftColumn]);
    }

    public function fileContent(Request $request, Response $response, $args)
    {
        $filePath = $request->getParam('file');
        return json_200(['content' => file_get_contents($filePath)]);
    }

    public function fileOutput(Request $request, Response $response, $args)
    {
        $filePath = $request->getParam('file');
        include($filePath);
    }
}

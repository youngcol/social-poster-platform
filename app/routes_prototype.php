<?php

/**
 * This files includes only when (APP_ENV=dev)
 * for development porpouse only
 */

$app->group('/api_1', function () {


});


$app->group('/proto', function () {

    $this->get('', 'App\Controllers\Prototype\PrototypeController:index')->setName('proto.index');
    $this->get('/posts', 'App\Controllers\Prototype\PrototypeController:posts')->setName('proto.posts');
    $this->post('/post/send/test', 'App\Controllers\Prototype\PrototypeController:postStoreTest')->setName('api.post.send.test');


})->add(new App\Middlewares\Auth($container));

<?php


$app->group('/admin', function () {

	$this->get('/development', 'App\Controllers\Admin\AdminController:development')->setName('admin.development');
	$this->get('/mysql_viewer', 'App\Controllers\Admin\MysqlViewerController:indexAction')->setName('admin.mysql_viewer');
	$this->get('/mysql_viewer/table/{table}', 'App\Controllers\Admin\MysqlViewerController:showTableAction')->setName('admin.mysql_viewer.table');

	if (settings('env') === 'dev')
    {
        $this->get('/mysql_builder', 'App\Controllers\Admin\AdminController:mysqlBuilder')->setName('admin.mysql_builder');

        $this->get('/routes', 'App\Controllers\Admin\AdminController:routes')->setName('admin.routes');
        $this->post('/development/command', 'App\Controllers\Admin\AdminController:developmentFormAction')->setName('admin.development.command');
        $this->get('/database/state', 'App\Controllers\Admin\AdminController:databaseStateAction')->setName('admin.database.state');
        $this->get('/database/state/run/{file}', 'App\Controllers\Admin\AdminController:runDatabaseStateAction')->setName('admin.database.state.run');
    }

    $this->get('/languages', 'App\Controllers\Admin\AdminController:languagesAction')->setName('admin.languages');
	$this->get('/dashboard', 'App\Controllers\Admin\AdminController:dashboard')->setName('admin.dashboard');
	$this->get('/logs', 'App\Controllers\Admin\AdminController:logs')->setName('admin.logs');


	$this->get('/users', 'App\Controllers\Admin\UserController:index')->setName('admin.users');
	$this->get('/users/add', 'App\Controllers\Admin\UserController:add')->setName('usersadd');
	$this->post('/users/save', 'App\Controllers\Admin\UserController:save')->setName('userssave');
	$this->get('/users/edit/{id}', 'App\Controllers\Admin\UserController:edit')->setName('usersedit');
	$this->post('/users/update/{id}', 'App\Controllers\Admin\UserController:update')->setName('usersupdate');
	$this->get('/users/delete/{id}', 'App\Controllers\Admin\UserController:delete')->setName('usersdelete');
   
})->add(new App\Middlewares\Auth($container, 'web', 'admin'));


if (settings('env') === 'dev')
{
    $app->get('/workspace/file', 'App\Controllers\Admin\AdminController:workspaceFileContent')->setName('admin.workspace');

    $app->group('/w', function () {
        $this->get('/search', 'App\Controllers\Admin\WorkspaceController:search');
        $this->post('/folder', 'App\Controllers\Admin\WorkspaceController:folderCreate')->setName('w.folder.create');
        $this->post('/file', 'App\Controllers\Admin\WorkspaceController:fileCreate')->setName('w.file.create');
        $this->get('/file/content', 'App\Controllers\Admin\WorkspaceController:fileContent')->setName('admin.file.content');
        $this->get('/file/output', 'App\Controllers\Admin\WorkspaceController:fileOutput')->setName('admin.file.output');
        $this->get('', 'App\Controllers\Admin\WorkspaceController:index')->setName('w.index');
        $this->get('/left-column', 'App\Controllers\Admin\WorkspaceController:leftColumn')->setName('w.leftColumn');
    });
}



//=================================
//======= PUBLIC ====================
//=================================

// user login, reset password, signup routes
$app->get('/email-verify/{code}', 'App\Controllers\HomeController:emailVerify')->setName('email.verify');
$app->get('/password/forgot', 'App\Controllers\HomeController:passwordForgot')->setName('password.forgot');
$app->post('/password/forgot', 'App\Controllers\HomeController:passwordForgot')->setName('password.forgot');
$app->get('/password/reset/{code}', 'App\Controllers\HomeController:passwordReset')->setName('password.reset');
$app->post('/password/change', 'App\Controllers\HomeController:passwordChange')->setName('password.change');
$app->get('/signup', 'App\Controllers\HomeController:signup')->setName('signup');
$app->post('/signup/store', 'App\Controllers\HomeController:signupStore')->setName('signup.store');
$app->post('/login', 'App\Controllers\HomeController:loginAction')->setName('login');
$app->get('/logout', 'App\Controllers\HomeController:logoutAction')->setName('logout');


// public pages
$app->get('/', 'App\Controllers\HomeController:index')->setName('index');
$app->get('/signin', 'App\Controllers\HomeController:signinAction')->setName('signin');
$app->get('/features', 'App\Controllers\HomeController:features')->setName('features');
$app->get('/prices', 'App\Controllers\HomeController:prices')->setName('prices');
$app->get('/blog', 'App\Controllers\BlogController:index')->setName('blog');
$app->get('/terms', 'App\Controllers\HomeController:termsAction')->setName('terms');
$app->get('/privacy-policy', 'App\Controllers\HomeController:privacyPolicyAction')->setName('privacy-policy');

$app->get('/new-layout', 'App\Controllers\HomeController:newLayoutAction')->setName('blog');





//=================================
//===== APP ROUTES =====================
//=================================



// reserved area
$app->group('/app', function () {

    $this->get('/settings', 'App\Controllers\App\AccountSettingsController:indexAction')->setName('app.settings');

    $this->get('/index', 'App\Controllers\App\AppController:indexAction')->setName('app.index');
    $this->get('/accounts', 'App\Controllers\App\AppController:accountsAction')->setName('app.accounts');
    $this->get('/publish', 'App\Controllers\App\AppController:publishAction')->setName('app.publish');
    $this->get('/posts', 'App\Controllers\App\AppController:postsAction')->setName('app.posts');
    $this->get('/reports', 'App\Controllers\App\ReportsController:indexAction')->setName('app.reports');
    $this->get('/interactions', 'App\Controllers\App\InteractionsController:indexAction')->setName('app.interactions');

    $this->get('/posts/sent', 'App\Controllers\App\PostsController:sentAction')->setName('app.posts.sent');
    $this->get('/posts/drafts', 'App\Controllers\App\PostsController:draftsAction')->setName('app.posts.drafts');
    $this->get('/posts/failed', 'App\Controllers\App\PostsController:failedAction')->setName('app.posts.failed');
    $this->get('/queue', 'App\Controllers\App\PublishController:queueAction')->setName('app.publish.queue');


    $this->group('/social-engine', function () {

        $this->get('/account/pinterest/add', 'App\Controllers\SocialEngineController:accountPinterest')->setName('social-engine.account.pinterest.add');
        $this->get('/account/twitter/add', 'App\Controllers\SocialEngineController:accountTwitter')->setName('social-engine.account.twitter.add');
        $this->get('/account/facebook/add', 'App\Controllers\SocialEngineController:accountFacebook')->setName('social-engine.account.facebook.add');
        $this->get('/twitter/callback', 'App\Controllers\SocialEngineController:twitterCallback')->setName('twitter.callback');
        $this->get('/facebook/callback', 'App\Controllers\SocialEngineController:facebookCallback')->setName('facebook.callback');
        $this->get('/pinterest/callback', 'App\Controllers\SocialEngineController:pinterestCallback')->setName('pinterest.callback');
        $this->get('/instagram/callback', 'App\Controllers\SocialEngineController:instagramCallback')->setName('instagram.callback');

    });

    if (settings('env') === 'dev')
    {
        $this->get('/templates', 'App\Controllers\App\AppController:templatesAction')->setName('app.templates');
        $this->get('/vue-stories', 'App\Controllers\App\AppController:vueStoriesAction')->setName('app.vue.stories');
        $this->get('/vue-stories/{folder}/{story}', 'App\Controllers\App\AppController:vueStoriesAction')->setName('app.vue.story');

    }

})->add(new App\Middlewares\Auth($container));


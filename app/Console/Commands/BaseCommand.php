<?php


namespace App\Console\Commands;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BaseCommand extends Command
{
    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this->setName('base:command');
    }

    protected function log(OutputInterface $output, $message, $data=null)
    {
        $fileName = implode('_', explode(':', $this->getName()));
        log_tag("command_$fileName", $message, $data);

        $output->writeln([$message, print_r($data, true)]);
    }

    /**
     * Safely execute
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function executeMain(InputInterface $input, OutputInterface $output)
    {
        $this->log($output, 'Start command', [
            $input->getArguments(),
            $input->getOptions()
        ]);

        $isException = false;
        try
        {
            $this->main($input, $output);
        }
        catch (\Exception $e)
        {
            $isException = true;
            log_exception($e);

            if (settings('env') == 'dev')
            {
                throw $e;
            }
        }

        if ($isException)
        {
            $this->log($output,'Failed. Exception raised!');
        }
        else
        {
            $this->log($output,'Success.Command finished');
        }
    }
}

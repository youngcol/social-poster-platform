<?php
declare(strict_types=1);
namespace App\Console\Commands\App;

use App\Common\Res;
use App\Facades\db;
use App\Models\SocialEngine\Post\PostQueuedPlace;
use App\Models\SocialEngine\QueueTime;
use App\Tasks\SocialEngine\Publish\PublishToSocialAccountsTask;
use Carbon\Carbon;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Console\Traits\CodeGenerate;

/**
 * ProcessPostsPublishingQueueCommand
 */
class ProcessPostsPublishingQueueCommand extends Command
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('process:posts:publishing:queue')
            ->setDescription('Command process:posts:publishing:queue')
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $nowGmt = now_date();
        $output->writeln([
            '<info>Run process publishing queue</info>:',
            'Gmt time: ' . date_mysql_format($nowGmt)
        ]);

        $processUserIds = db::table('post_queued_places')
            ->select('user_id')
            ->distinct()
            ->where('is_processed', 0)
            ->get()
            ->pluck('user_id')
            ->toArray();


        $output->writeln('Found users: '. implode(', ', $processUserIds));

        if (count($processUserIds) == 0)
        {
            return;
        }
        //-=-=-=-=-=-=-=-=-=-=-=-=

        foreach ($processUserIds as $processUserId)
        {
            $output->writeln('Process user: '. $processUserId);
            $res = $this->processUser($processUserId, $nowGmt, $output);
        }

        $output->writeln('Done');


// TODO we can get this ids for all users
//$queueTimePublishedTodayIds = db::table('queue_time_published_log as ql')
//    ->leftJoin('queue_times as qt', 'qt.id', '=', 'ql.queue_time_id')
//    ->where('ql.created_at', '>=', $nowGmt->format('Y-m-d 00:00:00'))
//    ->where('qt.user_id', $processUserId)
//    ->pluck('queue_time_id')
//    ->toArray();


//$postQueuedPlaces = PostQueuedPlace::query()
//    ->where('is_processed', 0)
//    ->with('user')
//    ->get();
//
//dump($postQueuedPlaces);echo 'process_queued_posts.php:21'; exit;
//
//
//$rows = db::table('queue_times as qt')
//        ->leftJoin('queue_time_published_log as qtpl', 'qt.id','=','qtpl.queue_time_id')
//        ->whereNull('qtpl.post_queued_place_id')
//        ->get();
//
//dump($rows);echo 'process_queued_posts.php:21'; exit;

    }

    private function processUser(int $userId, Carbon $nowGmt, $output)
    {
        /** @var  QueueTime $queueTime */
        $queueTime = QueueTime::where('week_day', day_of_week($nowGmt))
            ->where('user_id', $userId)
            ->where('intraday_time', '<=', $nowGmt->format('H:i'))
            ->orderBy('intraday_time', 'DESC')
            ->with('publishedQueuedPlace')
            ->first();

        $publishedQueuedPlace = $queueTime->publishedQueuedPlace
            ->where('created_at', '>=', $nowGmt->format('Y-m-d 00:00:00'))
            ->first()
        ;

        if ($publishedQueuedPlace)
        {
            $line = "Last queue time `" . $queueTime->id . "` has published_queued_place `" . $publishedQueuedPlace->id . "` - skip it";
            $output->writeln($line);
            return;
        }

        $postQueuedPlace =
            PostQueuedPlace::query()
                ->where('is_processed', 0)
                ->orderBy('order')
                ->first();

        if (eN($postQueuedPlace))
        {
            $output->writeln('No users to process - exit');
            log_warn('Cant find PostQueuedPlace for user '. $userId);
            return null;
        }

        $output->writeln('Publish queue time: `'.$queueTime->id. '`` with post_queued_place `'. $postQueuedPlace->id . '`' );

        $now = now_date();
        $postQueuedPlace->process_started_time = date_mysql_format($now);
        $postQueuedPlace->save();


        $res = task(new PublishToSocialAccountsTask,
            [
                $postQueuedPlace
            ]
        );

        $postQueuedPlace->is_processed = 1;
        $postQueuedPlace->save();

        $queueTime->publishedLog()->attach
        (
            $postQueuedPlace->id, [
                'created_at' => date_mysql_format(now_date())
            ]
        );

        return new Res(
            [
                'publishTask' => $res,
            ]
        );
    }
}

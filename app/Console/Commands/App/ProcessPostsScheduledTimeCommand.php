<?php
declare(strict_types=1);
namespace App\Console\Commands\App;

use App\Models\SocialEngine\Post\PostScheduledTime;
use App\Tasks\SocialEngine\Publish\PublishToSocialAccountsTask;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Console\Traits\CodeGenerate;

/**
 * ProcessPostsScheduledTimeCommand
 */
class ProcessPostsScheduledTimeCommand extends Command
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('process:posts:scheduled:time')
            ->setDescription('Command process:posts:scheduled:time')
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $nowGmt = now_date();

        $output->writeln([
            '<info>Run process scheduled posts</info>:',
            'Gmt time: ' . date_mysql_format($nowGmt)
        ]);


        $postScheduledTimes = PostScheduledTime::query()
            ->where('scheduled_time', '<=', date_mysql_format($nowGmt))
            ->where('process_status', 'scheduled')
            ->orderBy('scheduled_time')
            ->get();


        foreach ($postScheduledTimes as $postScheduledTime)
        {
            $now = now_date();
            $postScheduledTime->process_started_time = date_mysql_format($now);
            $postScheduledTime->process_status = 'processing';
            $postScheduledTime->save();

            $output->writeln('Processing post scheduled id: '. $postScheduledTime->id);

            $res = task(new PublishToSocialAccountsTask,
                [
                    $postScheduledTime
                ]
            );

            $postScheduledTime->process_status = 'done';
            $postScheduledTime->save();
        }


        $output->writeln(['<info>Done</info>']);
    }
}

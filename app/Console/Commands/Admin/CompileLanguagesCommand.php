<?php
declare(strict_types=1);
namespace App\Console\Commands\Admin;

use App\Console\Commands\BaseCommand;
use App\Services\Language;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Console\Traits\CodeGenerate;
use Symfony\Component\Finder\Finder;

/**
 * CompileLanguages
 */
class CompileLanguagesCommand extends BaseCommand
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('compile:languages')
            ->setDescription('Command all languages into single file')
            //->addArgument('command_class', InputArgument::REQUIRED, 'What command class name?')
        ;
    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @return int|void|null
    * @throws Exception
    */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executeMain($input, $output);
    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @throws Exception
    */
    protected function main(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<info>Running command...</info>']);
        //$commandClass = $input->getArgument('command_class');

        $finder = new Finder();
        $allLangs = [];

        foreach ($finder->directories()->in(PATH_ROOT . 'lang') as $folder) {
            $locale = $folder->getBasename();

            $dictionary = [];
            $finder = new Finder();
            $finder->files()->in(PATH_ROOT . 'lang/' . $locale);

            foreach ($finder as $file)
            {
                $keyName = $file->getBasename('.php');
                $dictionary[$keyName] = $this->includeFile($file->getPathname());
            }

            $allLangs[$locale] = $dictionary;
        }

        $content = serialize($allLangs);
        $destFile = PATH_ROOT . 'i18n.php';
        $ret = file_put_contents($destFile, $content);

        $this->log($output, "Updated $destFile.");

        return $ret;
    }


    protected function includeFile($file)
    {
        return include $file;
    }

}

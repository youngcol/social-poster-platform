<?php
declare(strict_types=1);
namespace App\Console\Commands\Admin;

use App\Console\Commands\BaseCommand;
use App\Facades\db;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Console\Traits\CodeGenerate;

/**
 * DropWholeDatabase
 */
class DatabaseDropCommand extends BaseCommand
{

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('database:drop')
            ->setDescription('Command for dropping database data')
        ;
    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @return int|void|null
    * @throws Exception
    */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executeMain($input, $output);
    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @throws Exception
    */
    protected function main(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<info>Running command...</info>']);
        $skip_remove_users_tables = (int)$input->getOption('skip_remove_users_tables');
        $skip_delete_migrations = (int)$input->getOption('skip_delete_migrations');


        $tables = map(db::select('show tables'), function ($item) {
            $item = (array)$item;
            return array_pop($item);
        });

        db::statement('SET FOREIGN_KEY_CHECKS=0');

        $excludeTables = ['sessions', 'users', 'migrations', 'seeds'];


        $this->log($output, 'Skip this tables: ', $excludeTables);
        $tables = array_filter($tables, function ($item) use ($excludeTables){
            return !in_array($item, $excludeTables);
        });

        map($tables, function ($item) {
            db::statement('drop table '. $item);
        });

        $migrations = db::table('migrations')->get()->toArray();
        $migrations = array_filter
        (
            $migrations,
            function ($item)
            {
                return strstr($item->version, 'init_migration') === false;
            }
        );
        $versions = array_pluck($migrations, 'version');
        db::table('migrations')
            ->whereIn('version', $versions)
            ->delete();


        $seeds = db::table('seeds')->get()->toArray();
        $seeds = array_filter($seeds, function ($item) {
            return strstr($item->version, 'UserSeed') === false;
        });

        db::table('seeds')
            ->whereIn('version', array_pluck($seeds, 'version'))
            ->delete();

        db::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

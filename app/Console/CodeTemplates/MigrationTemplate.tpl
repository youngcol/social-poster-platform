<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * <fileName>
 */
class <class>
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('<tableName>', function($table) {

        });

        Capsule::schema()->create('<tableName>', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('deleted_at')->nullable();

            $table->string('col', 255);

            $table->unsignedInteger('votes');
            $table->integer('integer');
            $table->float('amount', 8, 2);
            $table->tinyInteger('votes');

            $table->dateTime('col');
            $table->date('col');
            $table->time('sunrise');

            $table->longText('longText');
            $table->text('description');
            $table->enum('level', ['easy', 'hard'])->default('hard');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('<tableName>');

        Capsule::schema()->table('<tableName>', function($table) {

        });

    }
}

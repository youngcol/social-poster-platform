/**
* @OA\Get(
*     path="/pet/findByTags",
*     summary="Finds Pets by tags",
*     tags={"pet"},
*     description="Muliple tags can be provided with comma separated strings. Use tag1, tag2, tag3 for testing.",
*     operationId="findPetsByTags",
*     @OA\Parameter(
*         name="tags",
*         in="query",
*         description="Tags to filter by",
*         required=true,
*         @OA\Schema(
*           type="array",
*           @OA\Items(type="string"),
*         ),
*         style="form"
*     ),
*     @OA\Response(
*         response=200,
*         description="successful operation",
*         @OA\Schema(
*             type="array",
*             @OA\Items(ref="#/components/schemas/Pet")
*         ),
*     ),
*     @OA\Response(
*         response="400",
*         description="Invalid tag value",
*     ),
*     security={
*         {"petstore_auth": {"write:pets", "read:pets"}}
*     },
* )
*/
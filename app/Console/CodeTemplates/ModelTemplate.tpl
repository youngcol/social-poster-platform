<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class <class>
 *
<phpdoc>
 *
 * @package App\Model
 */
final class <class> extends Model
{
    use SoftDeletes;

    protected $table = '<tableName>';

    protected $fillable = [
<fillable>
    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'social_network_id' => 'integer'
    ];

}

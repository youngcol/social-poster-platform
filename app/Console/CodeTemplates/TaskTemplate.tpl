<?php
declare(strict_types=1);
namespace <namespace>;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class <class> extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {



        return new Res([
            'res' => ''
        ]);
    }
}

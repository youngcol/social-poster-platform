<?php
/**
 * Facebook sdk api service
 */

namespace App\Services;

use App\Common\Res;
use Facebook\FacebookResponse;
use Slim\Container;

class Facebook extends Service
{
    private $fb;

    public function __construct(Container $c)
    {
        parent::__construct($c);

        $this->fb = new \Facebook\Facebook([
            'app_id' => getenv('FACEBOOK_APP_ID'),
            'app_secret' => getenv('FACEBOOK_APP_SECRET'),
            'default_graph_version' => 'v2.10',
            //'default_access_token' => '{access-token}', // optional
        ]);
    }

    public function getLoginLink(): Res
    {
        $helper = $this->fb->getRedirectLoginHelper();

        $permissions = ['email', 'public_profile']; // Optional permissions
        $url = url('facebook.callback', [], true);
        $loginUrl = $helper->getLoginUrl($url, $permissions);

        return new Res(['link' => $loginUrl]);
    }

    public function setAccessToken($accessToken)
    {
        $this->fb->setDefaultAccessToken($accessToken);
    }

    public function getCallbackAccessToken()
    {
        $helper = $this->fb->getRedirectLoginHelper();
        return $helper->getAccessToken();
    }

    public function getMeUser()
    {
        $response = $this->fb->get('/me');
//        return $response->getGraphNode();
        return $response->getDecodedBody();
    }

    public function getMePages()
    {
        $response = $this->fb->get('/me/accounts?fields=name,access_token,perms');
        return $response;
    }
}

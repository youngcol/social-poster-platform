<?php

/**
 * Media files uploader
 */
namespace App\Services;


use App\Common\FlowException;
use App\Common\Res;
use App\Models\SocialEngine\MediaFile;
use App\VO\VoVal;

class FileUploader extends Service
{
    private $s3;

    public function __construct($c)
    {
        parent::__construct($c);

        $this->s3 = new \Aws\S3\S3Client([
            'region'  => settings('amazon.s3.region'),
            'version' => 'latest',
            'credentials' => [
                'key'    => settings('amazon.s3.key'),
                'secret' => settings('amazon.s3.secret'),
            ]
        ]);
    }

    /**
     * Upload file to s3
     *
     * @param string $s3Url
     * @param string $sourceFilePath
     * @param string $bucket
     * @return Res
     */
    public function uploadMediaToS3(string $s3Url, string $sourceFilePath, string $bucket)
    {

        $result = $this->s3->putObject([
            'Bucket'        => $bucket,
            'Key'           => $s3Url,
            'SourceFile'    => UPLOADS_PATH . DIRECTORY_SEPARATOR . $sourceFilePath
        ]);

        return new Res([
            'aws' => $result
        ]);
    }

    /**
     *
     * @param string $url
     * @param string $destination
     */
    public function downloadFileFromUrl(string $url, string $path)
    {
        $ret = false;

        $newfname = $path;
        $file = fopen ($url, 'rb');

        if ($file) {
            $newf = fopen ($newfname, 'wb');
            if ($newf) {
                while(!feof($file)) {
                    fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
                }
            }
        }

        if ($file && $newf)
        {
            $ret = true;
        }

        if ($file) {
            fclose($file);
        }

        if ($newf) {
            fclose($newf);
        }

        return $ret;
    }
}

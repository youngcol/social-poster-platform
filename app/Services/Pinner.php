<?php
/**
 * Pinterest official api service
 */

namespace App\Services;


use App\Common\Res;
use App\VO\VoVal;
use App\VO\Pinterest\OAuthScopes;
use Slim\Container;

class Pinner extends Service
{
    private $pinterest;
    public $auth;
    public $users;

    public function __construct(Container $c, $pinterest)
    {
        parent::__construct($c);

        $this->pinterest = $pinterest;
        $this->auth = $this->pinterest->auth;
        $this->users = $this->pinterest->users;
    }

    /**
     * @param OAuthScopes $scopes
     * @return Res
     */
    public function getLoginLink(OAuthScopes $scopes): Res
    {
        $url = url('pinterest.callback', [], true);
        $authUrl = $this->pinterest->auth->getLoginUrl($url, $scopes->_());

        return new Res(['link' => $authUrl]);
    }

    /**
     * @return Res
     */
    public function getMeBoards(): Res
    {
        return new Res(['boards' => $this->pinterest->users->getMeBoards()]);
    }

    /**
     * @param $text
     * @param $imageUrl
     * @param $board
     * @return Res
     */
    public function createPinFromImageUrl(string $text, string $imageUrl, string $board): Res
    {
        $ret = $this->pinterest->pins->create(array(
            "note"          => $text,
            "image_url"     => $imageUrl,
            "board"         => $board
        ));

        return new Res(['pin' => $ret]);
    }

    /**
     * @param string $text
     * @param string $imagePathname
     * @param string $board
     * @return Res
     */
    public function createPinFromImagePathname(string $text, string $imagePathname, string $board): Res
    {
        $ret = $this->pinterest->pins->create(array(
            "note"          => $text,
            "image"         => $imagePathname,
            "board"         => $board
        ));

        return new Res(['pin' => $ret]);
    }

    /**
     * @param $name
     * @param $description
     * @return Res
     */
    public function createBoard(string $name, string $description): Res
    {
        $ret = $this->pinterest->boards->create(array(
            "name"          => $name,
            "description"   => $description
        ));
        return new Res(['create' => $ret]);
    }


    public function getMeUser(): Res
    {
        return new Res([
            'me' => $this->pinterest->users->me(array(
                'fields' => 'username,first_name,last_name,image[small,large]'
            ))
        ]);
    }

    /**
     * @return Res
     */
    public function getRateLimitRemaining()
    {
        return new Res([
            'rate_limit_remaining' => $this->pinterest->getRateLimitRemaining()
        ]);
    }
}

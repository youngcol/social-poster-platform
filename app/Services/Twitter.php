<?php
/**
 * Twitter api service
 */

namespace App\Services;
use Abraham\TwitterOAuth\TwitterOAuth;
use App\Common\Res;
use App\Facades\session;
use App\VO\VoVal;

class Twitter extends Service
{
    private $connection;

    public function __construct($c)
    {
        parent::__construct($c);

        $this->connection = new TwitterOAuth(
            settings('twitter.consumer_key'),
            settings('twitter.consumer_secret')
        );

        $this->connection->setTimeouts(10, 15);

    }

    public function getLoginLink()
    {
        $connection = new TwitterOAuth(
            settings('twitter.consumer_key'),
            settings('twitter.consumer_secret')
        );

        // request token of application
        $request_token = $connection->oauth
            (
                'oauth/request_token',
                [
                    'oauth_callback' => url('twitter.callback', [], true)
                ]
            );

        check($connection->getLastHttpCode() !== 200, 'Connection can\'t be established');

        session::set('twitter.oauth_token', $request_token['oauth_token']);
        session::set('twitter.oauth_token_secret', $request_token['oauth_token_secret']);

        // generate the URL to make request to authorize our application
        $url = $connection->url('oauth/authorize', ['oauth_token' => $request_token['oauth_token']]);

        return new Res(['link' => $url]);
    }

    /**
     * @param $access_token
     * @param $access_token_secret
     * @throws \Exception
     */
    public function makeConnection(VoVal $access_token, VoVal $access_token_secret)
    {
        $this->connection = new TwitterOAuth(
            settings('twitter.consumer_key'),
            settings('twitter.consumer_secret'),
            (string)$access_token,
            (string)$access_token_secret
        );
    }

    /**
     * @return Res
     */
    public function getUserRequestTokenInfo()
    {
        return new Res([
            'oauth_token' => session::get('twitter.oauth_token'),
            'oauth_token_secret' => session::get('twitter.oauth_token_secret'),
        ]);
    }

    /**
     * @param VoVal $oauthVerifier
     * @return Res
     * @throws \Abraham\TwitterOAuth\TwitterOAuthException
     */
    public function getUserAccessToken(VoVal $oauthVerifier)
    {
        $token = $this->connection->oauth('oauth/access_token', ['oauth_verifier' => (string)$oauthVerifier]);
        return new Res(['token' => $token]);
    }


    public function getMeUser()
    {
        $params = [];
        $content = $this->connection->get('account/verify_credentials', $params);
        return new Res(
            [
                'user' => $content,
            ]
        );
    }

    /**
     * @param int $count
     * @return Res
     */
    public function getHomeTimeline($count = 25)
    {
        $statuses = $this->connection->get("statuses/home_timeline", ["count" => $count, "exclude_replies" => true]);

        return new Res(
            [
                'statuses' => $statuses,
            ]
        );
    }

    /**
     * @param string $text
     * @param array $mediaFilesPaths
     * @return Res
     */
    public function createTweet(string $text, array $mediaFilesPaths=[]): Res
    {

        $mediaIds = [];
        foreach ($mediaFilesPaths as $mediaFilePath)
        {
            $media = $this->connection->upload('media/upload', ['media' => $mediaFilePath]);
            $mediaIds[] = $media->media_id_string;
        }

//        $media2 = $this->connection->upload('media/upload', ['media' => '/path/to/file/kitten2.jpg']);
//        $this->getLastHttpCode() != 200

        $parameters = [
            'status' => $text
        ];

        if (count($mediaIds))
        {
            $parameters['media_ids'] = implode(',', $mediaIds);
        }

        $result = $this->connection->post('statuses/update', $parameters);

        return new Res(
            [
                'result' => $result,
            ]
        );
    }
}

<?php


$app->group('/api_1', function () {

    $this->group('/social_engine', function () {

        $this->post('/post',        'App\Api\SocialEngineController:postStoreAction')->setName('api.post.store');
        $this->post('/post/update', 'App\Api\SocialEngineController:postUpdateAction')->setName('api.post.update');

    })->add(new App\Middlewares\Auth($this->getContainer(), 'api'));


    $this->group('/app', function () {

        $this->post('/cu/models', 'App\Api\AppController:getCurrentUserModels')->setName('api.cu.models');
        $this->post('/run/task', 'App\Api\AppController:runTask')->setName('api.run.task');

        $this->delete('/user_social_account', 'App\Api\SocialEngineController:deleteUserSocialAccountAction')->setName('user_social_account.delete');

        $this->post('/res/GetLastPostsTask', 'App\Api\AppController:resGetLastPostsTask')->setName('GetLastPostsTask');
        $this->post('/res/UpdatePostTask', 'App\Api\AppController:resUpdatePostTask')->setName('UpdatePostTask');
        $this->post('/res/GetPinterestAccountsBoardsTask', 'App\Api\AppController:resGetPinterestAccountsBoardsTask')->setName('GetPinterestAccountsBoardsTask');


    })->add(new App\Middlewares\Auth($this->getContainer(), 'api'));

});

<?php
/**
 * Current user authorized with session
 */
declare(strict_types=1);
namespace App\Facades;

use App\Models\User;

class CU
{
    /** App\Models\User $user*/
    protected static $user = null;
    
    public static function setup()
    {
        $user = session::get('current_user');
        if ($user) {
            self::$user = new User();
            self::$user->fill($user);
        }
    }

    public static function user()
    {
        return self::$user;
    }

    public static function isAuthorized()
    {
        return session::get('is_authorized', false);
    }

    /**
     * @example can('run watch go view')
     * @example can('watch')
     *
     * @param string $permission
     * @return bool
     */
    public static function can(string $permission): bool
    {
        if (eN(self::$user))
        {
            return false;
        }
        //-=-=-=-=-=-=-=-=-=-=-=-=

        foreach (explode(' ', $permission) as $item)
        {
            if (!self::$user->can($item))
            {
                return false;
            }
        }

        return true;
    }
}

#!/usr/bin/env bash

if [ ! -d "cache" ]; then
    mkdir cache;
    mkdir cache/blade
    mkdir logs
    mkdir temp
    php partisan migrate:up
    php partisan run:seed
fi

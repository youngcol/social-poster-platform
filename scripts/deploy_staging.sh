#!/usr/bin/env bash
ssh vlad@52.51.255.999 << EOF
    cd /var/www/sparklme.com
    sudo git checkout -- .
    sudo git pull
    cd /var/www/sparklme.com/www/system_1/
    sudo composer install
    sudo cp /home/vlad/sparklme/init.php /var/www/sparklme.com/www/
    sudo cp /home/vlad/sparklme/.htaccess /var/www/sparklme.com/www/
    sudo cp /home/vlad/sparklme/.htpasswd /var/www/sparklme.com/www/
EOF

#"scripts": {
#		"post-create-project-cmd": "bash -c 'chmod 777 ./log && chmod 777 ./public/uploads && mv ./.env.dist ./.env'",
#		"post-update-cmd": "bash -c 'chmod 0755 ./version.sh; ./version.sh ${JOB_NAME} ${BUILD_NUMBER} ${GIT_BRANCH} > ./version.json'",
#		"post-install-cmd": "bash -c 'chmod 0755 ./version.sh; ./version.sh ${JOB_NAME} ${BUILD_NUMBER} ${GIT_BRANCH} > ./version.json'"
#	}

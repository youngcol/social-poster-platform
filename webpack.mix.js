
// https://laravel.com/docs/5.8/mix

let mix = require('laravel-mix');

var sassFiles = [
    './htdocs/app_/assets/scss/components.scss',
    // './htdocs/app_/assets/scss/style.scss'
];

var appSassFolder = './htdocs/app_/assets/styles/';

sassFiles.map(function (fileName) {
    mix.sass(fileName, appSassFolder);
});

mix.js('./app_vue/app.js', './htdocs/app_/build');
mix.scripts([

    './htdocs/app_/assets/js/jquery-3.2.1.min.js',
    './htdocs/app_/assets/js/underscore.js',
    './app_vue/global_helpers.js',

], './htdocs/app_/build/global_all.js');

mix.sourceMaps();


// App section
// compile css into single file for app dashboard
mix.styles([
    './htdocs/app_/assets/styles/components.css',
], './htdocs/app_/build/all.css');


// Public app
mix
    .js('./app_vue/public_app.js', './htdocs/public/build')
    .sourceMaps();

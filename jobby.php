<?php

//
// Add this line to your crontab file:
//
// * * * * * cd /path/to/project && php jobby.php 1>> /dev/null 2>&1
//

require_once __DIR__ . '/vendor/autoload.php';

$jobby = new \Jobby\Jobby();

define('LOGS', 'logs/' );
$commandsLogPath = LOGS. 'jobby.log';


$jobby->add('CommandExample', array(
    'command' => 'php partisan run:test',
    'schedule' => '* * * * *',
    'output' => $commandsLogPath,
    'enabled' => false,
));


//
//$jobby->add('ProcessQueuedPosts', array(
//    'command' => 'php partisan process:posts:publishing:queue',
//    'schedule' => '* * * * *',
//    'output' => LOGS . 'ProcessQueuedPosts.log',
//    'enabled' => true,
//));



$jobby->add('ProcessPostsScheduledTimeCommand', array(
    'command' => 'php partisan process:posts:scheduled:time',
    'schedule' => '* * * * *',
    'output' => LOGS . 'ProcessPostsScheduledTimeCommand.log',
    'enabled' => true,
));



$jobby->add('ClosureExample', array(
    'command' => function() {
        echo "I'm a function!\n";
        return true;
    },
    'schedule' => '* * * * *',
    'output' => $commandsLogPath,
    'enabled' => true,
));

$jobby->run();

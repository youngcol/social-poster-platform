<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">-->
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">-->

    <title>Exception page</title>


    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->


    <style media="screen">
        .row-100vh{
            height: 100vh;
        }
        .panel-title {
            font-size: 18px;
        }
        .font14{
            font-size: 14px;
        }
        body {
            font-family: monospace, fixed;
            font-family: 'Inconsolata', monospace;
            font-size: 18px;
            line-height: 18px;
        }
        code{
            margin: 10px;
        }

        .top-title
        {
            color: #111;
            line-height: 35px;
        }

    </style>

</head>
<body class="body-page">

    <h2>Exception</h2>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title top-title">
                <?=$e->getMessage()?>
                <?if($class == 'App\Common\ValidationException') { ?>
<!--                    --><?//=$e->getDataAsString()?>
                <?}?>
            </h3>
        </div>
        <h4><?=$e->getFile()?>:<?=$e->getLine()?></h4>

        <table class="table table-hover" id="task-table">

            <tbody>
                <tr>
                    <td>
                        <?php $content = file($e->getFile());?>
                        <p><pre><?=$e->getLine()-3?>:<?=sp($content, $e->getLine()-4, '')?></pre></p>
                        <p><pre><?=$e->getLine()-2?>:<?=sp($content, $e->getLine()-3, '')?></pre></p>

                        <p><pre><?=$e->getLine()-1?>:<?=sp($content, $e->getLine()-2, '')?></pre></p>
                        <p><b><?=$e->getLine()?>:<?=sp($content, $e->getLine()-1, '')?></b></p>
                        <p><pre><?=$e->getLine()+1?>:<?=sp($content, $e->getLine()-0, '')?></pre></p>
                        <p><pre><?=$e->getLine()+2?>:<?=sp($content, $e->getLine()+1, '')?></pre></p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Stacktrace</h3>
        </div>

        <table class="table table-hover font14" id="dev-table">

            <tbody>

            <?foreach ($trace as $item):?>

                <tr>
                    <td>

                    <?if(sp($item, 'file')):?>
                        <div class="file mb-3">
                            <?=$item['file']?>:<?=$item['line']?>
                        </div>
                        <?php
                            $content = file($item['file']);
                        ?>

                        <code class="mb-2">
                            <?=$item['line']?>:<?=$content[$item['line']-1]?>
                        </code>

                    <?else:?>

                    <?endif;?>

                </tr>

            <?endforeach;?>

            </tbody>
        </table>
    </div>

</body>
</html>

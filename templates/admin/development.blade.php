
@extends("admin.layout")

@section('content')


    <form class="command-form" action="{{route('admin.development.command')}}" method="POST">
        <h4>New task</h4>
        {{csrf($csrf)}}
        <input type="hidden" name="command" value="generate:task">
        <input name="param1" type="text" class="form-control mb-1" placeholder="D/F/CreatePostTask">
        <input type="submit" class="form-control col-3 btn btn-primary">
        <div class="console"></div>
    </form>

    <hr>

    <form class="command-form" action="{{route('admin.development.command')}}" method="POST">
        <h4>New migration</h4>
        {{csrf($csrf)}}

        <p>
            If model is `Post`, table should be `posts`.
        </p>

        <input type="hidden" name="command" value="generate:migration">
        <input name="param1" type="text" class="form-control mb-1" placeholder="new_table_migration_file">
        <input type="submit" class="form-control col-3 btn btn-primary">
        <div class="console"></div>
    </form>

    <hr>

    <form class="command-form" action="{{route('admin.development.command')}}" method="POST">
        <h4>New model</h4>
        <p>
            Models build from migrations, please create migration first and run it
            in the forms below, after that the model can be crafted
        </p>
        <p>
            migration: `post_queued_log`, model would be `PostQueuedLog`
        </p>
        {{csrf($csrf)}}
        <input type="hidden" name="command" value="generate:model">
        <input name="param1" type="text" class="form-control mb-1" placeholder="migration file: `post_queued_logs`" value="">
        <input type="submit" class="form-control col-3 btn btn-primary">
        <div class="console"></div>
    </form>

    <hr>

    <form class="command-form" action="{{route('admin.development.command')}}" method="POST">
        <h4>New controller</h4>
        {{csrf($csrf)}}
        <input type="hidden" name="command" value="generate:controller">
        <input name="param1" type="text" class="form-control mb-1" placeholder="Admin\GoController">
        <br>
        <input type="submit" class="form-control col-3 btn btn-primary">
        <div class="console"></div>
    </form>

    <hr>

    <form class="command-form" action="{{route('admin.development.command')}}" method="POST" style="background-color: #ccffdd">
        <h4>New VO</h4>
        {{csrf($csrf)}}
        <input type="hidden" name="command" value="generate:vo">
        <input name="param1" type="text" class="form-control mb-1" placeholder="Pinterest/ProfileText">
        <br>
        <input type="submit" class="form-control col-3 btn btn-primary">
        <div class="console"></div>
    </form>

    <hr>

    <form class="command-form" action="{{route('admin.development.command')}}" method="POST">
        <h4>New seed</h4>
        {{csrf($csrf)}}
        <input type="hidden" name="command" value="generate:seed">
        <input name="param1" type="text" class="form-control mb-1" placeholder="CategoriesSeed">
        <br>
        <input type="submit" class="form-control col-3 btn btn-primary">
        <div class="console"></div>
    </form>
    <hr>
    <form class="command-form" action="{{route('admin.development.command')}}" method="POST">
        <h4>New command</h4>
        {{csrf($csrf)}}
        <input type="hidden" name="command" value="generate:command">
        <input name="param1" type="text" class="form-control mb-1" placeholder="CategoriesSeed">
        <br>
        <input type="submit" class="form-control col-3 btn btn-primary">
        <div class="console"></div>
    </form>

    <hr>
    <hr>

    <form id="run-migration-form" class="command-form" action="{{route('admin.development.command')}}" method="POST" style="background-color: #ccddff">
        {{csrf($csrf)}}

        <h4>Run migrations up</h4>
        <br>

        <button type="submit" class="form-control mt-2 col-3 btn btn-primary" name="action_iup" value="up">Up</button>

        <hr>

        <h4>Run migration down</h4>
        <br>

        <input type="hidden" class="command" name="command" value="migrate:up">
        <select name="param1" class="form-control mb-1" id="">
            @foreach($migrations as $migration)
                <option value="{{$migration->version}}">{{$migration->version}}</option>
            @endforeach
        </select>

        <br>
        <button id="migrate-down" type="button" class="form-control col-3 btn btn-primary" name="action" value="down">Down</button>

        <div class="console"></div>
    </form>

    <hr>

    <form class="command-form" action="{{route('admin.development.command')}}" method="POST" style="background-color: #ffe6ff">
        <h4>Drop whole database</h4>
        <br>

        {{csrf($csrf)}}
        <input type="hidden" name="command" value="drop:whole:database">
        <input type="submit" class="form-control col-3 btn btn-primary" value="Drop all!">
        <div class="console"></div>
    </form>

    <hr>
    <hr>

    <form class="command-form" action="{{route('admin.development.command')}}" method="POST">

        <p><h4>Run seeds</h4></p>
        {{csrf($csrf)}}
        <input type="hidden" name="command" value="run:seed">

        {{--<select name="param1" class="form-control mb-1" id="">--}}
            {{--@foreach($seeds as $seed)--}}
                {{--<option value="{{str_replace('.php', '', $seed)}}">{{$seed}}</option>--}}
            {{--@endforeach--}}
        {{--</select>--}}

        <input type="submit" value="Run" class="form-control col-3 btn btn-primary">
        <div class="console"></div>
    </form>

    <hr>

    <form class="command-form" action="{{route('admin.development.command')}}" method="POST">
        <h4>Generate api docs</h4>
        {{csrf($csrf)}}
        <input type="hidden" name="command" value="generate:docs">
        <input type="submit" class="form-control col-3 btn btn-primary">
        <div class="console"></div>
    </form>


    <style>
        .content {
            font-family: monospace, fixed;
            font-family: 'Inconsolata', monospace;
            font-size: 18px;
            line-height: 18px;
        }

        .command-form .console {
            margin: 10px;
        }

        .console
        {
            border: 1px solid gray;
            padding: 7px;

        }

        form
        {
            margin-bottom: 40px;
            padding: 7px;
        }

    </style>

@endsection

@section('scripts')
    <script>

        jQuery(document).ready(function($) {

            function send_form($form, callb)
            {
                $.ajax({
                    method: 'POST',
                    url: $form.attr('action'),
                    data: $form.serialize(),
                }).fail(function(resp) {
                    $('.console', $form).html(resp.responseText);
                    callb && callb();
                }).done(function( resp ) {
                    $('.console', $form).html(resp.data.exec);
                    callb && callb();
                });
            }

            $('.command-form').submit(function (event) {
                event.preventDefault();
                $('.console', $(this)).html('loading...');
                send_form($(this));
            });

            $('#migrate-down').click(function(){
                var $form = $(this).parents('form');
                $('.command', $form).val('migrate:down');

                send_form($form, function () {
                    $('.command', $form).val('migrate:up')
                });
            });

            $('.generate-api-docs').click(function(){

            })
        });

    </script>
@endsection

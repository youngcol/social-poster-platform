
@extends("admin.layout")

@section('content')
    <div class="breadcrumbs mb-5">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Mysql builder</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <code>
            $flights = App\Flight::where('active', 1)
            ->orderBy('name', 'desc')
            ->take(10)
            ->get();
        </code>
        <div class="col-8">
            <p>
                <pre>
                $flights = App\Flight::where('active', 1)
                ->orderBy('name', 'desc')
                ->take(10)
                ->get();
                </pre>
            </p>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

        jQuery(document).ready(function($) {

        });

    </script>
@endsection

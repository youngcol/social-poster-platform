<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet"
          href="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.15.6/build/styles/default.min.css">
    <script src="//cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.15.6/build/highlight.min.js"></script>

    <title>&copy; WORK &copy; SPACE &copy;</title>

    <style media="screen">
        .row-100vh{
            height: 100vh;
        }

        body {
            font-family: monospace, fixed;
            font-family: 'Inconsolata', monospace;
            font-size: 18px;
            line-height: 18px;
        }

        .hightlighted
        {
            background-color: greenyellow;
        }

        .hidden
        {
            display: none;
        }



    </style>

</head>
<body>

<div class="container-fluid">
    <div class="row justify-content-center">

        <div class="col-md-3">

            <h3>
                <a href="/admin/dashboard">admin</a>
                <a href="#" class="new-dir btn btn-default pull-right">+ dir</a>
            </h3>

            <div class="new-dir-form hidden" style="padding: 10px">
                <form action="{{route('w.folder.create')}}" method="POST" class="new-folder-form">
                    {{csrf($csrf)}}
                    <input type="text" name="folder" class="form-control new-folder mb-2" placeholder="new folder">
                    <input type="submit">
                </form>
            </div>

            <input type="text" class="form-control search" placeholder="search">
            <hr>

            <div class="files-column">
                {!! $leftColumn !!}
            </div>

        </div>

        <div class="col-md-9 opened-file" style="border-left: 1px solid gray">
            <h4 class="name">
                <span class="text">no opened file</span>
                <a href="#" class="reload-file">reload</a>
            </h4>

            <hr>

            <div class="content">
                <div class="output"></div>
                <hr>
                <pre><code class="php-file-content php" style="font-size: 14px"></code></pre>
                {{--<textarea class="php-textarea" name="" id="" cols="30" rows="10"></textarea>--}}
            </div>
        </div>
    </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

<script>

    openedFile = "{{$openedFile}}";

    function search_service()
    {

        $('.search').keyup(function() {
            var val = $(this).val();
            if(val.length < 2) return;
            //=-=-=-=-=-=-=-=-=-=

            $.ajax({
                url: "/w/search",
                data: {
                    needle: val,
                },
            }).fail(function(resp) {
                // $('.opened-file .content').html(resp.responseText);
            }).done(function( resp ) {

                $('.files-column').html(resp.data.column);

            });

        });

        $('.new-dir').click(function(){
            $('.new-dir-form').toggleClass('hidden');
        });


        $('.new-folder-form').submit(function (e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(resp)
                {

                    $('.files-column').html(resp.data.column);
                    $('.new-dir-form').toggleClass('hidden');

                }
            });

        });

    }

    function load_opened_file(openedFile, is_load_output)
    {
        $('.opened-file .name .text').text(openedFile.split('..')[1]);


        $('.opened-file .content .output').html('');

        if (is_load_output)
        {
            $('.opened-file .content .output').html('loading...');

            $.ajax({
                url: "/w/file/output",
                data: {
                    file: openedFile,
                },
            }).fail(function(resp)
            {
                $('.opened-file .content .output').html(resp.responseText);

            }).done(function( output )
            {
                $('.opened-file .content .output').html(output);
            });
        }


        $.ajax({
            url: "/w/file/content",
            data: {
                file: openedFile,
            },
        }).done(function( resp )
        {
            // $('.php-textarea').val(resp.data.content);
            var $block = $('.opened-file .content .php-file-content');
            $block.text(resp.data.content);
            hljs.highlightBlock($block.get(0));

        });
    }

    $(document).ready(function () {

        if (openedFile)
        {
            load_opened_file(openedFile);
        }

        $(window).keyup(function(e) {

            if (e.altKey)
            {
                if (e.key == 'r')
                {
                    load_opened_file(openedFile, 1);
                }
            }

            console.log(e);

            if (e.key == 'Escape')
            {
                $.ajax({
                    type: "GET",
                    url: '/w/left-column',
                    success: function(resp)
                    {
                        $('.files-column').html(resp.data.column);
                    }
                });

                $('.search').val('').focus();
            }
        });


        search_service();

        $('.reload-file').click(function(){
            load_opened_file(openedFile, 1);
        });

    });

</script>

</body>
</html>

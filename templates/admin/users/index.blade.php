@extends("admin.layout")

@section('content')
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Stripped Table</strong>
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Active</th>
                        <th>Permissions</th>
                        <th>Created at</th>
                        <th>Updated at</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user['id'] }}</td>
                            <td>{{ $user['email'] }}</td>
                            <td>{{ $user['name'] }}</td>
                            <td>{{ $user['active'] }}</td>
                            <td>{{ $user['permissions'] }}</td>
                            <td>{{ $user['created_at'] }}</td>
                            <td>{{ $user['updated_at'] }}</td>
                            <td><a href="{{ route('usersedit', ['id' => $user['id']]) }}">Edit</a></td>
                            <td><a href="{{ route('usersdelete', ['id' => $user['id']]) }}">Delete</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

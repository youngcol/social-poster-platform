@extends("admin.layout")

@section('content')

    <div class="row">

        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>{{ $title }}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                                    <li><a href="{{route('admin.users')}}">Users</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(isset($messages['success']))
            <div class="col-8 mt-3">
                @foreach($messages['success'] as $m)
                    <div class="alert alert-primary">
                        {{$m}}
                    </div>
                @endforeach
            </div>
        @endif

        <div class="col-8">
            <div class="card mt-5">
                <div class="card-body">
                    <form method="post" action="{{ $action }}">

                        {{csrf($csrf)}}

                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" name="email" class="form-control" required value="{{ $user['email'] }}">
                            <small class="form-text text-muted">Used all username for login</small>
                        </div>

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" value="{{ $user['name'] }}">
                        </div>

                        <div class="form-group">
                            <label for="name">Permissions</label>
                            <input type="text" name="permissions" class="form-control" value="{{ $user['permissions'] }}">
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" @if(!isset($user)) required @endif>
                            <small class="form-text text-warning">Leave blank for no changes</small>
                        </div>

                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">

                                    <input class="form-check-input" type="radio" name="active" required
                                           value="1" @if(isset($user) && $user['active']) checked @endif
                                    > Active

                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">

                                    <input class="form-check-input" type="radio" name="active"
                                           value="0" @if(isset($user) && !$user['active']) checked @endif
                                    > Unactive
                                </label>
                            </div>
                        </div>

                        <button class="btn btn-block btn-primary">Save</button>

                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection


@extends("admin.layout")

@section('content')

    @include('admin.breadcrumbs', [
        'title' => 'Mysql table viewer',
        'links' => [
            ['route' => 'admin.dashboard', 'title' => 'Dashboard'],
            ['route' => 'admin.mysql_viewer', 'title' => 'Mysql viewer']
        ]
    ])

    <div class="section">
{{--        <div class="col-12">--}}
            @foreach($tableTiers['up'] as $tier)
                <div class="row">
                    @foreach($tier as $table)

                        <div class="col-4 ml-2">
                            <h3>
                                {{$table['name']}}
                                <a class="ml-2" href="{{route('admin.mysql_viewer.table', ['table' => $table['name']])}}#current_table">view</a>
                            </h3>
                            <table class="mt-2 table table-striped">
                                @foreach($table['fields'] as $field)
                                    <tr>
                                        <td>{{$field->Field}}</td>
                                        <td>{{$field->Type}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>

                    @endforeach
                </div>
                <hr>
            @endforeach

            <hr>
            <a href="#current_table">
                <h2 id="current_table">{{$tableTiers['table']['name']}}</h2>

                <table class="mt-2 table table-striped">
                    @foreach($tableTiers['table']['fields'] as $field)
                        <tr>
                            <td><h4>{{$field->Field}}</h4></td>
                            <td><h4>{{$field->Type}}</h4></td>
                        </tr>
                    @endforeach
                </table>
            </a>
            <hr>

        @foreach($tableTiers['down'] as $tier)
            <div class="row">
                @foreach($tier as $table)

                    <div class="col-4 ml-2">
                        <h3>
                            {{$table['name']}}
                            <a class="ml-2" href="{{route('admin.mysql_viewer.table', ['table' => $table['name']])}}#current_table">view</a>
                        </h3>
                        <table class="mt-2 table table-striped">
                            @foreach($table['fields'] as $field)
                                <tr>
                                    <td>{{$field->Field}}</td>
                                    <td>{{$field->Type}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>

                @endforeach
            </div>
            <hr>
        @endforeach
{{--        </div>--}}
    </div>

@endsection

@section('scripts')
    <script>

        jQuery(document).ready(function($) {

        });

    </script>
@endsection

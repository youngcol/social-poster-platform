


@foreach($folders as $index => $folder)
    <div class="folder-block">

        <h2>
            {{$folder}}
            <a href="javascript:void(0)" data-folder="{{$folder}}" data-index="{{$index}}" class="btn btn-default pull-right new-file">+</a>
        </h2>

        <div class="new-file-form-{{$index}} hidden" style="padding: 10px">
            <form action="{{route('w.file.create')}}" method="POST" class="new-file-form">
                {{csrf($csrf)}}
                <input type="hidden" name="folder" value="{{$folder}}">
                <input type="text" name="file" class="form-control mb-2" placeholder="new file">
                <input type="submit">
            </form>
        </div>

        @foreach($grouped[$folder] as $file)
            <p>
                <a href="{{route('w.index')}}?file={{$file['path']}}" class="debug-file" data-file="{{$file['path']}}" data-path="{{$file['path']}}">{{$file['name']}}</a>
            </p>
        @endforeach

    </div>
@endforeach



<script>

    {{--openedFile = "{{$openedFile}}";--}}

    {{--function load_opened_file()--}}
    {{--{--}}
        {{--if (openedFile) {--}}

            {{--$('.opened-file .name .text').text(openedFile.split('..')[1]);--}}

            {{--$.ajax({--}}
                {{--url: "/w/file",--}}
                {{--data: {--}}
                    {{--file: openedFile,--}}
                {{--},--}}
            {{--}).fail(function(resp) {--}}
                {{--$('.opened-file .content').html(resp.responseText);--}}
            {{--}).done(function( output ) {--}}
                {{--$('.opened-file .content').html(output);--}}
            {{--});--}}
        {{--}--}}
    {{--}--}}

    $(document).ready(function () {

        $('.debug-file').click(function (e) {
            e.preventDefault();
            var file = $(this).attr('data-file');

            openedFile = file;
            load_opened_file(file);
            $(window).scrollTop(0);
        });

        $('.new-file').click(function () {

            var index = $(this).attr('data-index');
            $('.new-file-form-'+ index).toggleClass('hidden');

        });

        $('.new-file-form').submit(function (e) {
            e.preventDefault();
            var $form = $(this);

            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function(resp)
                {
                    $('.files-column').html(resp.data.column);
                }
            });

        });


    });

</script>

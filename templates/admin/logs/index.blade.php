@extends("admin.layout")

@section('content')

    <style>
        .content {
            font-family: monospace, fixed;
            font-family: 'Inconsolata', monospace;
        }
        .content .list{
            font-size: 18px;
            line-height: 18px;
        }

        .command-form .console {
            margin: 10px;
        }

        .log-content
        {
            font-size: 14px;
            line-height: 14px;
        }
    </style>

    @if($fileContent)

        <div class="breadcrumbs mb-5">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>{{ $title }}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                                    <li><a href="{{route('admin.logs')}}">Logs</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @if($isTag)
            <div class="log-content">
                @foreach ($parsedContent as $item)
                    <p class="mb-0">{{$item['time']}}</p>
                    <h4>{{$item['title']}}</h4>

                    @if(isset($item['data']))
                        @php(dump($item['data']))
                    @endif
                    <hr>
                @endforeach
            </div>
        @elseif($isExceptions)
            @foreach ($parsedContent as $item)
                @foreach ($item['header'] as $line)
                    <h5>{{$line}}</h5>
                @endforeach

                <div class="mt-3"></div>
                @foreach ($item['lines'] as $line)
                    <p>{{$line}}</p>
                @endforeach

                <hr>
            @endforeach
        @else
            <div class="log-content">
                <p>...</p>
                <pre>{{{ $fileContent }}}</pre>
            </div>
        @endif
    @else
        <div class="list">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Log file</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($files as $file)
                        @if($file == '..' or $file == '.')
                            @continue;
                        @endif

                        <tr>
                            <td>
                                <a href="{{route('admin.logs')}}?log={{$file}}">{{$file}}</a>
                            </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    @endif

@endsection

@extends("app.layout")


@section('content')

    <div class="container settings">
        <div class="level">
            <h1>Settings</h1>
        </div>

        <div class="columns">

            <div class="column is-3 ">
                <aside class="menu is-hidden-mobile">
                    <p class="menu-label">
                        General
                    </p>
                    <ul class="menu-list">
                        <li><a href="{{route('app.settings')}}" class="is-active">Account settings</a></li>
                        <li><a href="{{route('app.settings')}}" class="">Change password</a></li>
                    </ul>

                    {{--                    <p class="menu-label">--}}
                    {{--                        Administration--}}
                    {{--                    </p>--}}
                    {{--                    <ul class="menu-list">--}}
                    {{--                        <li><a>Team Settings</a></li>--}}
                    {{--                        <li>--}}
                    {{--                            <a>Manage Your Team</a>--}}
                    {{--                            <ul>--}}
                    {{--                                <li><a>Members</a></li>--}}
                    {{--                                <li><a>Plugins</a></li>--}}
                    {{--                                <li><a>Add a member</a></li>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                        <li><a>Invitations</a></li>--}}
                    {{--                        <li><a>Cloud Storage Environment Settings</a></li>--}}
                    {{--                        <li><a>Authentication</a></li>--}}
                    {{--                    </ul>--}}
                    <p class="menu-label">
                        Transactions
                    </p>
                    <ul class="menu-list">
                        <li><a>Payments</a></li>
                        <li><a>Transfers</a></li>
                        <li><a>Balance</a></li>
                    </ul>
                </aside>
            </div>



            <div class="column is-9">

{{--                <nav class="breadcrumb" aria-label="breadcrumbs">--}}
{{--                    <ul>--}}
{{--                        <li><a href="../">Bulma</a></li>--}}
{{--                        <li><a href="../">Templates</a></li>--}}
{{--                        <li><a href="../">Examples</a></li>--}}
{{--                        <li class="is-active"><a href="#" aria-current="page">Admin</a></li>--}}
{{--                    </ul>--}}
{{--                </nav>--}}

                <section>


                    <b-field label="Name">
                        <b-input value="{{$current_user['name']}}"></b-input>
                    </b-field>

                    <b-field label="Email"
                             type="is-danger"
                             message="This email is invalid">
                        <b-input type="email"
                                 value="john@"
                                 maxlength="30">
                        </b-input>
                    </b-field>

                    <b-field label="Username"
                             type="is-success"
                             message="This username is available">
                        <b-input value="johnsilver" maxlength="30"></b-input>
                    </b-field>

                    <b-field label="Password">
                        <b-input type="password"
                                 value="iwantmytreasure"
                                 password-reveal>
                        </b-input>
                    </b-field>

                    <b-field label="Message">
                        <b-input maxlength="200" type="textarea"></b-input>
                    </b-field>
                </section>



                <section class="hero is-info welcome is-small">
                    <div class="hero-body">
                        <div class="container">
                            <h1 class="title">
                                Hello, Admin.
                            </h1>
                            <h2 class="subtitle">
                                I hope you are having a great day!
                            </h2>
                        </div>
                    </div>
                </section>

                <section class="info-tiles">
                    <div class="tile is-ancestor has-text-centered">
                        <div class="tile is-parent">
                            <article class="tile is-child box">
                                <p class="title">439k</p>
                                <p class="subtitle">Users</p>
                            </article>
                        </div>
                        <div class="tile is-parent">
                            <article class="tile is-child box">
                                <p class="title">59k</p>
                                <p class="subtitle">Products</p>
                            </article>
                        </div>
                        <div class="tile is-parent">
                            <article class="tile is-child box">
                                <p class="title">3.4k</p>
                                <p class="subtitle">Open Orders</p>
                            </article>
                        </div>
                        <div class="tile is-parent">
                            <article class="tile is-child box">
                                <p class="title">19</p>
                                <p class="subtitle">Exceptions</p>
                            </article>
                        </div>
                    </div>
                </section>


                <div class="columns">
                    <div class="column is-6">
                        <div class="card events-card">
                            <header class="card-header">
                                <p class="card-header-title">
                                    Events
                                </p>
                                <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                  </span>
                                </a>
                            </header>
                            <div class="card-table">
                                <div class="content">
                                    <table class="table is-fullwidth is-striped">
                                        <tbody>
                                        <tr>
                                            <td width="5%"><i class="fa fa-bell-o"></i></td>
                                            <td>Lorum ipsum dolem aire</td>
                                            <td><a class="button is-small is-primary" href="#">Action</a></td>
                                        </tr>
                                        <tr>
                                            <td width="5%"><i class="fa fa-bell-o"></i></td>
                                            <td>Lorum ipsum dolem aire</td>
                                            <td><a class="button is-small is-primary" href="#">Action</a></td>
                                        </tr>
                                        <tr>
                                            <td width="5%"><i class="fa fa-bell-o"></i></td>
                                            <td>Lorum ipsum dolem aire</td>
                                            <td><a class="button is-small is-primary" href="#">Action</a></td>
                                        </tr>
                                        <tr>
                                            <td width="5%"><i class="fa fa-bell-o"></i></td>
                                            <td>Lorum ipsum dolem aire</td>
                                            <td><a class="button is-small is-primary" href="#">Action</a></td>
                                        </tr>
                                        <tr>
                                            <td width="5%"><i class="fa fa-bell-o"></i></td>
                                            <td>Lorum ipsum dolem aire</td>
                                            <td><a class="button is-small is-primary" href="#">Action</a></td>
                                        </tr>
                                        <tr>
                                            <td width="5%"><i class="fa fa-bell-o"></i></td>
                                            <td>Lorum ipsum dolem aire</td>
                                            <td><a class="button is-small is-primary" href="#">Action</a></td>
                                        </tr>
                                        <tr>
                                            <td width="5%"><i class="fa fa-bell-o"></i></td>
                                            <td>Lorum ipsum dolem aire</td>
                                            <td><a class="button is-small is-primary" href="#">Action</a></td>
                                        </tr>
                                        <tr>
                                            <td width="5%"><i class="fa fa-bell-o"></i></td>
                                            <td>Lorum ipsum dolem aire</td>
                                            <td><a class="button is-small is-primary" href="#">Action</a></td>
                                        </tr>
                                        <tr>
                                            <td width="5%"><i class="fa fa-bell-o"></i></td>
                                            <td>Lorum ipsum dolem aire</td>
                                            <td><a class="button is-small is-primary" href="#">Action</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <footer class="card-footer">
                                <a href="#" class="card-footer-item">View All</a>
                            </footer>
                        </div>
                    </div>
                    <div class="column is-6">
                        <div class="card">
                            <header class="card-header">
                                <p class="card-header-title">
                                    Inventory Search
                                </p>
                                <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                  </span>
                                </a>
                            </header>
                            <div class="card-content">
                                <div class="content">
                                    <div class="control has-icons-left has-icons-right">
                                        <input class="input is-large" type="text" placeholder="">
                                        <span class="icon is-medium is-left">
                      <i class="fa fa-search"></i>
                    </span>
                                        <span class="icon is-medium is-right">
                      <i class="fa fa-check"></i>
                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <header class="card-header">
                                <p class="card-header-title">
                                    User Search
                                </p>
                                <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                  </span>
                                </a>
                            </header>
                            <div class="card-content">
                                <div class="content">
                                    <div class="control has-icons-left has-icons-right">
                                        <input class="input is-large" type="text" placeholder="">
                                        <span class="icon is-medium is-left">
                      <i class="fa fa-search"></i>
                    </span>
                                        <span class="icon is-medium is-right">
                      <i class="fa fa-check"></i>
                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    --}}
    {{--    <section class="site-section py-sm pt-5">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-md-6">--}}
    {{--                    <h2 class="mb-4">Settings</h2>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="row blog-entries">--}}
    {{--                <div class="col-md-12 col-lg-8 main-content">--}}
    {{--                    <div class="row">--}}

    {{--                        <div class="col-md-12">--}}
    {{--                            <p>--}}
    {{--                                Name: {{$current_user['name']}}--}}
    {{--                            </p>--}}

    {{--                            <p>--}}
    {{--                                Registered email: {{$current_user['email']}}--}}
    {{--                            </p>--}}

    {{--                            <p>--}}
    {{--                                Registered date:--}}
    {{--                            </p>--}}
    {{--                        </div>--}}


    {{--                        --}}{{--<div class="col-md-6">--}}
    {{--                            --}}{{--<a href="blog-single.html" class="blog-entry element-animate" >--}}
    {{--                                --}}{{--<img src="/app_/assets/images/img_5.jpg" alt="Image placeholder">--}}
    {{--                                --}}{{--<div class="blog-content-body">--}}
    {{--                                    --}}{{--<div class="post-meta">--}}
    {{--                                        --}}{{--<span class="author mr-2"><img src="/app_/assets/images/person_1.jpg" alt="Colorlib"> Colorlib</span>&bullet;--}}
    {{--                                        --}}{{--<span class="mr-2">March 15, 2018 </span> &bullet;--}}
    {{--                                        --}}{{--<span class="ml-2"><span class="fa fa-comments"></span> 3</span>--}}
    {{--                                    --}}{{--</div>--}}
    {{--                                    --}}{{--<h2>How to Find the Video Games of Your Youth</h2>--}}
    {{--                                --}}{{--</div>--}}
    {{--                            --}}{{--</a>--}}
    {{--                        --}}{{--</div>--}}

    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}

@endsection

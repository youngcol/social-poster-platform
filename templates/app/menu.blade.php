
@if(!isset($submenu))
    @php($submenu = null)
@endif

<nav class="navbar is-white topNav">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item" href="{{route('app.index')}}">
                <img src="https://bulma.io/images/bulma-logo.png" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
            </a>
            <div class="navbar-burger burger" data-target="topNav">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <div id="topNav" class="navbar-menu">
            <div class="navbar-start">

                {!!menu_item(
                    'Dashboard', route('app.index'), 'dashboard',
                    [
                        ['Queue', route('app.publish.queue'), 'queue'],
                        ['Drafts', route('app.posts.drafts'), 'drafts'],
                        ['Sent', route('app.posts.sent'), 'sent'],
                        ['Failed', route('app.posts.failed'), 'failed']
                    ], $page, $submenu
                )!!}

                {!!menu_item(
                    'Publish', route('app.publish.queue'), 'publish',
                    [
                        ['Queue', route('app.publish.queue'), 'queue'],
                        ['Drafts', route('app.posts.drafts'), 'drafts'],
                        ['Sent', route('app.posts.sent'), 'sent'],
                        ['Failed', route('app.posts.failed'), 'failed']
                    ], $page, $submenu
                )!!}

{{--                <div class="navbar-item has-dropdown is-hoverable">--}}
{{--                    <a class="navbar-link" href="https://bulma.io/documentation/overview/start/">--}}
{{--                        Docs--}}
{{--                    </a>--}}
{{--                    <div class="navbar-dropdown is-boxed">--}}
{{--                        <a class="navbar-item" href="https://bulma.io/documentation/overview/start/">--}}
{{--                            Overview--}}
{{--                        </a>--}}
{{--                        <a class="navbar-item" href="https://bulma.io/documentation/modifiers/syntax/">--}}
{{--                            Modifiers--}}
{{--                        </a>--}}
{{--                        <a class="navbar-item" href="https://bulma.io/documentation/columns/basics/">--}}
{{--                            Columns--}}
{{--                        </a>--}}
{{--                        <a class="navbar-item" href="https://bulma.io/documentation/layout/container/">--}}
{{--                            Layout--}}
{{--                        </a>--}}
{{--                        <a class="navbar-item" href="https://bulma.io/documentation/form/general/">--}}
{{--                            Form--}}
{{--                        </a>--}}
{{--                        <hr class="navbar-divider">--}}
{{--                        <a class="navbar-item" href="https://bulma.io/documentation/elements/box/">--}}
{{--                            Elements--}}
{{--                        </a>--}}
{{--                        <a class="navbar-item is-active" href="https://bulma.io/documentation/components/breadcrumb/">--}}
{{--                            Components--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}

            </div>

            <div class="navbar-end">

                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                        <span class="icon">
                            <i class="fab fa-twitter"></i>
                        </span>

                        <span class="m-left-10">
                            Account
                        </span>
                    </a>

                    <div class="navbar-dropdown">

                        <a href="{{route('app.settings')}}" class="navbar-item">
                            Settings
                        </a>

                        <hr class="navbar-divider">

                        <a href="{{route('logout')}}" class="navbar-item">
                            Logout
                        </a>

                        @if(settings('env') == 'dev')

                            <hr class="navbar-divider">
                            <hr class="navbar-divider">

                            <a href="{{route('app.templates')}}" class="navbar-item">
                                Templates
                            </a>

                            <a href="{{route('app.vue.stories')}}" class="navbar-item">
                                Vue stories
                            </a>

                        @endif
                    </div>
                </div>

                <a class="navbar-item">
                    Help
                </a>
            </div>
        </div>
    </div>
</nav>

{!! render_submenu($page, $submenu) !!}

@extends("app.layout")


@section('content')

    <div class="container templates">
        <div class="level">
            <h1>Templates</h1>
        </div>


        <hr>

        <div class="column is-3 ">
            <aside class="menu is-hidden-mobile">
                <p class="menu-label">
                    General
                </p>
                <ul class="menu-list">
                    <li><a href="{{route('app.settings')}}" class="is-active">Account settings</a></li>
                    <li><a href="{{route('app.settings')}}" class="">Change password</a></li>
                </ul>

                {{--                    <p class="menu-label">--}}
                {{--                        Administration--}}
                {{--                    </p>--}}
                {{--                    <ul class="menu-list">--}}
                {{--                        <li><a>Team Settings</a></li>--}}
                {{--                        <li>--}}
                {{--                            <a>Manage Your Team</a>--}}
                {{--                            <ul>--}}
                {{--                                <li><a>Members</a></li>--}}
                {{--                                <li><a>Plugins</a></li>--}}
                {{--                                <li><a>Add a member</a></li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}
                {{--                        <li><a>Invitations</a></li>--}}
                {{--                        <li><a>Cloud Storage Environment Settings</a></li>--}}
                {{--                        <li><a>Authentication</a></li>--}}
                {{--                    </ul>--}}
                <p class="menu-label">
                    Transactions
                </p>
                <ul class="menu-list">
                    <li><a>Payments</a></li>
                    <li><a>Transfers</a></li>
                    <li><a>Balance</a></li>
                </ul>
            </aside>
        </div>

        <hr>

    </div>


@endsection('content')

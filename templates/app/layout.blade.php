<!doctype html>
<html lang="en">
<head>
    <title>{{$title}} | {{$name}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="/app_/assets/images/img_2.jpg" rel="icon">

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300, 400,700|Inconsolata:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css">
{{--    <link rel="stylesheet" href="https://use.fontawsome.com/releases/v5.2.0/css/all.css">--}}

    <link rel="stylesheet" href="{{mix('/app_/build/all.css')}}">
</head>
<body>


@include('app.menu')

<div class="wrap" v-scroll="handleScroll">
    @yield('content')
</div>
<!-- END wrap -->

<footer class="footer">
    <div class="container">
        <div class="content has-text-centered">
            <div class="columns is-mobile is-centered">
                <div class="field is-grouped is-grouped-multiline">
                    <div class="control">
                        <div class="tags has-addons">
{{--                            <a class="tag is-link" href="https://github.com/dansup/bulma-templates">Bulma Templates</a>--}}
                            <span class="tag is-light">
                                All rights resorved {{date('Y')}}
                            </span>
                        </div>
                    </div>

                    <div class="control">
                        <div class="tags has-addons">
                            <a class="tag is-link"></a>
                            <span class="tag is-light"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


@foreach($components_html as $html)
    {!! $html !!}
@endforeach


<script type="text/javascript">

    (function() {
        var burger = document.querySelector('.burger');
        var menu = document.querySelector('#'+burger.dataset.target);
        var bottomNav = document.querySelector('.navbar-menu.bottomNav');

        burger.addEventListener('click', function() {
            burger.classList.toggle('is-active');
            menu.classList.toggle('is-active');
            if (bottomNav)
            {
                bottomNav.classList.toggle('is-active');
            }
        });
    })();

    current_user = {!! json_encode($current_user) !!}
    routes = {!! json_encode($global_js_models['routes']) !!};

    csrf = {
        "{{$csrf['nameKey']}}" : "{{$csrf['name']}}",
        "{{$csrf['valueKey']}}" : "{{$csrf['value']}}"
    };

    i18n = {!! json_encode($global_js_models['i18n']) !!}

</script>

<script src="{{mix('/app_/build/global_all.js')}}"></script>
<script src="{{mix('/app_/build/app.js')}}"></script>

@yield('scripts')

</body>
</html>

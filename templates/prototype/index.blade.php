
@extends("prototype.layout")

@section('content')
    <h1>Proto index</h1>

    <hr>

    <p>
        <a class="pure-button" href="{{ route('index') }}">
            Public
        </a>

        <a class="pure-button" href="{{ route('app.index') }}">
            App
        </a>

        <a class="pure-button" href="{{ route('admin.dashboard') }}">
            Admin
        </a>
    </p>

    <div>

    </div>

    <hr>

    <h3>Accounts list</h3>

    <table class="pure-table pure-table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Picture</th>
            <th>Username</th>
            <th>Social network</th>
            <th>Created</th>
        </tr>
        </thead>

        <tbody>
            @foreach($accounts as $account)
                <tr>
                    <td>{{$account->id}}</td>
                    <td>
                        <img src="{{$account->profile_image}}" alt="">
                    </td>
                    <td>{{$account->username}}</td>
                    <td>
                        <span><b>{{$account->socialNetwork->name}}</b></span>
                    </td>
                    <td>
                        {{$account->created_at}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <hr>

    <a class="pure-button" href="{{route('social-engine.account.pinterest.add')}}">Add pinterest account</a>
    <a class="pure-button" href="{{route('social-engine.account.twitter.add')}}">Add twitter account</a>
    <hr>


    <h3>Post message to all accounts</h3>

    <div class="pure-g">
        <form class="pure-form pure-form-aligned pure-g" action="{{route('api.post.send.test')}}" method="post" enctype='multipart/form-data'>

            <div class="pure-u-1 pure-u-md-1-3">

                {{ csrf($csrf) }}

                <div class="pure-control-group">
                    <textarea name="text" id="" cols="30" rows="10"></textarea>
                </div>


                <div class="pure-control-group">
                    <input class="pure-button" type="file" name="media1">
                </div>

                <div class="pure-control-group">
                    <input class="pure-button" type="file" name="media2">
                </div>


            </div>

            <div class="pure-u-1 pure-u-md-1-3">
                <p>Post build params</p>

                <div class="pure-control-group">
                    <label for="post_instantly">
                        <input id="post_instantly" name="creation_type" value="instantly" type="radio"> Post instantly
                    </label>
                </div>

                <div class="pure-control-group">
                    <label for="post_queued">
                        <input name="creation_type" id="post_queued" value="queued" type="radio"> Post queued
                    </label>
                </div>

                <div class="pure-control-group">
                    <label for="scheduled">
                        <input name="creation_type" id="scheduled" type="radio" value="scheduled"> Scheduled
                    </label>

                    <input type="text" name="scheduled_time" class="scheduled_time" value="{{date('Y-m-d H:i:s', time() + 60 * rand(1, 45))}}">
                </div>

                <div class="pure-control-group">
                    <label for="post_queued">
                        Local social accounts ids
                    </label>
                    <input name="social_accounts" id="post_queued" type="text" value="1,2">
                </div>

                <p>
                    Pinterest boards:
                </p>

                <p>851884154454087762(new board)</p>
                <p>851884154453994772(Homemade)</p>

                <div class="pure-control-group">
                    <textarea name="publish_params" id="" cols="30" rows="10">
{
  "accounts": [
    {
      "account_id": 1,
      "board_ids": [
        "851884154454087762"
      ]
    }
  ]
}
                    </textarea>
                </div>

                <div class="pure-control-group">
                    <button type="submit" class="pure-button pure-button-primary">Submit</button>
                </div>
            </div>

        </form>
    </div>

    <div>

    </div>

    <hr>

@endsection


@section('js_scripts')
    <script type="text/javascript">
        $( function() {

            // $( ".scheduled_time" ).datetimepicker();
        } );

    </script>
@endsection


@extends("public.layout")


@section('content')

    <section id="signup" class="section-padding1">

        <div class="container">
            <div class="row">

                <div class="mx-auto col-md-6 col-sm-12 text-center">
                    <h2 class="">Reset password</h2>

                    <div class="mt-4">
                        @if($isPost)
                            @if($isSentEmail)
                                <p>Email was sent, please check the inbox</p>
                            @else
                                <p>No such email in our database!</p>

                            @endif

                        @else

                            <form action="{{route('password.forgot')}}" method="POST">
                                {{csrf($csrf)}}

                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>

                            </form>

                        @endif
                    </div>
                </div>

            </div>
        </div>

    </section>

@endsection

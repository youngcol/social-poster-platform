
@extends("public.layout")

@section('content')

    <section id="" class="section-padding1">

        <div class="container">
            <div class="row">

                <div class="mx-auto col-md-6 col-sm-12">
                    <div class="about-content">

                        <h2>Password change</h2>

                        @if($isVerified)
                            <p>Password changed, now you are redirecting to the app...</p>
                            <a href="{{route('app.index')}}"></a>
                        @else
                            <p>Wrong code!</p>
                        @endif


                        <script type="text/javascript">
                            $(document).ready(function(){

                                setTimeout(function () {
                                    location.href = "{{route('app.index')}}";
                                }, 2000);

                            });
                        </script>

                    </div>
                </div>

            </div>
        </div>

    </section>

@endsection

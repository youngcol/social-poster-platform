
@extends("public.layout")

@section('content')

    <section id="signup" class="section-padding1">

        <div class="container">
            <div class="row">

                <div class="mx-auto col-md-6 col-sm-12 text-center">
                    <div class="about-content">

                        <h2>Sign in</h2>

                        <div class="mt-5">
                            <form method="post" action="{{ route('login') }}">
                                {{ csrf($csrf) }}

                                <div class="form-group">
                                    <input type="text" name="username" class="form-control" required placeholder="Username">
                                </div>

                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" required placeholder="Password">
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-block btn-primary">Login</button>
                                </div>

                                <div class="form-group text-left">
                                    <input type="checkbox" name="remember_me">
                                    Remember me
                                </div>

                                <div class="form-group mt-5">
                                    <a href="{{route('password.forgot')}}">Forgot your password?</a>
                                    • Don't have an account?
                                    <a href="{{route('signup')}}">Sign up now!</a>
                                </div>

                            </form>

                            @if($env == 'dev')
                                <div class="card card-body">
                                    <ul class="text-left">
                                        <li>Username: <code>admin@slimstarterpack.ltd</code></li>
                                        <li>Password <code>slimstarterpack</code></li>
                                    </ul>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>

                <div class="mx-auto col-md-6 col-sm-12 text-center">
                    &nbsp;
                </div>

            </div>
        </div>

    </section><!-- #signup -->


@endsection

<!doctype html>
<html lang="en-US">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/public/assets/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="/public/assets/style.css" type="text/css" />

    <!-- Ionic icons -->
    <link href="https://unpkg.com/ionicons@4.2.0/dist/css/ionicons.min.css" rel="stylesheet">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <meta name="description" content="{{data_get($seo, 'pages.home.description')}}">

    <title>{{data_get($seo, 'pages.home.title')}}</title>

</head>

<body>

<!-- N A V B A R -->
{{--<nav class="navbar navbar-default navbar-expand-lg fixed-top custom-navbar">--}}
    {{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">--}}
        {{--<span class="icon ion-md-menu"></span>--}}
    {{--</button>--}}

    {{--<a href="{{route('index')}}" class="mx-auto">--}}
        {{--<img src="/public/assets/images/logo.png" class="img-fluid nav-logo-mobile" alt="Company Logo">--}}
    {{--</a>--}}

    {{--<div class="collapse navbar-collapse" id="navbarNavDropdown">--}}
        {{--<div class="container">--}}

            {{--<a href="{{route('index')}}" >--}}
                {{--<img src="/public/assets/images/logo.png" class="img-fluid nav-logo-desktop" alt="Company Logo">--}}
            {{--</a>--}}

            {{--@if(isset($page) && $page == 'home')--}}
                {{--<ul class="navbar-nav ml-auto nav-right" data-easing="easeInOutExpo" data-speed="1250" data-offset="65">--}}
                    {{--<li class="nav-item nav-custom-link">--}}
                        {{--<a class="nav-link" href="#hero">Home <i class="icon ion-ios-arrow-forward icon-mobile"></i></a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item nav-custom-link">--}}
                        {{--<a class="nav-link" href="#marketing">Features <i class="icon ion-ios-arrow-forward icon-mobile"></i></a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item nav-custom-link">--}}
                        {{--<a class="nav-link" href="#testimonials">Testimonials <i class="icon ion-ios-arrow-forward icon-mobile"></i></a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item nav-custom-link">--}}
                        {{--<a class="nav-link" href="#pricing">Pricing <i class="icon ion-ios-arrow-forward icon-mobile"></i></a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item nav-custom-link btn btn-demo-small ml-3">--}}
                        {{--<a class="nav-link" href="{{route('signup')}}">Try for Free <i class="icon ion-ios-arrow-forward icon-mobile"></i></a>--}}
                    {{--</li>--}}

                    {{--<li class="nav-item nav-custom-link btn btn-demo-small ml-2">--}}
                        {{--<a class="nav-link" href="{{route('signin')}}">Sign In <i class="icon ion-ios-arrow-forward icon-mobile"></i></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--@endif--}}

        {{--</div>--}}
    {{--</div>--}}
{{--</nav>--}}
<!-- E N D  N A V B A R -->

@yield('content')

<!--  F O O T E R  -->
{{--<footer>--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-3">--}}
                {{--<h5>Seo Ranking</h5>--}}
                {{--<ul>--}}
                    {{--<li><a href="#">Pricing</a></li>--}}
                    {{--<li><a href="#">Affiliate Program</a></li>--}}
                    {{--<li><a href="#">Developer API</a></li>--}}
                    {{--<li><a href="#">Support</a></li>--}}
                    {{--<li><a href="#">Video Tutorials</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<h5>Main Tools</h5>--}}
                {{--<ul>--}}
                    {{--<li><a href="#">Rank Tracker</a></li>--}}
                    {{--<li><a href="#">Backlink Checker</a></li>--}}
                    {{--<li><a href="#">Keyword Generator</a></li>--}}
                    {{--<li><a href="#">Serp Checker</a></li>--}}
                    {{--<li><a href="#">Site Audit</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<h5>Blog</h5>--}}
                {{--<ul>--}}
                    {{--<li><a href="#">Get High Quality Backlinks</a></li>--}}
                    {{--<li><a href="#">Top Google Searches</a></li>--}}
                    {{--<li><a href="#">Avoid Google Penalties</a></li>--}}
                    {{--<li><a href="#">White Hat SEO Tips</a></li>--}}
                    {{--<li><a href="#">Google Trends</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<h5>Company</h5>--}}
                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur scelerisque, tortor nec mattis feugiat, velit purus euismod odio, quis vulputate velit urna.</p>--}}
                {{--<p><a href="mailto:sales@theseocompany.com" class="external-links">sales@theseocompany.com</a></p>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="divider"></div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6 col-xs-12">--}}
                {{--<a href="#"><i class="icon ion-logo-facebook"></i></a>--}}
                {{--<a href="#"><i class="icon ion-logo-instagram"></i></a>--}}
                {{--<a href="#"><i class="icon ion-logo-twitter"></i></a>--}}
                {{--<a href="#"><i class="icon ion-logo-youtube"></i></a>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-xs-12">--}}
                {{--<small>{{date('Y')}} &copy; All rights reserved.</small>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</footer>--}}
<!--  E N D  F O O T E R  -->


<!-- External JavaScripts -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/public/assets/js/jquery-3.3.1.slim.min.js"  crossorigin="anonymous"></script>
<script src="/public/assets/js/popper.min.js"  crossorigin="anonymous"></script>
<script src="/public/assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
</body>
</html>

<?php

use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\SocialEngine\PublishPostToPinterestTask;
use App\VO\FilledString;
use App\VO\Pinterest\OAuthScopes;



$socialAccount = UserSocialAccount::find(1);
$post = Post::find(1);


$Res = task(new PublishPostToPinterestTask, [$socialAccount, $post]);



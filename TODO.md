# social poster app

## basic block

- can add social accounts: tw, fb, pin
- can create posts locally with text and media resources
- can send post to social media instantly
- can schedule post to social media calendar
- can add post to group


## analytics block
- can track post performance
- aggregates posts performance for accounts 

## grow account
- auto following
- auto liking
- evergreen content


## communication block
- direct messages
- can comment on posts

## team 
- posts drafts
- подтверждение постов
- team members 



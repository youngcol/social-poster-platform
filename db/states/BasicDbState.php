<?php

use App\Common\Res;
use App\Models\Pet;
use App\Tasks\SignUpUserTask;

/**
 * Class BasicDbState
 *
 */
class BasicDbState extends DbState
{

    public function __construct()
    {
        parent::__construct('basic:setup');
    }

    public function run()
    {

        // new users

        Pet::create([
            'name' => 'some',
            'parent' => 'parent'
        ]);


//        $res = task(new SignUpUserTask,
//            [
//                vo('Mike3'),
//                vo('Mike3@mail.fjds'),
//                vo('passss1'),
//
//            ]
//        );

        $users = [];

        return new Res([
            'users' => $users
        ]);
    }

}

<?php

use App\Models\SocialEngine\SocialNetwork;

class SocialNetworkSeed
{
    public function run()
    {
        SocialNetwork::create(['name' => 'pinterest']);
        SocialNetwork::create(['name' => 'twitter']);
        SocialNetwork::create(['name' => 'linkedin']);
        SocialNetwork::create(['name' => 'instagram']);
        SocialNetwork::create(['name' => 'facebook']);
        SocialNetwork::create(['name' => 'youtube']);

    }
}

<?php

use App\Models\User;

class UserSeed
{
    public function run()
    {
        $user = new User();
        $user->email = 'admin@slimstarterpack.ltd';
        $user->name = 'Admin';
        $user->active = 1;
        $user->is_admin = 1;
        $user->email_verify_code='';
        $user->permissions = 'all';
        $user->timezone='Etc/GMT+0';
        $user->setPassword('slimstarterpack');
        $user->save();

        $user = new User();
        $user->email = 'user@slimstarterpack.ltd';
        $user->name = 'User';
        $user->active = 1;
        $user->is_admin = 0;
        $user->permissions = '';
        $user->email_verify_code='';
        $user->timezone='Europe/Kiev';
        $user->setPassword('slimstarterpack');
        $user->save();

        $user = new User();
        $user->email = 'user2@slimstarterpack.ltd';
        $user->name = 'User2';
        $user->active = 1;
        $user->is_admin = 0;
        $user->email_verify_code='';
        $user->permissions = '';
        $user->timezone='Etc/GMT-6';
        $user->setPassword('slimstarterpack');
        $user->save();
    }
}

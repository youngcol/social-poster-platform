<?php

use Illuminate\Database\Capsule\Manager as Capsule;


/**
 * 20190316193727_social_engine_tables
 * Class SocialEngineTables
 */
class SocialEngineTables
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->create('social_networks', function($table) {
            $table->increments('id');
            $table->string('name');
        });

        Capsule::schema()->create('user_social_accounts', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('deleted_at');

            $table->integer('user_id')->unsigned();
            $table->integer('social_network_id')->unsigned();
            $table->string('access_token', 1000);
            $table->string('access_token_secret', 1000);

            $table->string('username', 100);
            $table->string('social_account_id', 1000);
            $table->string('profile_image', 1000);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Capsule::schema()->create('post_sheduled_times', function($table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('user_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->dateTime('scheduled_time');

            $table->dateTime('process_started_time');
            $table->enum('process_status', ['scheduled', 'done', 'processing', 'manually_canceled', 'draft']);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');

        });

        Capsule::schema()->create('post_publish_logs', function($table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('social_network_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->tinyInteger('is_success');
            $table->text('info');


            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            $table->foreign('social_network_id')->references('id')->on('social_networks')->onDelete('cascade');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('post_posting_log');
        Capsule::schema()->drop('post_sheduled_time');
        Capsule::schema()->drop('user_social_account');
        Capsule::schema()->drop('social_networks');
    }
}

<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * 20190328224745_media_file_new_fields.php
 */
class MediaFileNewFields
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('media_files', function($table) {

            $table->dropColumn('url');
            $table->string('s3_url');
            $table->string('filename');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->table('media_files', function($table) {

            $table->string('url');
            $table->dropColumn('s3_url');
            $table->dropColumn('filename');
            $table->dropColumn('user_id');

        });
    }
}

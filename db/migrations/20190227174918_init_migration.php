<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class InitMigration
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->create('users', function($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('password');
            $table->string('permissions')->nullable();
            $table->string('timezone');
            $table->boolean('is_admin')->nullable();
            $table->boolean('active');
            $table->string('email')->unique();

            $table->tinyInteger('is_email_verified')->nullable();
            $table->string('email_verify_code')->nullable();
            $table->string('password_reset_code')->nullable();

            $table->timestamps();
        });

        Capsule::schema()->create('sessions', function($table) {
            $table->increments('id');
            $table->string('uniqid')->unique();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('users');
        Capsule::schema()->drop('sessions');
    }
}

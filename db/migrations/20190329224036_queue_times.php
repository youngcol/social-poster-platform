<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * 20190329224036_queue_time.php
 */
class QueueTimes
{
    /**
     * Do the migration
     */
    public function up()
    {

        Capsule::schema()->create('queue_times', function($table) {
            $table->increments('id');
            $table->timestamps();

            $table->enum('week_day', ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']);
            $table->time('intraday_time');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('queue_time');

    }
}

'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
sass.compiler = require('node-sass');
// var sourcemaps = require('gulp-sourcemaps');
var exec = require('child_process').exec;

// var babel           = require('gulp-babel');
// var browserSync     = require('browser-sync');
// var rename          = require('gulp-rename');
// var vueComponent    = require('gulp-vue-single-file-component');
//

var paths = {

};

var adminSass = function()
{
    return gulp.src('./htdocs/admin/assets/scss/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('./htdocs/admin/assets/css'));
};


var compileLangs = function (cb) {
    return exec('php partisan compile:languages', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
};


// Watch files
function watchFiles() {
    gulp.watch("./htdocs/admin/assets/scss/**/*.scss", adminSass);
    gulp.watch("./lang/**/*.php", compileLangs);
    // gulp.watch("./assets/js/**/*", gulp.series(scriptsLint, scripts));
    // gulp.watch(
    //     [
    //         "./_includes/**/*",
    //         "./_layouts/**/*",
    //         "./_pages/**/*",
    //         "./_posts/**/*",
    //         "./_projects/**/*"
    //     ],
    //     gulp.series(jekyll, browserSyncReload)
    // );
    // gulp.watch("./assets/img/**/*", images);
}




gulp.task('admin:sass', function () {

});


gulp.task('default', function () {
    gulp.watch('./htdocs/admin/assets/scss/**/*.scss', adminSass);
});


const watch = gulp.parallel(watchFiles);


var partisan = function()
{

};


// gulp.task('partisan', function () {
//     // return cp.execFile('ls -la');
//     // gulp.watch('./htdocs/admin/assets/scss/**/*.scss', ['admin:sass']);
// });


// export tasks
// exports.images = images;
// exports.css = css;
// exports.js = js;
// exports.jekyll = jekyll;
// exports.clean = clean;
exports.partisan = partisan;
exports.watch = watch;

<?php

/**
 * All imports of the directory  /config/* int this file
 */
return [
 	'settings' => [

 	    // ** IMPORTS
 	    'seo' => require PATH_ROOT . 'config/seo.php',

        // ** END IMPORTS

		'determineRouteBeforeAppMiddleware' => false,
		'displayErrorDetails' => getenv('APP_ENV') === 'dev',

        'env' => getenv('APP_ENV'),
        'host' => getenv('APP_HOST'),
        'key' => getenv('APP_KEY'),
        'name' => getenv('APP_NAME'),

        'upload_media_temp_folder' => getenv('UPLOAD_MEDIA_TEMP_FOLDER'),
        'temp_uploads_lifetime' => getenv('TEMP_UPLOADS_LIFETIME'),

		'mailer' =>[
			// sender Api from mailjet.com
			'sender_email' 			=> '',
			'sender_password' 		=> '',
			'sender_host'			=> 'ssl://in-v3.mailjet.com',
			// sender info
			'sender_display_name' 	=> 'Slim Starter Pack',
			'sender_display_email' 	=> 'admin@slimstarterpack.ltd',
			'sender_reply_to_name' 	=> 'Slim Starter Pack',
			'sender_reply_to_email' => 'admin@slimstarterpack.ltd',
			// debug info
			'debug'					=> 0, // [0,1,2]

		],
        'week_days' => [
            1 => 'monday',
            2 => 'tuesday',
            3 => 'wednesday',
            4 => 'thursday',
            5 => 'friday',
            6 => 'saturday',
            7 => 'sunday',
        ],

        'db' => [
        	'driver'    => getenv('DB_DRIVER'),
        	'host'      => getenv('DB_HOST'),
        	'database'  => getenv('DB_NAME'),
        	'username'  => getenv('DB_USER'),
        	'password'  => getenv('DB_PASS'),
        	'charset'   => 'utf8',
        	'collation' => 'utf8_unicode_ci',
        	'prefix'    => '',
        ],

        'pinterest' => [
            'client_id' => getenv('PINTEREST_CLIENT_ID'),
            'client_secret' => getenv('PINTEREST_CLIENT_SECRET'),
        ],

        'twitter' => [
            'consumer_key' => getenv('TWITTER_CONSUMER_KEY'),
            'consumer_secret' => getenv('TWITTER_CONSUMER_SECRET'),
        ],

        'amazon' => [
            's3' => [
                'media_bucket' => getenv('AMAZON_MEDIA_UPLOAD_BUCKET'),
                'region' => getenv('AMAZON_S3_REGION'),
                'key'    => getenv('AMAZON_S3_KEY'),
                'secret' => getenv('AMAZON_S3_SECRET')
            ]
        ]
	]
];

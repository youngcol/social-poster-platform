# Slim Starter Pack architecture


# Code solutions




## Controller validate request
```php
list
    (
    $err,
    $pass1,
    $pass2,
    $code
    ) = params
    (
        $request,
        'user.password.change',
        [
            'pass1' => 'user.password',
            'pass2' => 'user.password',
            'code' => 'filled.string',
        ]
    );
    
if ($err)
{
    return bad_request_error($err);
//            return back_with_errors($err);
}
///-=-=-=-=-=-=-=-=-=-=-=-=

if (!CU::can('watch'))
{
    return json_403();
}

```

## Build value object 
```$xslt
$voObject = vo('value', 'filled.string'),
$voDate = vo('2003-12-05 15:15:33', 'datetime'),
```

## Facades
- sessions
- db
- CU - current user


## Repositories for models
repo(User:class)

## Run job scripts
Run php job script async

## Tasks
Small blocks of safe code

## Stories
Sequences of tasks
